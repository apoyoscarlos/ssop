ALTER TABLE "public"."Coordenada" 
  DROP CONSTRAINT "Coordenada_pkey",
  ALTER COLUMN "nNombre" SET NOT NULL,
  ADD CONSTRAINT "Coordenada_pkey" PRIMARY KEY ("iIdObra", "nPunto", "nNombre");