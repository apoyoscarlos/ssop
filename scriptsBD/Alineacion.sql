CREATE TABLE "Alineacion" (
    "iIdAlineacion" SERIAL PRIMARY KEY,
    "iIdObra" smallint REFERENCES "Obra",
    "iIdEje" smallint REFERENCES "PED2019Eje",
    "iIdPoliticaPublica" smallint REFERENCES "PED2019Tema",
    "iIdObjetivo" smallint REFERENCES "PED2019Objetivo",
    "iIdIndicador" smallint REFERENCES "Indicador",
    "iIdEstrategia" smallint REFERENCES "PED2019Estrategia",
    "iIdOds" smallint REFERENCES "ODS",
    "iIdCompromiso" smallint REFERENCES "Compromiso",
    "vDescripcion" text
);