--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5
-- Dumped by pg_dump version 10.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Apertura; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Apertura" (
    "iIdApertura" smallint NOT NULL,
    "vDescripcion" character varying(200),
    "nImporte" numeric(20,2),
    "iAnio" smallint DEFAULT 2019,
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Apertura" OWNER TO postgres;

--
-- Name: Apertura_iIdApertura_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Apertura_iIdApertura_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Apertura_iIdApertura_seq" OWNER TO postgres;

--
-- Name: Apertura_iIdApertura_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Apertura_iIdApertura_seq" OWNED BY public."Apertura"."iIdApertura";


--
-- Name: Categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Categoria" (
    "iIdCategoria" smallint NOT NULL,
    "vCategoria" character varying(255) NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL,
    "vAbreviacion" character varying(100)
);


ALTER TABLE public."Categoria" OWNER TO postgres;

--
-- Name: Categoria_iIdCategoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Categoria_iIdCategoria_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Categoria_iIdCategoria_seq" OWNER TO postgres;

--
-- Name: Categoria_iIdCategoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Categoria_iIdCategoria_seq" OWNED BY public."Categoria"."iIdCategoria";


--
-- Name: Compromiso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Compromiso" (
    "iIdCompromiso" smallint NOT NULL,
    "vCompromiso" character varying(350) NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Compromiso" OWNER TO postgres;

--
-- Name: Compromiso_iIdCompromiso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Compromiso_iIdCompromiso_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Compromiso_iIdCompromiso_seq" OWNER TO postgres;

--
-- Name: Compromiso_iIdCompromiso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Compromiso_iIdCompromiso_seq" OWNED BY public."Compromiso"."iIdCompromiso";


--
-- Name: Contrato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Contrato" (
    "iIdContrato" smallint NOT NULL,
    "vContrato" character varying(255) NOT NULL,
    "vRuta" character varying(255) NOT NULL,
    "iIdObra" smallint NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Contrato" OWNER TO postgres;

--
-- Name: Contrato_iIdContrato_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Contrato_iIdContrato_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Contrato_iIdContrato_seq" OWNER TO postgres;

--
-- Name: Contrato_iIdContrato_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Contrato_iIdContrato_seq" OWNED BY public."Contrato"."iIdContrato";


--
-- Name: Convenio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Convenio" (
    "iIdConvenio" smallint NOT NULL,
    "vNombreArchivo" character varying(255) NOT NULL,
    "vRuta" character varying(255) NOT NULL,
    "iIdObra" smallint NOT NULL,
    "iActivo" character varying(255)
);


ALTER TABLE public."Convenio" OWNER TO postgres;

--
-- Name: Convenio_iIdConvenio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Convenio_iIdConvenio_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Convenio_iIdConvenio_seq" OWNER TO postgres;

--
-- Name: Convenio_iIdConvenio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Convenio_iIdConvenio_seq" OWNED BY public."Convenio"."iIdConvenio";


--
-- Name: Coordenada; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Coordenada" (
    "iIdObra" smallint NOT NULL,
    "nPunto" smallint NOT NULL,
    "iTipo" smallint DEFAULT 1 NOT NULL,
    "nLat" numeric(20,18),
    "nLong" numeric(20,18),
    "nNombre" character varying(255)
);


ALTER TABLE public."Coordenada" OWNER TO postgres;

--
-- Name: Dependencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Dependencia" (
    "iIdDependencia" smallint NOT NULL,
    "vDependencia" character varying(255) NOT NULL,
    "vSiglas" character varying(255),
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Dependencia" OWNER TO postgres;

--
-- Name: Dependencia_iIdDependencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Dependencia_iIdDependencia_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Dependencia_iIdDependencia_seq" OWNER TO postgres;

--
-- Name: Dependencia_iIdDependencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Dependencia_iIdDependencia_seq" OWNED BY public."Dependencia"."iIdDependencia";


--
-- Name: Financiamiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Financiamiento" (
    "iIdFinanciamiento" smallint NOT NULL,
    "vFinanciamiento" character varying(255) NOT NULL,
    "iAnio" smallint DEFAULT 2019 NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Financiamiento" OWNER TO postgres;

--
-- Name: Financiamiento_iIdFinanciamiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Financiamiento_iIdFinanciamiento_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Financiamiento_iIdFinanciamiento_seq" OWNER TO postgres;

--
-- Name: Financiamiento_iIdFinanciamiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Financiamiento_iIdFinanciamiento_seq" OWNED BY public."Financiamiento"."iIdFinanciamiento";


--
-- Name: Imagen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Imagen" (
    "iIdObra" smallint NOT NULL,
    "vNombre" character varying(255) NOT NULL,
    "vRuta" character varying(255) NOT NULL,
    "iTipo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Imagen" OWNER TO postgres;

--
-- Name: Indicador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Indicador" (
    "iIdIndicador" smallint NOT NULL,
    "vIndicador" character varying(255),
    "iIdObjetivo" smallint NOT NULL
);


ALTER TABLE public."Indicador" OWNER TO postgres;

--
-- Name: Indicador_iIdIndicador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Indicador_iIdIndicador_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Indicador_iIdIndicador_seq" OWNER TO postgres;

--
-- Name: Indicador_iIdIndicador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Indicador_iIdIndicador_seq" OWNED BY public."Indicador"."iIdIndicador";


--
-- Name: PED2019LineaAccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."PED2019LineaAccion" (
    "iIdLineaAccion" smallint NOT NULL,
    "vLineaAccion" character varying(350) NOT NULL,
    "iIdEstrategia" smallint NOT NULL,
    "iIdOds" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public."PED2019LineaAccion" OWNER TO postgres;

--
-- Name: LineaAccion_iIdLineaAccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."LineaAccion_iIdLineaAccion_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."LineaAccion_iIdLineaAccion_seq" OWNER TO postgres;

--
-- Name: LineaAccion_iIdLineaAccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."LineaAccion_iIdLineaAccion_seq" OWNED BY public."PED2019LineaAccion"."iIdLineaAccion";


--
-- Name: Localidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Localidad" (
    "iIdLocalidad" smallint NOT NULL,
    "vLocalidad" character varying(255) NOT NULL,
    "iIdMunicipio" smallint NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL,
    "vClave" character varying(5) NOT NULL
);


ALTER TABLE public."Localidad" OWNER TO postgres;

--
-- Name: Localidad_iIdLocalidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Localidad_iIdLocalidad_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Localidad_iIdLocalidad_seq" OWNER TO postgres;

--
-- Name: Localidad_iIdLocalidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Localidad_iIdLocalidad_seq" OWNED BY public."Localidad"."iIdLocalidad";


--
-- Name: Municipio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Municipio" (
    "iIdMunicipio" smallint NOT NULL,
    "vMunicipio" character varying(255) NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL,
    "vClave" character varying(3)
);


ALTER TABLE public."Municipio" OWNER TO postgres;

--
-- Name: Municipio_iIdMunicipio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Municipio_iIdMunicipio_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Municipio_iIdMunicipio_seq" OWNER TO postgres;

--
-- Name: Municipio_iIdMunicipio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Municipio_iIdMunicipio_seq" OWNED BY public."Municipio"."iIdMunicipio";


--
-- Name: ODS; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ODS" (
    "iIdOds" smallint NOT NULL,
    "vOds" character varying(255) NOT NULL
);


ALTER TABLE public."ODS" OWNER TO postgres;

--
-- Name: ODS_iIdOds_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."ODS_iIdOds_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ODS_iIdOds_seq" OWNER TO postgres;

--
-- Name: ODS_iIdOds_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."ODS_iIdOds_seq" OWNED BY public."ODS"."iIdOds";


--
-- Name: Obra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Obra" (
    "iIdObra" smallint NOT NULL,
    "vNombre" character varying(255) NOT NULL,
    "vDescripcion" character varying(500),
    "iIdResponsable" smallint,
    "iIdEjecutor" smallint,
    "dFechaValidacion" date,
    "iIdApertura" smallint,
    "iAnioEjecucion" smallint DEFAULT 2019 NOT NULL,
    "dFechaCaptura" timestamp(6) without time zone,
    "iEstatus" smallint,
    "iIdPartida" smallint,
    "vDescripcionImpacto" character varying(500),
    "vBeneficiarios" character varying(500),
    "nImporteOriginal" numeric(20,2),
    "iAnioRecurso" smallint,
    "iIdCategoria" smallint,
    "vDomicilio" character varying(200),
    "iAtencionPrioritaria" smallint DEFAULT 0,
    "iIdUnidad" smallint,
    "iIdUgi" smallint,
    "iIdUbp" smallint,
    "vNombreModificado" character varying(300),
    "vConvenioFed" character varying(300),
    "vConveniodEjecutor" character varying(300),
    "vConvenioBeneficiario" character varying(300),
    "vClaveEscuela" character varying(50),
    "iParcial" smallint DEFAULT 0,
    "vDescripcionAdjudicacion" character varying(255),
    "vRegistroConstructor" character varying(100),
    "vEmpresa" character varying(100),
    "vRepresentante" character varying(100),
    "iTipo" smallint,
    "nNumeroContrato" character varying(100),
    "nImporteContrato" numeric(20,2),
    "dFechaInicio" timestamp(6) without time zone,
    "dFechaTermino" timestamp(6) without time zone,
    "iNumeroEmpleos" smallint,
    "iAvanceFisico" smallint,
    "iAvanceFinanciero" smallint,
    "nAmpliacion" numeric(20,2),
    "nMontoAmpliacion" numeric(20,2),
    "iDiasRetraso" smallint,
    "iEstado" smallint,
    "dNuevaFechaTermino" date,
    "vObservaciones" character varying(500),
    "nAportacionFiditrac" numeric(20,2),
    "nAportacionCMIC" numeric(20,2),
    "iTerminada" smallint DEFAULT 0,
    "nMontoPagado" numeric(20,2),
    "vEconomias" character varying(150),
    "vMetas" character varying(500),
    "vCierreAdministrativo" character varying(150),
    "dEntregaBeneficiario" date,
    "dEntregaContraloria" date,
    "nFiniquitoSOP" numeric(20,2)
);


ALTER TABLE public."Obra" OWNER TO postgres;

--
-- Name: ObraFinanciamiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ObraFinanciamiento" (
    "iIdObra" smallint NOT NULL,
    "iIdFinanciamiento" smallint NOT NULL,
    "nMonto" numeric(20,2) DEFAULT 0 NOT NULL
);


ALTER TABLE public."ObraFinanciamiento" OWNER TO postgres;

--
-- Name: ObraLocalidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ObraLocalidad" (
    "iIdObra" smallint NOT NULL,
    "iIdLocalidad" smallint NOT NULL
);


ALTER TABLE public."ObraLocalidad" OWNER TO postgres;

--
-- Name: Obra_iIdObra_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Obra_iIdObra_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Obra_iIdObra_seq" OWNER TO postgres;

--
-- Name: Obra_iIdObra_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Obra_iIdObra_seq" OWNED BY public."Obra"."iIdObra";


--
-- Name: Oficio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Oficio" (
    "iIdOficio" smallint NOT NULL,
    "vDescripcion" character varying(150) NOT NULL,
    "vRuta" character varying(255) NOT NULL,
    "iIdObra" smallint DEFAULT 1 NOT NULL,
    "iActivo" smallint NOT NULL
);


ALTER TABLE public."Oficio" OWNER TO postgres;

--
-- Name: Oficio_iIdOficio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Oficio_iIdOficio_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Oficio_iIdOficio_seq" OWNER TO postgres;

--
-- Name: Oficio_iIdOficio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Oficio_iIdOficio_seq" OWNED BY public."Oficio"."iIdOficio";


--
-- Name: PED2019Eje; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."PED2019Eje" (
    "iIdEje" smallint NOT NULL,
    "vEje" character varying(255) NOT NULL
);


ALTER TABLE public."PED2019Eje" OWNER TO postgres;

--
-- Name: PED2019Eje_iIdEje_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."PED2019Eje_iIdEje_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PED2019Eje_iIdEje_seq" OWNER TO postgres;

--
-- Name: PED2019Eje_iIdEje_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."PED2019Eje_iIdEje_seq" OWNED BY public."PED2019Eje"."iIdEje";


--
-- Name: PED2019Estrategia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."PED2019Estrategia" (
    "iIdEstrategia" smallint NOT NULL,
    "vEstrategia" character varying(255) NOT NULL,
    "iIdObjetivo" smallint NOT NULL
);


ALTER TABLE public."PED2019Estrategia" OWNER TO postgres;

--
-- Name: PED2019Estrategia_iIdEstrategia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."PED2019Estrategia_iIdEstrategia_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PED2019Estrategia_iIdEstrategia_seq" OWNER TO postgres;

--
-- Name: PED2019Estrategia_iIdEstrategia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."PED2019Estrategia_iIdEstrategia_seq" OWNED BY public."PED2019Estrategia"."iIdEstrategia";


--
-- Name: PED2019Objetivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."PED2019Objetivo" (
    "iIdObjetivo" smallint NOT NULL,
    "vObjetivo" character varying(255) NOT NULL,
    "iIdTema" smallint NOT NULL
);


ALTER TABLE public."PED2019Objetivo" OWNER TO postgres;

--
-- Name: PED2019Objetivo_iIdObjetivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."PED2019Objetivo_iIdObjetivo_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PED2019Objetivo_iIdObjetivo_seq" OWNER TO postgres;

--
-- Name: PED2019Objetivo_iIdObjetivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."PED2019Objetivo_iIdObjetivo_seq" OWNED BY public."PED2019Objetivo"."iIdObjetivo";


--
-- Name: PED2019Tema; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."PED2019Tema" (
    "iIdTema" smallint NOT NULL,
    "vTema" character varying(255) NOT NULL,
    "iIdEje" smallint NOT NULL
);


ALTER TABLE public."PED2019Tema" OWNER TO postgres;

--
-- Name: PED2019Tema_iIdTema_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."PED2019Tema_iIdTema_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PED2019Tema_iIdTema_seq" OWNER TO postgres;

--
-- Name: PED2019Tema_iIdTema_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."PED2019Tema_iIdTema_seq" OWNED BY public."PED2019Tema"."iIdTema";


--
-- Name: Partida; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Partida" (
    "iIdPartida" smallint NOT NULL,
    "vClave" character varying(20) NOT NULL,
    "vPartida" character varying(255) NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Partida" OWNER TO postgres;

--
-- Name: Partida_iIdPartida_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Partida_iIdPartida_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Partida_iIdPartida_seq" OWNER TO postgres;

--
-- Name: Partida_iIdPartida_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Partida_iIdPartida_seq" OWNED BY public."Partida"."iIdPartida";


--
-- Name: Permiso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Permiso" (
    "iIdPermiso" smallint NOT NULL,
    "vPermiso" character varying(255) NOT NULL,
    "vDescripcion" character varying(255) NOT NULL,
    "iTipo" smallint DEFAULT 1 NOT NULL,
    "vUrl" character varying(255),
    "iIdPermisoPadre" smallint DEFAULT 0 NOT NULL,
    "vClass" character varying(255),
    "iOrden" smallint DEFAULT 0 NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL,
    "iInicial" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public."Permiso" OWNER TO postgres;

--
-- Name: Permiso_iIdPermiso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Permiso_iIdPermiso_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Permiso_iIdPermiso_seq" OWNER TO postgres;

--
-- Name: Permiso_iIdPermiso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Permiso_iIdPermiso_seq" OWNED BY public."Permiso"."iIdPermiso";


--
-- Name: Rol; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Rol" (
    "iIdRol" smallint NOT NULL,
    "vRol" character varying(255) NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Rol" OWNER TO postgres;

--
-- Name: RolPermiso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."RolPermiso" (
    "iIdRol" smallint NOT NULL,
    "iIdPermiso" smallint NOT NULL,
    "iTipoAcceso" smallint DEFAULT 2 NOT NULL
);


ALTER TABLE public."RolPermiso" OWNER TO postgres;

--
-- Name: Rol_iIdRol_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Rol_iIdRol_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Rol_iIdRol_seq" OWNER TO postgres;

--
-- Name: Rol_iIdRol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Rol_iIdRol_seq" OWNED BY public."Rol"."iIdRol";


--
-- Name: Suficiencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Suficiencia" (
    "iIdSuficiencia" smallint NOT NULL,
    "vDescripcion" character varying(255) NOT NULL,
    "vRuta" character varying(255) NOT NULL,
    "iIdObra" smallint NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Suficiencia" OWNER TO postgres;

--
-- Name: Suficiencia_iIdSuficiencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Suficiencia_iIdSuficiencia_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Suficiencia_iIdSuficiencia_seq" OWNER TO postgres;

--
-- Name: Suficiencia_iIdSuficiencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Suficiencia_iIdSuficiencia_seq" OWNED BY public."Suficiencia"."iIdSuficiencia";


--
-- Name: UBP; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."UBP" (
    "iIdUBP" smallint NOT NULL,
    "vUBP" character varying(255),
    "iAnio" smallint DEFAULT 2019,
    "iActivo" smallint DEFAULT 1 NOT NULL,
    "vClave" character varying(5)
);


ALTER TABLE public."UBP" OWNER TO postgres;

--
-- Name: UBP_iIdUbp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."UBP_iIdUbp_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."UBP_iIdUbp_seq" OWNER TO postgres;

--
-- Name: UBP_iIdUbp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."UBP_iIdUbp_seq" OWNED BY public."UBP"."iIdUBP";


--
-- Name: UGI; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."UGI" (
    "iIdUGI" smallint NOT NULL,
    "vClave" character varying(25),
    "vUGI" character varying(150),
    "vDescripcion" character varying(500),
    "iActivo" smallint DEFAULT 1
);


ALTER TABLE public."UGI" OWNER TO postgres;

--
-- Name: UGI_iIdUgi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."UGI_iIdUgi_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."UGI_iIdUgi_seq" OWNER TO postgres;

--
-- Name: UGI_iIdUgi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."UGI_iIdUgi_seq" OWNED BY public."UGI"."iIdUGI";


--
-- Name: Unidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Unidad" (
    "iIdUnidad" smallint NOT NULL,
    "vUnidad" character varying(255) NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Unidad" OWNER TO postgres;

--
-- Name: Unidad_iIdUnidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Unidad_iIdUnidad_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Unidad_iIdUnidad_seq" OWNER TO postgres;

--
-- Name: Unidad_iIdUnidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Unidad_iIdUnidad_seq" OWNED BY public."Unidad"."iIdUnidad";


--
-- Name: Usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Usuario" (
    "iIdUsuario" smallint NOT NULL,
    "vUsuario" character varying(255) NOT NULL,
    "vNombre" character varying(255) NOT NULL,
    "vPrimerApellido" character varying(255),
    "vSegundoApellido" character varying(255),
    "vPassword" character varying(255),
    "vCorreo" character varying(255),
    "vToken" bit varying(255),
    "iIdDependencia" smallint,
    "iIdRol" smallint DEFAULT 1 NOT NULL,
    "iActivo" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."Usuario" OWNER TO postgres;

--
-- Name: UsuarioPermiso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."UsuarioPermiso" (
    "iIdUsuario" smallint NOT NULL,
    "iIdPermiso" smallint NOT NULL,
    "iTipoAcceso" smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public."UsuarioPermiso" OWNER TO postgres;

--
-- Name: Usuario_iIdUsuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Usuario_iIdUsuario_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Usuario_iIdUsuario_seq" OWNER TO postgres;

--
-- Name: Usuario_iIdUsuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Usuario_iIdUsuario_seq" OWNED BY public."Usuario"."iIdUsuario";


--
-- Name: Apertura iIdApertura; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Apertura" ALTER COLUMN "iIdApertura" SET DEFAULT nextval('public."Apertura_iIdApertura_seq"'::regclass);


--
-- Name: Categoria iIdCategoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Categoria" ALTER COLUMN "iIdCategoria" SET DEFAULT nextval('public."Categoria_iIdCategoria_seq"'::regclass);


--
-- Name: Compromiso iIdCompromiso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Compromiso" ALTER COLUMN "iIdCompromiso" SET DEFAULT nextval('public."Compromiso_iIdCompromiso_seq"'::regclass);


--
-- Name: Contrato iIdContrato; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Contrato" ALTER COLUMN "iIdContrato" SET DEFAULT nextval('public."Contrato_iIdContrato_seq"'::regclass);


--
-- Name: Convenio iIdConvenio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Convenio" ALTER COLUMN "iIdConvenio" SET DEFAULT nextval('public."Convenio_iIdConvenio_seq"'::regclass);


--
-- Name: Dependencia iIdDependencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Dependencia" ALTER COLUMN "iIdDependencia" SET DEFAULT nextval('public."Dependencia_iIdDependencia_seq"'::regclass);


--
-- Name: Financiamiento iIdFinanciamiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Financiamiento" ALTER COLUMN "iIdFinanciamiento" SET DEFAULT nextval('public."Financiamiento_iIdFinanciamiento_seq"'::regclass);


--
-- Name: Indicador iIdIndicador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Indicador" ALTER COLUMN "iIdIndicador" SET DEFAULT nextval('public."Indicador_iIdIndicador_seq"'::regclass);


--
-- Name: Localidad iIdLocalidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Localidad" ALTER COLUMN "iIdLocalidad" SET DEFAULT nextval('public."Localidad_iIdLocalidad_seq"'::regclass);


--
-- Name: Municipio iIdMunicipio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Municipio" ALTER COLUMN "iIdMunicipio" SET DEFAULT nextval('public."Municipio_iIdMunicipio_seq"'::regclass);


--
-- Name: ODS iIdOds; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ODS" ALTER COLUMN "iIdOds" SET DEFAULT nextval('public."ODS_iIdOds_seq"'::regclass);


--
-- Name: Obra iIdObra; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Obra" ALTER COLUMN "iIdObra" SET DEFAULT nextval('public."Obra_iIdObra_seq"'::regclass);


--
-- Name: Oficio iIdOficio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Oficio" ALTER COLUMN "iIdOficio" SET DEFAULT nextval('public."Oficio_iIdOficio_seq"'::regclass);


--
-- Name: PED2019Eje iIdEje; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019Eje" ALTER COLUMN "iIdEje" SET DEFAULT nextval('public."PED2019Eje_iIdEje_seq"'::regclass);


--
-- Name: PED2019Estrategia iIdEstrategia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019Estrategia" ALTER COLUMN "iIdEstrategia" SET DEFAULT nextval('public."PED2019Estrategia_iIdEstrategia_seq"'::regclass);


--
-- Name: PED2019LineaAccion iIdLineaAccion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019LineaAccion" ALTER COLUMN "iIdLineaAccion" SET DEFAULT nextval('public."LineaAccion_iIdLineaAccion_seq"'::regclass);


--
-- Name: PED2019Objetivo iIdObjetivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019Objetivo" ALTER COLUMN "iIdObjetivo" SET DEFAULT nextval('public."PED2019Objetivo_iIdObjetivo_seq"'::regclass);


--
-- Name: PED2019Tema iIdTema; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019Tema" ALTER COLUMN "iIdTema" SET DEFAULT nextval('public."PED2019Tema_iIdTema_seq"'::regclass);


--
-- Name: Partida iIdPartida; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Partida" ALTER COLUMN "iIdPartida" SET DEFAULT nextval('public."Partida_iIdPartida_seq"'::regclass);


--
-- Name: Permiso iIdPermiso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Permiso" ALTER COLUMN "iIdPermiso" SET DEFAULT nextval('public."Permiso_iIdPermiso_seq"'::regclass);


--
-- Name: Rol iIdRol; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Rol" ALTER COLUMN "iIdRol" SET DEFAULT nextval('public."Rol_iIdRol_seq"'::regclass);


--
-- Name: Suficiencia iIdSuficiencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Suficiencia" ALTER COLUMN "iIdSuficiencia" SET DEFAULT nextval('public."Suficiencia_iIdSuficiencia_seq"'::regclass);


--
-- Name: UBP iIdUBP; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UBP" ALTER COLUMN "iIdUBP" SET DEFAULT nextval('public."UBP_iIdUbp_seq"'::regclass);


--
-- Name: UGI iIdUGI; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UGI" ALTER COLUMN "iIdUGI" SET DEFAULT nextval('public."UGI_iIdUgi_seq"'::regclass);


--
-- Name: Unidad iIdUnidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Unidad" ALTER COLUMN "iIdUnidad" SET DEFAULT nextval('public."Unidad_iIdUnidad_seq"'::regclass);


--
-- Name: Usuario iIdUsuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Usuario" ALTER COLUMN "iIdUsuario" SET DEFAULT nextval('public."Usuario_iIdUsuario_seq"'::regclass);


--
-- Data for Name: Apertura; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Apertura" ("iIdApertura", "vDescripcion", "nImporte", "iAnio", "iActivo") FROM stdin;
\.


--
-- Data for Name: Categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Categoria" ("iIdCategoria", "vCategoria", "iActivo", "vAbreviacion") FROM stdin;
1	Acciones de vivienda	1	vivienda
2	Agua potable	1	agua
3	Alcantarillado y saneamiento	0	\N
4	Ampliación de la red eléctrica	1	red elec
5	Caminos sacacosechas	1	sacacosechas
6	Conservación de carreteras	1	conservación
7	Construcción de sanitarios	1	sanitarios
8	Construcción y reconstrucción de calles	1	calles
9	Construcción, rehabilitación y equipamiento de escuelas nivel básico	1	básico
10	Construcción, rehabilitación y equipamiento de escuelas nivel medio superior	1	medio
11	Construcción, rehabilitación y equipamiento de escuelas nivel superior	1	superior
12	Infraestructura para el medio ambiente	1	medio amb
13	Infraestructura cultural	1	cultura
14	Infraestructura de descentralización regional de servicios	0	\N
15	Infraestructura de justicia	1	justicia
16	Infraestructura de salud	1	salud
17	Infraestructura de seguridad	1	seguridad
18	Infraestructura gubernamental	1	guber
19	Infraestructura deportiva	1	deporte
20	Infraestructura turística	1	turística
21	Innovación y desarrollo tecnológico	1	innovación
22	Mitigación y adaptación contra el cambio	0	\N
23	Construcción y modernización de carreteras	1	carreteras
24	Parques urbanos	0	\N
25	Preservación del patrimonio arquitectónico	0	\N
26	PUENTES, LIBRAMIENTOS E INFRAESTRUCTURAS URBANAS	0	\N
27	Recuperación de playas	0	\N
28	Infraestructura urbana	1	urbana
29	Infraestructura estratégica	1	estratégica
30	Obras con recurso federal transferido a los municipios	1	recurso transferido
31	Infraestructura productiva	1	productiva
32	Estudios y proyectos	1	estudios
\.


--
-- Data for Name: Compromiso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Compromiso" ("iIdCompromiso", "vCompromiso", "iActivo") FROM stdin;
1	Impulsar la atracción de inversiones y creación de empresas al interior del estado	1
2	Fomentar la generación de empleos formales para jóvenes	1
3	Promover a Yucatán como capital de la guayabera e impulsar el sector textil en la región	1
4	Consolidar la Semana de Yucatán en México como parte de la promoción turística del estado	1
5	Impulsar la estrategia Hecho en Yucatán 	1
6	Gestionar ante el gobierno federal la modernización del Puerto de Altura de Progreso 	1
7	Promover en los municipios del estado actividades de atracción turística acordes a su vocación	1
8	Conjuntar esfuerzos para un Yucatán con mayor desarrollo turístico 	1
9	Impulsar la promoción turística internacional del Mundo Maya, a fin de generar pernocta en Valladolid, Izamal, Chichén Itzá, Uxmal, entre otros 	1
10	Promover a Yucatán como sede de eventos internacionales	1
11	Postular a Yucatán como sede del Tianguis Turístico	1
12	Reestructurar el Comité de Sanidad Animal	1
13	Establecer el Consejo Peninsular de Sanidad e Inocuidad Agroalimentaria (CPSIA)	1
14	 Fortalecer el sector agroindustrial y empleo en la zona rural	1
15	Desarrollar proyectos de vinculación para lograr la producción del henequén 	1
16	Apoyar la planta de procesamiento cítrico del municipio de Oxkutzcab 	1
17	Apoyar el procesamiento del chile habanero a través de la puesta en marcha de un consejo regulador	1
18	Fomentar el uso de semilla certificada de maíz, soya y sorgo para  aumentar la calidad 	1
19	Garantizar que los listados de los productos apoyados a través de los programas  del estado, tengan precios por debajo del mercado 	1
20	Impulsar la modernización de la actividad agropecuaria	1
21	Fomentar el abasto y la sustentabilidad energética 	1
22	Otorgar facilidades para la mecanización del campo	1
23	Construir, rehabilitar y modernizar caminos saca cosechas	1
24	Fomentar la industria apícola y la comercialización de la miel y sus derivados	1
25	Gestionar la obtención de una Indicación Geográfica o la Denominación de Origen para fortalecer la industria del procesamiento de miel 	1
26	Impulsar el comercio justo para  la industria apícola 	1
27	Crear la Secretaría de Pesca y Acuacultura Sustentables de Yucatán	1
28	Fomentar la diversificación de las actividades económicas en la costa durante la época de veda	1
29	Crear el Festival de la Veda 	1
30	Generar apoyos económicos o en especie a pescadores	1
31	Actualización del padrón de beneficiarios del programa Veda del Mero	1
32	Promover el consumo de las especies endémicas	1
33	Implementar programas de repoblación a través de la acuacultura	1
34	Promover proyectos productivos para dar valor agregado a los productos pesqueros	1
35	Otorgar créditos dirigidos a proyectos productivos del interior del estado para micros y pequeñas empresas	1
36	Impulsar los centros de emprendimiento en el interior del estado	1
37	Impulsar esquemas de financiamiento y apoyos en especie a empresas constituidas por mujeres, así como impulsar el empoderamiento de la mujer en el sector laboral del estado	1
38	Creación de un área en el Gobierno del estado con facultades y atribuciones relacionadas a promover asuntos energéticos	1
39	Promover los huertos urbanos comunitarios al interior del estado como parte de la estrategia de combate a la carencia alimentaria	1
40	Promover el desarrollo de clúster agrícolas 	1
41	Impulsar acciones y eventos para generación de productos de gastronomía yucateca	1
42	Promover el Programa Médico a Domicilio y ampliar su disponibilidad en el interior del estado	1
43	Ampliación de horario de atención médica a 24 horas del día los 7 días de la semana en las cabeceras municipales	1
44	Crear una estrategia educativa para informar y sensibilizar sobre cuidados infantiles, crianza respetuosa y estimulación favoreciendo el desarrollo de entornos familiares  	1
45	Prestaciones para todos los niños y niñas que se atienden en el sistema público de salud	1
46	Fortalecer la entrega de raciones alimenticias de los programas sociales dirigidos  a niños y niñas con calidad nutricia y promover la lactancia exclusiva durante los primeros 6 meses de vida	1
47	Promover la participación coordinada de la sociedad civil en programas y campañas de salud para fortalecer la atención integral de enfermedades de importancia en salud pública	1
48	Impulsar programas que reduzcan los niveles de obesidad en niñas y niños	1
49	Impulsar acciones de promoción de estilos de vida saludable para reducir la prevalencia e incidencia del sobrepeso, obesidad y diabetes para la población en general	1
50	Entrega de vehículos de traslado en el interior del estado	1
51	Promover la educación inclusiva con acciones que permitan la integración educativa y social de las y los estudiantes con necesidades educativas especiales o con alguna discapacidad	1
52	Promover que  el estado de Yucatán sea un estado bilingüe siendo la lengua maya  el segundo idioma oficial 	1
53	Impulsar una estrategia de capacitación y certificación de habilidades técnicas en el nivel medio superior que vayan acorde a la demanda laboral 	1
54	Impulsar el Sistema de Educación en línea a nivel medio superior y superior en el interior del estado	1
55	Ampliar la oferta de becas para las y los estudiantes de todos los niveles educativos, a través de programas diversificados que promuevan la equidad e impulsen mejores índices de eficiencia terminal.	1
56	Impulsar acciones que eleven la calidad y espacios de las viviendas priorizando los municipios con alta marginación 	1
57	Mejorar la atención médica de padecimientos por adicciones a través del Centro de Integración Juvenil para la atención de adicciones 	1
58	Ampliación del número de escuelas con el  Servicio de Apoyo a la Educación Regular (USAER) itinerantes  	1
59	Fomentar el aprendizaje musical y la educación artística	1
60	Promover la realización de eventos en los centros culturales con la participación de artistas, creadores y grupos artísticos locales en el interior del estado. 	1
61	Implementar programas destinados a la búsqueda de talentos jóvenes con la finalidad de formarse como agentes culturales del interior del estado. 	1
62	Impulsar a creadores en artes escénicas, artes visuales, música y literatura  para la generación de productos artísticos de calidad en  diferentes espacios culturales del interior del estado.	1
63	Difundir y promover a los Grupos Artísticos locales para su participación en festivales, ferias y diversos espacios culturales en el interior del estado	1
64	Promover la mejora de las casas de la cultura del estado 	1
65	Crear academias de béisbol en los 105 municipios del interior del estado	1
66	Crear academias de fútbol en los 105 municipios del interior del estado	1
67	Llevar caravanas con actividades deportivas y de activación física al interior del estado	1
68	Generar inclusión dentro de las actividades deportivas en el estado, para mejorar la calidad de vida en toda la sociedad 	1
69	Impulsar el desarrollo y la formación de deportistas de alto rendimiento	1
70	Promover acciones para la limpieza de la zona costera	1
71	Impulsar mecanismos intergubernamentales para conservar la biodiversidad del estado	1
72	Impulsar el desarrollo de una Economía Verde en el estado	1
73	Promover la cultura del medio ambiente desde la edad escolar para generar conciencia	1
74	Promover la creación de una ley que regule el uso eficiente y el cuidado del agua	1
75	Fomentar la planeación para el manejo integral de los residuos	1
76	Promover la regulación del uso de plástico en todo el estado (bolsas y popotes)	1
77	Mejora al sistema de transporte público de pasajeros en Mérida	1
78	Crear la Certificación de Operadores de Transporte y mejorar las condiciones de trabajo de los choferes 	1
79	Mejorar la movilidad urbana en área metropolitana de Mérida 	1
80	Impulsar la creación de un hospital y refugio público para mascotas en el municipio de Mérida	1
81	Fomentar la prevención de la violencia para las niñas y niños	1
82	Garantizar el derecho a la identidad de las y los niños para que tengan acceso a todos sus derechos civiles  	1
83	Impulsar acciones de atención a la discapacidad visual de niños y niñas 	1
84	Promover estrategias de prevención y atención del bullying 	1
85	Crear el Instituto para la Inclusión de las  Personas con Discapacidad del Estado de Yucatán	1
86	Crear la Secretaría de la Mujeres en el estado de Yucatán para fortalecer los derechos humanos de las mujeres e impulsar acciones para la igualdad de género	1
87	Impulsar el observatorio de la mujer sobre los casos de violencia de género (banco de datos)	1
88	Gestionar el establecimiento de Instancias Municipales de las mujeres  al interior del estado	1
89	Fomentar acciones interinstitucionales para mejorar las estrategias de  prevención y atención de la violencia contra las mujeres	1
90	Implementar una campaña informativa permanente en el marco de los derechos humanos de las mujeres en medios masivos de comunicación que promuevan la sensibilización antes las violencias hacía las mujeres 	1
91	Promover la modificación del Código Penal del Estado de Yucatán para incorporar como agravante de tipo penal en materia de feminicidio la calidad de edad de la víctima  	1
92	Promover que el delito de feminicidio no sea juzgado en juicio abreviado ni tenga derecho a reducción de pena, sin que esto implique excluir la reparación del daño	1
93	Certificar al personal encargado de la prevención y atención de las desigualdades por razones de género y la violencia hacia las mujeres, niñas, niños y adolescentes  	1
94	Promover que la violencia obstétrica se tipifique como un delito	1
95	Vincular las universidades con la capacitación laboral de la demanda industrial 	1
96	Crear un Padrón Único de servicios y programas sociales	1
97	Creación del Comité de Transparencia para vigilar el otorgamiento de plazas y horas  magisteriales que  garanticen la justicia y legalidad en los procesos de asignación	1
98	Adherirse a la Alianza por el Gobierno Abierto (AGA), suscrita por el Instituto Nacional de Acceso a la Información (INAI) en conjunto con la Sociedad Civil y el Gobierno del Estado.	1
99	Crear la fiscalía anticorrupción (elevar el nivel de vice fiscalía a fiscalía)	1
100	Diseñar y aplicar el principio de austeridad en la administración pública del estado a través de la optimización del gasto corriente	1
101	Actualización del marco normativo del proceso de adquisiciones de conformidad con estándares nacionales e internacionales de contrataciones abiertas 	1
102	 Optimizar los Recursos Humanos de la Administración Pública  	1
103	Crear el Consejo Consultivo del Presupuesto y Ejercicio del Gasto Público de Yucatán 	1
104	Fomentar el uso de las Tics en los procesos de adquisiciones según estándares nacionales e internacionales de contrataciones abiertas  	1
105	 Difundir la información sobre contratos y licitaciones en formato de datos abiertos según estándares nacionales e internacionales   	1
106	Simplificar y digitalizar los trámites y servicios de la administración pública estatal	1
107	Crear la Ventanilla única que integre trámites de alto impacto del estado de Yucatán y el municipio de Mérida	1
108	Impulsar la política de mejora regulatoria para impulsar el desarrollo económico del estado alineados a mejores prácticas nacionales e internacionales 	1
109	Impulsar estrategias digitales que permitan implementar  un gobierno electrónico eficiente	1
110	Promover un gobierno que reduzca el uso de archivos de papel	1
111	Promover un gobierno que reduce el número de pagos en efectivo a empleados, proveedores, contratistas y pensiones  	1
112	Promover la actualización del Sistema de Información Estadística y Geográfica de Yucatán 	1
113	Facilitar el acceso a los servicios y pagos del gobierno a través de unidades de servicio electrónico y ventanilla única digital	1
114	Presentación de las declaraciones patrimoniales de los servidores públicos del estado	1
115	Expedir el Código de Ética de los Servidores Públicos del Estado 	1
116	Fomentar la publicación de las auditorías externas que por Ley deban de estar de manera transparente y accesibles para la ciudadanía	1
117	Simplificar los procedimientos para las contrataciones de obra pública con los estándares nacionales e internacionales de datos abiertos	1
118	Creación de la  Casa del Migrante Yucateco en Estados Unidos de América	1
119	Modernizar el Registro Público de la Propiedad a través del uso de tecnologías de la información	1
120	Modernizar el catastro del estado de Yucatán 	1
121	Mejorar y agilizar procesos y servicios del Registro Civil a través de diversas tecnologías de la información 	1
122	Proporcionar la asistencia de traductores e intérpretes mayas en los procesos de procuración de justicia a la población maya hablante para garantizar un acceso equitativo a la sociedad	1
123	Generar programas y acciones de mejoramiento y acceso a la vivienda para policías 	1
124	Mantener e incrementar la seguridad pública en el estado 	1
125	Fortalecer el programa integral de prevención del delito	1
126	Reforzar la certeza jurídica del patrimonio de los ciudadanos en pobreza extrema	1
127	Actualización de la Ley de Asentamientos Humanos del Estado de Yucatán	1
128	Actualizar el Programa Estatal de Desarrollo Urbano	1
129	Crear un código urbano estatal que vigile la legislación en materia de desarrollo urbano y sustentabilidad	1
130	Elaborar el Programa de Desarrollo Urbano de la Zona Metropolitana de Mérida	1
131	Formular lineamientos y programas de elaboración de planes de desarrollo urbano en los municipios con menos de 50 mil habitantes 	1
132	Instalar internet gratuito en las plazas  municipales al interior del estado	1
133	Gestionar ante el gobierno federal acciones de modernización y ampliación de la infraestructura carretera	1
134	Impulsar la conectividad del estado a través de la construcción de vías de comunicación al interior del estado	1
135	Impulsar el desarrollo logístico del estado a través de acciones de mejora de la infraestructura ferroviaria y aeroportuaria	1
136	Construcción de Caminos Sacacosechas al interior del estado	1
\.


--
-- Data for Name: Contrato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Contrato" ("iIdContrato", "vContrato", "vRuta", "iIdObra", "iActivo") FROM stdin;
\.


--
-- Data for Name: Convenio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Convenio" ("iIdConvenio", "vNombreArchivo", "vRuta", "iIdObra", "iActivo") FROM stdin;
\.


--
-- Data for Name: Coordenada; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Coordenada" ("iIdObra", "nPunto", "iTipo", "nLat", "nLong", "nNombre") FROM stdin;
\.


--
-- Data for Name: Dependencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Dependencia" ("iIdDependencia", "vDependencia", "vSiglas", "iActivo") FROM stdin;
1	Secretaría Técnica de Planeación y Evaluación	SOP	1
2	COMEY	COMEY	0
3	CONACULTA	CONACULTA	0
4	CONADE	CONADE	0
5	CONCITEY-FOMIX	CONCITEY-FOMIX	0
6	IDEFEY	IDEFEY	1
7	INCAY	INCAY	1
8	INCCOPY	INCCOPY	1
9	IVEY	IVEY	1
10	JAPAY	JAPAY	1
11	JEDEY	JEDEY	1
12	INDERM	INDERM	1
13	SCT	SCT	0
14	SDS	SDS	1
15	SEFOTUR	SEFOTUR	0
16	SEGEY	SEGEY	1
17	SHCP	SHCP	0
18	SSY	SSY	1
19	UADY	UADY	1
20	DESPACHO DEL GOBERNADOR	DESPACHO DEL GOBERNADOR	0
21	SEPLAN	SEPLAN	1
22	FGE	FGE	0
23	SSP	SSP	1
24	CONSEJERIA JURIDICA	CONSEJERIA JURIDICA	0
25	DGTI	DGTI	0
26	SAF	SAF	0
27	IMDUT	IMDUT	1
28	S TECNICA	S TECNICA	0
29	MUNICIPIOS	MUNICIPIOS	1
30	SEDESOL	SEDESOL	1
31	SOP1	SOP1	0
32	SEFOE	SEFOE	0
33	FISCALIA	FISCALIA	0
34	FISCALIA GENERAL	FISCALIA GENERAL	0
35	GOBIERNO	GOBIERNO	0
36	SEDER	SEDER	1
37	Poder Judical	Poder Judical	0
38	SEDECULTA	SEDECULTA	0
39	SECOGEY	SECOGEY	1
40	CONAGUA	CONAGUA	0
41	Coordinación de Proyectos Estratégicos	Coordinación de Proyectos Estratégicos	1
42	ISSTEY	ISSTEY	1
43	IYEM	IYEM	0
44	SIIES	SIIES	0
45	IPFY	IPFY	1
46	CULTUR	CULTUR	1
47	CJ	CJ	0
48	UGI (SAF)	UGI (SAF)	1
49	DESPACHO DEL GOBERNADOR	DESPACHO DEL GOBERNADOR	1
50	SAF	SAF	1
51	Secretaría de Investigación, Innovación y Educación Superior (SIIES)	Secretaría de Investigación, Innovación y Educación Superior (SIIES)	1
\.


--
-- Data for Name: Financiamiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Financiamiento" ("iIdFinanciamiento", "vFinanciamiento", "iAnio", "iActivo") FROM stdin;
1	2% Fondo de Previsión Presupuestal del Seguro Popular	2015	0
2	Sistema de Protección Social en Salud - Seguro Popular	2015	0
3	Programa de Agua Potable, Alcantarillado y Saneamiento en Zonas Urbanas 	2015	0
4	BID-BANOBRAS	2015	0
5	Banco Interamericano de Desarrollo BID - El Banco Nacional de Obras y Servicios Públicos BANOBRAS	2015	0
6	Fondo de  cultura	2015	0
7	Programa de Infraestructura Deportiva	2015	0
8	Programa de esquema de financiamiento y subsidio federal para vivienda	2015	0
9	CONVENIO EDUCACION	2015	0
10	Convenio SCT	2015	0
11	Proyectos de Infraestructura de Turismo	2015	0
12	Fondo Metropolitano	2015	0
13	Fondo de Aportaciones para el Fortalecimiento de las Entidades Federativas 	2015	0
14	FAFEF RAMO 39	2015	0
15	FAM Infraestructura Educativa Básica	2015	0
16	FAM Infraestructura Educativa Media Superior y Superior	2015	0
17	FIMEY	2015	0
18	FISE	2015	0
19	Fondo de Infraestructura Social para Entidades	2015	0
20	Fideicomiso Fondo Mixto Fomix	2015	0
21	Fondo Nacional de Habitaciones Populares	2015	0
22	Fondo de Pavimentacio, Espacios Deportivos, Alumbrado Público y Rehabilitación de Infraestructura Educativa	2015	0
23	Programa de Apoyo para Fortalecer la Calidad de los Servicios de Salud	2015	0
24	Programa Medio Ambiente y Recursos Naturales	2015	0
25	PIBAI	2015	0
26	Proyecto Institucional de Fortalecimiento de los Institutos Tecnológicos	2015	0
27	Proyecto de Desarrollo Regional	2015	0
28	PRODER	2015	0
29	Fondo de Apoyo para la Infraestructura y Seguridad	2015	0
30	Programa para la Sostenibilidad de los Servicios de Agua Potable y Saneamiento en Comunidades Rurales	2015	0
31	Programa de Tratamiento de Aguas Residuales	2015	0
32	Recurso Propio	2015	0
33	Fondo Concursable de Inversión en Infraestructura para Educación Media Superior	2015	0
34	Fortalecimiento de la gestión institucional	2015	0
35	RECURSOS PROPIOS UTM 2012	2015	0
36	Proyecto ampliación de oferta educativa	2015	0
37	APOYO A PLANTELES MEDIA SUPERIOR	2015	0
38	Programa de infraestructura para la educación media superior	2015	0
39	Fondo Concurrente para el Incremento de la Matricula en Educación Superior	2015	0
40	Programas Regionales	2015	0
41	Equipamiento	2015	0
42	Equipamiento	2015	0
43	Aportación beneficiario	2015	0
44	Agencias Productoras de Vivienda,  Sociedad Hipotecaria Federal S.N.C. para operar el proyecto de Autoproducción Asistida.	2015	0
45	DIRECTAMENTE RECAUDADOS	2015	0
46	RECURSO ISSTEY	2015	0
47	Programa Escuelas Dignas	2015	0
48	SOCIEDAD HIPOTECARIA FEDERAL	2015	0
49	FONDO DE APOYO PARA LA MICRO, PEQUEÑA Y MEDIANA EMPRESA	2015	0
50	CANIETI	2015	0
51	Aportación municipal	2015	0
52	Programa para el Desarrollo de Zonas Prioritarias	2015	0
53	Ramo 33 Aportaciones Federales para Entidades Federativas y Municipios	2015	0
54	FONDO PROGRAMAS REGIONALES	2015	0
55	Fondo de Aportaciones para la Seguridad Pública de los Estados 	2015	1
56	CONVENIO SEDESOL	2015	0
57	RAMO 23	2015	0
58	Programa Nacional de Prevención al Delito	2015	0
59	Programa Deporte	2015	0
60	CONAVIN	2015	0
61	RAMO 04	2015	0
62	Programa de Infraestructura Indígena (2015)	2015	1
63	Fondo de Aportaciones para la Educación Básica y Normal (FAEB)	2015	0
64	Implementación del sistema de justicia penal en las entidades federativas	2015	0
65	Fondo de Contingencias Económicas	2015	0
66	PRODER municipal	2015	0
67	Programa Hídrico	2015	0
68	Programa de financiamiento para la infraestructura y seguridad pública (PROFISE)	2015	0
69	Programa de escuela de tiempo completo	2015	0
70	Convenio FGE	2015	0
71	Convenio Instituto promotor de ferias	2015	0
72	Convenio UADY	2015	0
73	Convenio SEFOTUR	2015	0
74	Convenio SEFOE	2015	0
75	Convenio IDEY	2015	0
76	Convenio DIF	2015	0
77	Convenio SEDECULTA	2015	0
78	Programa de Mejoramiento de Eficiencias de organismos Operadores PROME	2015	0
79	Programa Rescate de Espacios Públicos	2015	0
80	Fondo del Sur Sureste	2015	0
81	PEF 2014	2015	0
82	Anexo IV	2015	0
83	Fondo de Aportaciones para los Servicios de Salud	2015	0
84	PROSPERA	2015	0
85	Ramo 20 Desarrollo Social y Humano	2015	0
86	Proyectos para el desarrollo regional de la zona henequenéra del sureste	2015	0
87	Programa de medio ambiente y recursos naturales	2015	0
88	Fideicomiso del Sistema de Salud	2015	0
89	Programa para el desarrollo regional turístico sustentable	2015	0
90	Convenio CDI	2015	0
91	INJUVE	2015	0
92	SEDESOL PET	2015	0
93	Otros	2015	0
94	Convenio CFE	2015	0
95	Fondo de Pavimentación y  Desarrollo Municipal	2015	0
96	Fondo de Infraestructura Deportiva	2015	0
97	PEF	2015	0
98	CONADE	2015	0
99	Ramo 07 Defensa Nacional	2015	0
100	Convenio SSY	2015	0
101	Subsidios para contigencias económicas	2015	0
102	Recuros Federal	2015	0
103	INNOVEC	2015	0
104	PROEXOE EMS	2015	0
105	Fondo de Aportaciones Múltiples para Educación Básica	2015	0
106	Convenio IDEFEY	2015	0
107	Certificados de Infraestructura Escolares Nacionales CIEN	2015	0
108	Ramo 23 Provisiones Salariales y otras provisiones	2015	0
109	Comisión Nacional de Vivienda CONAVI	2015	0
110	Recurso Propio	2016	1
111	Fondo de Aportaciones para el Fortalecimiento de las Entidades Federativas (FAFEF)	2016	1
112	Fondo de Aportaciones para la Nómina Educativa FONE	2016	0
113	Fondo de Aportaciones para los Servicios de Salud (FASSA) 	2016	0
114	Fondo de Aportaciones Múltiples Media Superior (FAM Superior)	2016	1
115	Fondo de Infraestructura Social para Entidades (FISE)	2016	1
116	FAM Asistencia Social RAMO 33	2016	0
117	Fondo de Aportaciones Múltiples para la Infraestructura Educativa  Básica (FAM-IE Básica)	2016	1
118	Fondo de Aportaciones Múltiples para la Infraestructura Educativa Superior (FAM-IE Superior)	2016	1
119	Fondo de Aportaciones para la Educación Tecnológica y de Adultos FAETA RAMO 33	2016	0
120	Fortalecimiento a Municipios RAMO 33	2016	0
121	Fondo de Infraestructura Social Municipial FISM RAMO 33	2016	0
122	Fondo de Aportaciones para la Seguridad Pública de los Estados (FASP) 	2016	1
123	Fondo de Apoyos para el Fortalecimiento de las Entidades Federativas (FAFEF años anteriores) 	2016	1
124	UADY Subsidio Federal	2016	0
125	Fondo Sur Sureste (Fonsur)	2016	1
126	Convenio parte Estatal	2016	0
127	Programa para la Fiscalización del Gasto Federalizado PROFIS	2016	0
128	Fondo Metropolitano de Yucatán	2016	1
129	Fondo de Estabilización de los Ingresos de las Entidades Federativas AÑOS ANTERIORES	2016	0
130	Fondo para la Accesibilidad para las Personas con discapacidad (FOADIS 2014)	2016	0
131	Proyectos de Desarrollo Regional	2016	1
132	Subsidios para Contigencias Económicas	2016	1
133	Implementación de la Reforma Penal	2016	1
134	Recursos Propios de las Entidades Paraestatales	2016	0
135	Financiamiento	2016	0
136	Proyectos para el Desarrollo Regional de la Zona Henequenera del Sureste (Yucatán)	2016	0
137	Aportación Solidaria Estatal	2016	0
138	Programa Nacional de Fomento a la Lectura AÑOS ANTERIORES	2016	0
139	Programa para el Desarrollo Integral de las Culturas de los Pueblos y Comunidades	2016	0
140	Subsidio para el Colegio de Estudios Cinetíficos y Tecnológicos del Estado de Yucatán CECITEY	2016	0
141	Subsidio para la Implementación de la Reforma del Sistema de Justicia Penal	2016	0
142	Subsidio para el Colegio de Bachilleres del Estado de Yucatán COBAY	2016	0
143	Programa de Estrategia Integral de Desarrollo Comunitario Comunidad Diferente	2016	0
144	Programa de Antención a Personas con Discapacidad	2016	0
145	Programa de Protección y Desarrollo Integral de la Infancia	2016	0
146	Programa de Fortalecimiento de las Procuradurias de la Defensa del Menor y la Familia	2016	0
147	Programa de Atención a Familias y Población Vulnerable	2016	0
148	Programa Nacional de Becas	2016	0
149	Sistema Mexicano del Deporte de Alto Rendimiento	2016	0
150	Anexo específico del convenio de Coordinación y Colaboración Con la Comisión Nacional del Deporte (CONADE)	2016	0
151	Programa Deporte	2016	0
152	Programa de Atención a la Demanda IEAEY	2016	0
153	Programa de Apoyo a las Instancias de Mujeres en las Entidades Federativas para Ejecutar Acciones de Prevención de la Violencia Contra las Mujeres (PAIMEF)	2016	0
154	Programa de Fortalecimiento a la Transversalidad de la Perspectiva de Género	2016	0
155	Programa para el Mejoramiento de la Producción y la Productividad Indígena	2016	0
156	Programa de Apoyo a la Vivienda antes (FONHAPO )	2016	1
157	Programa de Esquemas de Financiamiento y Subsidio Federal para Vivienda Esta es tu Casa	2016	0
158	Programa de Agua Limpia, Alcantarillado y Saneamiento  (PROAGUA)(Antes Apazu,Prome, Prossapys y Agua Limpia)	2016	1
159	Programa de Saneamiento (PROSAN) (Antes PROTAR)	2016	1
160	Programa de Concurrencia con Entidades Federativas	2016	0
161	Extensión e Innovación Productiva	2016	0
162	Conservación y Uso Sustentable de Suelo y Agua COUSSA	2016	0
163	Programa de Sanidad e Inocuidad Agroalimentaria	2016	0
164	Proyecto Estratégico de Seguridad Alimentaria PESA	2016	0
165	Proyectos de Cultura (PEF)	2016	0
166	Programa de Infraestructura Indígena (PROII)	2016	1
167	Programa de Medio Ambiente y Recursos Naturales	2016	1
168	Programa de Desarrollo Regional Turístico Sustentable y Pueblos Mágicos 	2016	1
169	Apoyos Financieros para la Universidad de Oriente	2016	0
170	Proyectos de Construcción de Carreteras Alimentadoras  y Caminos Rurales (Ampliaciones determinadas por la Cámara de Diputados)	2016	1
171	Programa Nacional de Prevención del Delito (PRONAPRED)	2016	0
172	Apoyos Financieros para la Universidad Tecnólogica Metropolitana	2016	0
173	Apoyos Financieros para la Universidad Técnologica Regional del Sur	2016	0
174	Apoyos Financieros para la Universidad Técnologica del Centro	2016	0
175	Apoyos Financieros para la Universidad Técnologica del Poniente	2016	0
176	Apoyos Financieros para la Universidad Tecnológica del Mayab	2016	0
177	Convenio de colaboración entre la SHCP por conducto de la Unidad de Contabilidad Gubernamental Previsión para la Armonización Contable PEF 2015 Conac	2016	0
178	Subsidio para la Seguridad Pública Municipal SUBSEMUN	2016	0
179	Subsidio para el Fortalecimiento de las Instituciones de Seguridad Pública en Materia de Mando Policial (Policía Estatal Acreditable o SPA)	2016	0
180	Fondo para el Fortalecimiento de Acciones de Salud Pública en las Entidades Federativas (AFASPE)	2016	0
181	Unidades Médicas Móviles	2016	0
182	Programa Seguro Médico Siglo XXI	2016	0
183	Programa de Protección Social en Salud (Seguro Popular)Fideicomiso 2%	2016	1
184	Convenio para la Ejecución de Acciones de Prospera Programa de Inclusión Social	2016	0
185	Fondo de Protección Contra Gastos Catastróficos	2016	0
186	Programa de Apoyo al Empleo	2016	0
187	Instituciones Estatales de Cultura	2016	0
188	Programa de Movilidad Laboral	2016	0
189	Programa Escuelas de Tiempo Completo	2016	0
190	Apoyo al Fortalecimiento de Instancias Estatales de Juventud	2016	0
191	Espacios Poder Joven	2016	0
192	Red Nacional de Programas de Radio y Televisión Poder Joven y Radio por Internet	2016	0
193	Convenio de Coordinacion para la Creación, Operación y Apoyo Financiero de los i	2016	0
194	Laboratorio Estatal de Salud Pública COFEPRIS	2016	0
195	Programa de Apoyo para Fortalecer la Calidad de los Servicios de Salud	2016	0
196	Impulso a la Capitalización Pesquera y Acuícola	2016	0
197	Componente de Atención a Desastres Naturales en el Sector Agropecuario	2016	0
198	Programas de Carácter Cultural y Artístico (CONACULTA)	2016	0
199	Modernizacion Integral del Registro Civil	2016	0
200	Programa de Modernización y Vinculación Registral y Catastral del Padrón In	2016	0
201	Programa de Fondo Concursable de Infraestructura para la Educación Media Superior	2016	1
202	Programa Nacional de Inglés en la Educación Básica (PNIEB)	2016	0
203	Programa  de Mejoramiento del Profesorado (PROMEP)	2016	0
204	Programa Integral de Fortalecimiento Institucional (PIFI)	2016	0
205	Apoyo Financiero	2016	0
206	Fondo para Ampliar y Diversificar la Oferta Educativa en Educación Superior FADOE	2016	0
207	Convenio de Apoyo Financiero para el Programa de Carrera Docente	2016	0
208	Programa Nacional de Fomento a la Lectura	2016	0
209	Programa de Estímulo a la Creación y al Desarrollo Artístico	2016	0
210	Programa de Apoyo a las Culturas Municipales y Comunitarias PACMYC	2016	0
211	Programa de Desarrollo Cultural Municipal	2016	0
212	Convenio 5% Museos o Zonas Arqueológicas	2016	0
213	Programa de Desarrollo Cultural Infantil del Estado de Yucatán  Alas y Raíces	2016	0
214	Convenio de Colaboración de Apoyos del Fondo Nacional Emprendedor	2016	0
215	Proyectos de Gestión de Residuos sólidos en el Estado de Yucatán	2016	0
216	Desarrollo Estratégico de la Acuacultura	2016	0
217	Programa de Fomento a la Agricultura	2016	0
218	Convenios de Infraestructura Municipal	2016	0
219	Programa de Apoyos a la Cultura	2016	0
220	Unidad Regional de Culturas Populares del Estado de Yucatán	2016	0
221	Recursos Hídricos en el Ámbito del Consejo de Cuenca Península de Yucatán Mediante el Fortalecimiento de su Gerencia Operativa	2016	0
222	Convenio de Coordinación en Materia de Prevención Combate y Control de Incendios Forestales celebrado entre la Comisión Nacional Forestal y el Gobierno del Estado	2016	0
223	Convenio de Colaboración para el Desarrollo del Programa de Estímulos a la Investigación de Desarrollo Tecnológico e Innovación PEI del CONACYT 2015	2016	0
224	Convenio Específico Cresca Conadic Cenadic Yuc 001 2015	2016	0
225	Información Estadística y Estudios (SNIDRUS)	2016	0
226	Programa Rehabilitación Modernización Tecnificación y Equipamiento de Unidades de Riego	2016	0
227	Programa de Infraestructura	2016	0
228	Convenio de Coordinación para la creación, operación y  apoyo financiero del ICATY	2016	0
229	Programa de Expansión de la Oferta Educativa en la Educación Media Superior y Superior (PROEXOEES)	2016	1
230	Certificado de Infraestructura Educativa Nacional CIEN	2016	1
231	Fondo para el Fortalecimiento de la Infraestructura Estatal y Municipal (Fortalece)	2016	1
232	Programa Derechos Indígenas IEGY	2016	0
233	Programa 3 x 1 Atención a Migrantes	2016	1
234	Provisiones Salariales RAMO 23	2016	0
235	Provisiones Salariales RAMO 24	2016	0
236	Provisiones Salariales RAMO 25	2016	0
237	Convenio Gobierno del Estado - SCT	2016	0
238	Convenio de colaboración Gobierno del Estado - SEMARNAT	2016	1
239	Desarrollo Territorial y Urbano RAMO 15	2016	0
240	Programa de Fondo Concursable de Infraestructura para la Educación Media Superior	2016	0
241	Programa de agua limpia, alcantarillado y saneamiento Rural APARURAL	2016	0
242	Programa de agua limpia, alcantarillado y saneamiento Urbano APAUR	2016	0
243	Programa de desarrollo integral de organismos operadores PRODI	2016	0
244	Programa de Saneamiento PROSAN	2016	0
245	Programa de Agua Limpia, Alcantarillado y Saneamiento Rural (PROAGUA Rural) (Antes Prossapys).	2016	0
246	Programa de desarrollo integral de organismos operadores PRODI	2016	1
247	Programa de Acceso al Financiamiento para Soluciones  Habitacionales antes (CONAVI)	2016	1
248	Programa de Agua Limpia, Alcantarillado y Saneamiento (PROAGUA URBANO antes Apazu y Prome)	2016	1
249	Programa de Agua Limpia, Alcantarillado y Saneamiento (PROAGUA RURAL  antes PROSSAPYS)	2016	1
250	Programa de Agua Limpia, Alcantarillado y Saneamiento (Agua Limpia)	2016	1
251	Proyectos de Desarrollo Regional de la Zona Henequenera del Sureste Yucatán	2016	1
252	Proyectos de Desarrollo Regional de la Zona Henequenera -Ampliaciones-	2016	1
253	Proyectos de Desarrollo Regional (Municipios)	2016	1
254	Recursos Propios de las Entidades Paraestatales	2016	1
255	Programa de Infraestructura Ramo 15	2015	1
256	Fideicomiso para la Infraestructura en los Estados FIES	2015	1
257	Proyectos de Desarrollo Regional -Infraestructura Urbana de Mérida-	2015	1
258	Proyectos de Desarrollo Regional -Adicionales-	2015	1
259	Programa de Apoyo a la Cultura	2015	1
260	Fondo de Aportaciones Múltiples para la Infraestructura Educativa Básica FAM años anteriores	2015	1
261	Programas Regionales	2015	1
262	Programa de Empleo Temporal	2015	1
263	Fondo Mixto de Fomento a la Investigación Científica y Tecnológica Conacyt - FOMIX	2015	1
264	Fortalecimiento Financiero para Inversión 2016	2015	1
265	Fortalecimiento de la infraestructura y equipamiento a Escuelas de Tiempo Completo	2015	1
266	Comisión Nacional de Cultura Física y Deporte CONADE	2015	1
267	Fideicomiso de Infraestructura	2015	1
268	Aportación beneficiario	2015	1
269	Financiamiento	2015	1
270	Convenio SCT	2015	1
271	Fondo para el Fortalecimiento Financiero para Inversión	2017	1
272	PROGRAMA DE APOYOS PARA LA PROTECCIÓN DE LAS PERSONAS EN ESTADO DE NECESIDAD DEL SISTEMA NACIONAL PARA EL DIF	2017	1
273	Aportación municipal	2017	1
274	Secretaria de Desarrollo Social - Ramo 20	2017	1
275	Fideicomiso PROTEGO	2017	1
276	AMPLIACIÓN	0	1
277	SEDATU	2018	1
278	Aportación Solidaria Estatal Liquida	2019	1
279	Programa Fortalecimiento de la Calidad Educativa PACTEN	2019	1
280	FAFEF (1)	2019	1
281	PRESUPUESTO DE EGRESOS DE LA FEDERACIÓN (PEF)	2019	1
282	Recursos para Acciones de Fortalecimiento de la Infraestructura Física de la Cuota Social y Aportación Solidaria Federal 2019	2019	1
\.


--
-- Data for Name: Imagen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Imagen" ("iIdObra", "vNombre", "vRuta", "iTipo") FROM stdin;
\.


--
-- Data for Name: Indicador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Indicador" ("iIdIndicador", "vIndicador", "iIdObjetivo") FROM stdin;
1	Tasa de Crecimiento del Valor de la Actividad Económica	28
2	Tasa de Crecimiento de las Actividades Comerciales en el estado	1
3	Crecimiento Promedio en el Índice de Productividad Laboral de las Actividades Terciarias	28
4	Lugar que ocupa el estado en el Índice de Competitividad Estatal	1
5	Promedio anual de la Inversión Extranjera Directa en Yucatán	28
6	Tasa de Crecimiento del Producto Interno Bruto Estatal del Sector Secundario	28
7	Crecimiento promedio en el Índice de Productividad Laboral en las Actividades Secundarias de Yucatán	28
8	Tasa de Crecimiento de las Actividades de Servicios de alojamiento temporal y de preparación de alimentos y bebidas en el estado	7
9	Crecimiento promedio de Visitantes con Pernocta en el estado	28
10	Estadía Promedio del Visitante en el estado	28
11	Tasa de Crecimiento del Valor de la Producción Pesquera en el estado	28
12	Tasa de Crecimiento del Valor de la Producción Agrícola en el estado	28
13	Tasa de Crecimiento del Valor de la Producción Pecuaria en el estado	28
14	Tasa de Crecimiento de las Actividades Primarias en el estado\n	30
15	Crecimiento Promedio de Trabajadores Independientes en el estado	1
16	Crecimiento promedio en el Índice Global de Productividad Laboral de la Economía Yucateca	1
17	Crecimiento Promedio de Trabajadores Asegurados ante el IMSS en Yucatán	28
18	Índice de Ciudades Prósperas de la Aglomeración Urbana de Mérida	30
19	Porcentaje de Población con Accesibilidad Muy Baja o Baja	30
20	Porcentaje de la Población Urbana que Habita en Viviendas Precarias	30
21	Porcentaje de Inversión en Obra Pública respecto del PIB Estatal	28
22	Índice de Suficiencia Vial	28
23	Variación Porcentual de la Longitud de la Red Ferroviaria	28
24	Promedio de llegada de pasajeros al Aeropuerto de Mérida	28
25	Porcentaje de Personas Usuarias de Internet	28
26	Porcentaje de Municipios con Actividades de Planeación Urbana	32
27	Porcentaje de Población en Situación de Pobreza en Yucatán	1
28	Porcentaje de Población en Situación de Pobreza Extrema	1
29	Porcentaje de Población con Carencia por Acceso a los Servicios de Salud	28
30	Tasa de Mortalidad	32
31	Tasa de Morbilidad	32
32	Porcentaje de Población con Carencia por Acceso a la Alimentación	19
33	Tasa de Desnutrición Severa	19
34	Porcentaje de Población con Seguridad Alimentaria	28
35	Porcentaje de Población en Municipios Indígenas en Situación de Pobreza	1
36	Porcentaje de Población en Municipios Indígenas en Situación de Pobreza Extrema	1
37	Porcentaje de Población con Carencia por Rezago Educativo	1
38	Promedio del Aprendizaje en Lenguaje y Comunicación y Matemáticas de la Prueba Planea Educación Primaria	30
39	Promedio del Aprendizaje en Lenguaje y Comunicación y Matemáticas de la Prueba Planea Educación Secundaria	30
40	Promedio del Aprendizaje en Lenguaje y Comunicación y Matemáticas de la Prueba Planea Educación Media Superior	30
41	Porcentaje de Población con Carencia por Calidad y Espacios en la Vivienda	30
42	Porcentaje de Población con Carencia por Servicios Básicos en la Vivienda	75
43	Porcentaje de Municipios con Asentamientos Humanos Irregulares	75
44	Porcentaje de Población con Carencia por Acceso a la Seguridad Social	28
46	Tasa de la Cobertura en Educación Superior	28
47	Porcentaje de la Matrícula inscrita en programas que pertenecen al Padrón Nacional de Programas Educativos de Calidad	30
48	Tasa de Investigadores por cada 100 mil habitantes	28
49	Subíndice de Innovación de los sectores económicos	28
50	Porcentaje de la Población de 18 años y más que considera segura su entidad	75
51	Índice de Paz México	75
52	Tasa de Incidencia Delictiva por cada 100 mil habitantes	1
53	Percepción sobre el Desempeño del Ministerio Público y Procuradurías como Muy Efectivo o Algo Efectivo	30
54	Porcentaje de Delitos Denunciados en el estado	1
55	Subíndice del Sistema Político	32
56	Variación Porcentual de Acuerdos Internacionales firmados por el estado	1
57	Tasa de Crecimiento del Valor de la Actividad Económica de Servicios de esparcimiento culturales y deportivos, y otros servicios recreativos	1
58	Índice de Recursos Culturales	28
59	Promedio de Participantes en Actividades Artísticas y Culturales	28
60	Porcentaje de la Población con Auto Adscripción Indígena	75
61	Tasa de Artistas y Grupos Artísticos por cada 100 mil habitantes	30
62	Porcentaje de Escuelas de Educación Básica que cuentan con Servicios Pedagógicos de Educación Artística	31
63	Tasa de Matrícula en Artes por cada 100 mil habitantes	1
64	Variación Porcentual de Personas que tienen Acceso a los Bienes y Servicios Culturales relacionados con el Patrimonio Cultural	75
65	Lugar que ocupa el estado en el Medallero de la Olimpiada Nacional del Deporte	30
66	Lugar que ocupa el estado en el Medallero de la Paralimpiada Nacional del Deporte	30
67	Incidencia de Obesidad	20
68	Promedio de Asistencia a Eventos Deportivos Profesionales	1
69	Lugar que ocupa Yucatán en el Subíndice Manejo Sustentable de Medio Ambiente	30
70	Porcentaje de Áreas Naturales Protegidas de competencia estatal que cuentan con Programa de Manejo actualizado y publicado	75
71	Lugar de Yucatán en el Índice de Competitividad Forestal	38
72	Lugar de Yucatán en el Índice de Vulnerabilidad al Cambio Climático de la producción forrajera ante estrés hídrico	1
73	Contaminación del aire PM2.5 microorganismos por metro cúbico	30
74	Lugar de Yucatán en Volumen tratado de Aguas Residuales por cada mil personas	30
75	Porcentaje de Agua Suministrada y Desinfectada para consumo humano en litros	30
76	Porcentaje de Residuos Sólidos confinados en sitios de disposición final	30
77	Lugar de Yucatán en Volumen de Residuos Sólidos generados	75
78	Variación Porcentual de Energía Limpia generada (GWh/a)	28
79	Lugar de Yucatán en Intensidad Energética de la economía	30
80	Calidad Bacteriológica del agua del mar de Yucatán	28
81	Lugar de Yucatán en la Tasa de Transporte Público por cada 10 mil vehículos en circulación	28
82	Índice de Movilidad Urbana	75
83	Índice General del Avance en la Implementación del Presupuesto basado en Resultados y del Sistema de Evaluación del Desempeño	30
84	Índice de Gobierno Abierto	30
85	Tasa de Incidencia de Corrupción	1
86	Porcentaje de Avance en la Sección del PbR-SED	30
87	Lugar que ocupa el estado en el Indicador Subnacional de Mejora Regulatoria	32
88	Lugar que ocupa el estado en la Clasificación del Doing Business	32
89	Nivel de Endeudamiento Estatal	75
90	Índice de Desarrollo Humano	28
91	Índice de Desarrollo Humano de Mujeres	28
92	Índice de Desarrollo Humano en Salud para las Mujeres	75
93	Índice de Desarrollo Humano en Educación para las Mujeres	75
94	Índice de Desarrollo Humano en Ingreso de las Mujeres	28
95	Tasa de Feminicidios por cada 100 mil Mujeres	75
96	Tasa de Participación Económica de personas con discapacidad de 15 años y más	28
97	Porcentaje de la Población de 5 a 14 años que realiza una Ocupación Infantil no permitida	28
98	Porcentaje de Participación Económica de la población de 15 a 29 años	28
99	Mediana del Ingreso Real de la Población Joven	28
100	Porcentaje de la Población total de Adultos de 65 y más que cuentan con Afiliación a Servicios de Salud	28
101	Coeficiente de variación del Índice DESCA	1
102	Índice de Moran del Índice DESCA	1
\.


--
-- Data for Name: Localidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Localidad" ("iIdLocalidad", "vLocalidad", "iIdMunicipio", "iActivo", "vClave") FROM stdin;
1	Abalá	1	1	0001
2	Benito Juárez Uno	1	1	0018
3	Cacao	1	1	0009
4	Don Cuxo	1	1	0057
5	Emiliano Zapata	1	1	0015
6	Inquietas Uno	1	1	0061
7	Miguel Hidalgo Uno	1	1	0042
8	Montecristo	1	1	0064
9	Mucuyché	1	1	0003
10	Niño de Atocha	1	1	0046
11	Onichén	1	1	0017
12	Peba	1	1	0008
13	San Isidro	1	1	0021
14	San Juan Tehbacal	1	1	0025
15	San Luis	1	1	0047
16	San Pedro Ochil	1	1	0023
17	Santa Gabriela	1	1	0037
18	Sihunchén	1	1	0005
19	Temozón	1	1	0006
20	Uayalceh	1	1	0007
21	Víctor	1	1	0027
22	Acanceh	2	1	0001
23	Candi	2	1	0021
24	Canicab	2	1	0002
25	Chakahil	2	1	0071
26	Cibceh	2	1	0003
27	Dzitiná	2	1	0014
28	El Buen Pastor	2	1	0048
29	El Pedregal	2	1	0023
30	El Pípila [Quinta]	2	1	0024
31	El Progreso	2	1	0041
32	Guadalupano	2	1	0032
33	Hulbeenkaax	2	1	0051
34	La Casa de la Cristiandad	2	1	0045
35	La Concepción	2	1	0049
36	La Conchita	2	1	0062
37	Las Concordias	2	1	0072
38	Las Margaritas	2	1	0079
39	Los Arturitos	2	1	0047
40	Los Cerros [Unidad Agrícola]	2	1	0070
41	Los Dos Hermanos	2	1	0050
42	Los Gavilanes	2	1	0022
43	Los Jalapeños	2	1	0030
44	Los Lirios	2	1	0040
45	Mega Servicios [Gasolinera]	2	1	0061
46	Ninguno [UAIM]	2	1	0069
47	Petectunich	2	1	0005
48	Plantel Siete	2	1	0054
49	Plantel Veintitrés (Boxtzalam)	2	1	0055
50	Sacchich	2	1	0006
51	Sahé [Paradero]	2	1	0052
52	Salvador Alvarado	2	1	0042
53	San Agustín Tres	2	1	0039
54	San Antonio	2	1	0025
55	San Emilio	2	1	0028
56	San Felipe	2	1	0029
57	San Gerónimo	2	1	0056
58	San Isidro	2	1	0044
59	San José	2	1	0031
60	San José II	2	1	0080
61	San José Uitzá	2	1	0046
62	San Lorenzo Pelé	2	1	0016
63	Santa Cruz	2	1	0026
64	Santa Gertrudis Tzuc	2	1	0007
65	Santa Isabel [Paraje]	2	1	0053
66	Santa María	2	1	0081
67	Tekik de Salazar Mendoza	2	1	0009
68	Tepich Carrillo	2	1	0010
69	Ticopó	2	1	0011
70	Yabucú	2	1	0012
71	Akil	3	1	0001
72	Benito Juárez	3	1	0042
73	Buenavista	3	1	0019
74	Cinco de Mayo	3	1	0051
75	Emaus [Iglesia]	3	1	0052
76	Independencia	3	1	0056
77	Juguera de Akil [Procesadora de Cítricos]	3	1	0040
78	Luis Echeverría Álvarez	3	1	0054
79	Morelos	3	1	0008
80	Nabanché	3	1	0026
81	Ox-Ac II	3	1	0028
82	Ox-Ac III	3	1	0058
83	Ox-Ac IV	3	1	0057
84	Plan Chac Pozo Cinco	3	1	0041
85	Plan Chac Pozo Cuatro	3	1	0043
86	Plan Chac Pozo Tres	3	1	0013
87	Plan Chac Uno y Dos	3	1	0006
88	San Anastacio	3	1	0053
89	San Antonio Hotcab	3	1	0030
90	San Ausencio	3	1	0049
91	San Bernardino	3	1	0055
92	San Bernardo	3	1	0044
93	San Diego	3	1	0003
94	San Esteban	3	1	0039
95	San Gabriel	3	1	0010
96	San Nicolás	3	1	0032
97	San Pablo Tres	3	1	0059
98	San Román	3	1	0033
99	San Víctor	3	1	0004
100	Santa Cruz	3	1	0016
101	Santa Rita	3	1	0038
102	Santa Teresa	3	1	0035
103	Técnica Treinta y Uno [Escuela]	3	1	0021
104	Baca	4	1	0001
105	Boxactún	4	1	0020
106	Candelaria	4	1	0090
107	Casa Roja	4	1	0091
108	Chácara Da Bugambilias	4	1	0053
109	Chan Gregor	4	1	0083
110	Chelito Lindo	4	1	0079
111	Cindy [Llantera]	4	1	0098
112	Colonia Agrícola	4	1	0059
113	Concepción Xtuc	4	1	0049
114	El Imán	4	1	0082
115	El Potrero	4	1	0072
116	Emporio	4	1	0062
117	Esmeralda	4	1	0080
118	Ex-Gasolinera	4	1	0094
119	Garza	4	1	0096
120	Jocoyoc	4	1	0063
121	Kan Mas	4	1	0035
122	Kankabchén	4	1	0034
123	Kankabchén Gamboa	4	1	0012
124	Kilómetro Diecinueve	4	1	0097
125	La Comunidad	4	1	0066
126	La Esperanza	4	1	0064
127	La Fe	4	1	0061
128	La Lupita	4	1	0067
129	La Morena	4	1	0105
130	La Santísima	4	1	0073
131	Las Palomas	4	1	0107
132	Las Trincheras	4	1	0065
133	Los Conejos [Club de Tiro]	4	1	0092
134	Los Flamboyanes	4	1	0095
135	Los Potrillos	4	1	0075
136	Los Tres Reyes	4	1	0071
137	Morales [Rancho]	4	1	0108
138	Nidia	4	1	0106
139	Ninguno [Bodega]	4	1	0089
140	Ninguno [Deshuesadero]	4	1	0093
141	Nok Ak	4	1	0041
142	Oasis	4	1	0016
143	Pacchén	4	1	0042
144	Petkan	4	1	0043
145	Poco a Poco I y II	4	1	0076
146	Put-Ku	4	1	0048
147	Ríos [Unidad Hortícola]	4	1	0081
148	Sabac Pochote	4	1	0077
149	San Antonio	4	1	0058
150	San Antonio Kiché	4	1	0013
151	San Carlos	4	1	0005
152	San Diego	4	1	0050
153	San Dimas	4	1	0019
154	San Ignacio Hierro Tunic	4	1	0030
155	San Isidro I	4	1	0031
156	San Isidro Kuxub	4	1	0004
157	San José	4	1	0060
158	San Lorenzo Boxactún	4	1	0037
159	San Manuel	4	1	0038
160	San Nicolás	4	1	0040
161	San Pedro	4	1	0017
162	San Roberto	4	1	0109
163	Sanjor	4	1	0056
164	Santa Concepción	4	1	0086
165	Santa Cruz	4	1	0069
166	Santa Cruz Chinic	4	1	0025
167	Santa Cruz Collí	4	1	0007
168	Santa Cruz Dziuché	4	1	0024
169	Santa Cruz Sábila	4	1	0046
170	Santa María	4	1	0008
171	Santa Rita	4	1	0044
172	Santo Domingo	4	1	0009
173	Siniltún	4	1	0070
174	Tixkuncheil	4	1	0010
175	Tres Hermanos	4	1	0078
176	Tumben Há	4	1	0112
177	X-Cocil	4	1	0014
178	Xoux	4	1	0047
179	Bokobá	5	1	0001
180	Mucuyché Campos	5	1	0002
181	San Antonio Ayín	5	1	0003
182	San Antonio Choil	5	1	0004
183	Felipe Carrillo Puerto	5	1	0007
184	Santo Domingo	5	1	0008
185	Chan Chen	5	1	0009
186	La Bendición de Dios	5	1	0016
187	Ayín [Granja]	5	1	0017
188	San Pedro	5	1	0018
189	Xulá	5	1	0019
190	Actún	6	1	0492
191	Actún Dzadz	6	1	0288
192	Actún-Chen	6	1	0304
193	África	6	1	0175
194	Arco Hopchén	6	1	0306
195	Argáez	6	1	0307
196	Balischén	6	1	0020
197	Bermejo	6	1	0215
198	Boxdzonot	6	1	0022
199	Buctzotz	6	1	0001
200	Buena Esperanza	6	1	0023
201	Buena Vista	6	1	0142
202	Buriches	6	1	0308
203	Can Dzo	6	1	0417
204	Casa Rosada	6	1	0465
205	Casco	6	1	0036
206	Cenote Azul	6	1	0024
207	Chabelo	6	1	0311
208	Chac	6	1	0312
209	Chacdzidzib	6	1	0025
210	Chan Dzonot	6	1	0249
211	Chan Luch	6	1	0275
212	Chan Motul	6	1	0015
213	Che'en Pato	6	1	0029
214	Chen Burro	6	1	0442
215	Chen Canul	6	1	0027
216	Chen Chomac Uno y Dos	6	1	0028
217	Chen Cutz	6	1	0194
218	Chen Kulú	6	1	0314
219	Chen Taller	6	1	0466
220	Chilitux	6	1	0030
221	Chumbilá	6	1	0241
222	Chumpilá	6	1	0494
223	Chun Copó	6	1	0154
224	Chun Yaxnic	6	1	0252
225	Chun-Abal	6	1	0017
226	Chuncopó	6	1	0225
227	Chunhuayún	6	1	0031
228	Chuntzalam	6	1	0146
229	Chunyah	6	1	0198
230	Cimé	6	1	0309
231	Cuba	6	1	0447
232	Cuch Há	6	1	0415
233	Don Isidro	6	1	0467
234	Dzachacá	6	1	0316
235	Dzadz Gallo	6	1	0318
236	Dzadz Halal	6	1	0035
237	Dzadz-Cun	6	1	0317
238	Dziben	6	1	0216
239	Dzitnup	6	1	0319
240	Dzitox	6	1	0037
241	Dzonot Brito	6	1	0166
242	Dzonot Lú	6	1	0321
243	El Cortijo	6	1	0065
244	El Crucero [Taquería]	6	1	0475
245	El Diamante	6	1	0032
246	El Dorado	6	1	0274
247	El Girasol	6	1	0322
248	El Lirio	6	1	0052
249	El Olvido	6	1	0223
250	El Palacio	6	1	0478
251	El Paraíso	6	1	0334
252	El Pedregal	6	1	0059
253	El Pobre	6	1	0477
254	El Pocito	6	1	0164
255	El Potrerito	6	1	0338
256	El Regalito	6	1	0339
257	El Sacrificio	6	1	0064
258	El Tumbo	6	1	0114
259	Granito de Oro	6	1	0006
260	Guadalupe	6	1	0039
261	Haydzonot	6	1	0040
262	Huene	6	1	0495
263	Jomo Dzadz Uno	6	1	0450
264	Jujum Pit	6	1	0047
265	Kakal	6	1	0050
266	Kakaldzonot	6	1	0273
267	Kakanhá	6	1	0327
268	Kaladzadz	6	1	0171
269	Kan Há	6	1	0123
270	Kanhá	6	1	0324
271	Kankirixché	6	1	0008
272	Kimen Mis	6	1	0157
273	Komchén	6	1	0328
274	Kusbá	6	1	0451
275	La Curva	6	1	0479
276	La Gran China	6	1	0323
277	La Gran Lucha	6	1	0004
278	La Herradura	6	1	0107
279	La Rosita	6	1	0341
280	La Ubre	6	1	0485
281	Las Margaritas	6	1	0452
282	Lol Bé	6	1	0329
283	Lolo	6	1	0330
284	Los Álamos	6	1	0202
285	Los Almendros	6	1	0493
286	Los Cocos	6	1	0310
287	Los Gavilanes	6	1	0183
288	Los Tres Hermanos	6	1	0460
289	Macam	6	1	0224
290	Mukul	6	1	0331
291	Mul Há Ho	6	1	0136
292	Ninguno [UAIM]	6	1	0432
293	Ninguno [Vivero]	6	1	0508
294	Nitkichén	6	1	0053
295	No Ho Lum	6	1	0499
296	Nohyaxché	6	1	0014
297	Nuc Xebaz	6	1	0332
298	Nup-Dzonot	6	1	0057
299	Pénjamo	6	1	0335
300	Piedras Negras	6	1	0250
301	Piña	6	1	0336
302	Pixoy	6	1	0060
303	Pollito	6	1	0337
304	Puerto Vallarta	6	1	0500
305	Rancho Nuevo	6	1	0062
306	Rengo	6	1	0340
307	Sacrificio	6	1	0342
308	Sahcabá	6	1	0343
309	San Agustín	6	1	0344
310	San Alejo	6	1	0056
311	San Andrés	6	1	0205
312	San Ángel	6	1	0067
313	San Anselmo	6	1	0068
314	San Antonio	6	1	0160
315	San Antonio Baa Há	6	1	0347
316	San Arturo	6	1	0348
317	San Aurelio	6	1	0349
318	San Bernardino California	6	1	0350
319	San Bernardo	6	1	0168
320	San Carlos	6	1	0072
321	San Cenobio	6	1	0212
322	San Chumas	6	1	0357
323	San Cosme	6	1	0356
324	San Cristóbal	6	1	0073
325	San Daniel	6	1	0358
326	San Dayan	6	1	0453
327	San Diego	6	1	0075
328	San Dimas	6	1	0007
329	San Eduardo	6	1	0502
330	San Enrique	6	1	0152
331	San Esteban	6	1	0429
332	San Felipe	6	1	0277
333	San Francisco	6	1	0080
334	San Gabriel	6	1	0362
335	San Gaspar	6	1	0363
336	San Genaro	6	1	0081
337	San Germán	6	1	0264
338	San Gregorio	6	1	0456
339	San Hol	6	1	0042
340	San Ignacio	6	1	0364
341	San Isidro	6	1	0082
342	San Isidro Labrador	6	1	0366
343	San Jorge	6	1	0069
344	San José	6	1	0085
345	San José de los Reyes	6	1	0155
346	San José Pedro	6	1	0086
347	San Juan	6	1	0087
348	San Judas Tadeo	6	1	0430
349	San Julián	6	1	0090
350	San Lázaro	6	1	0378
351	San Lorenzo	6	1	0190
352	San Luis	6	1	0091
353	San Manuel	6	1	0283
354	San Marcial Uno	6	1	0257
355	San Marcos	6	1	0280
356	San Martín	6	1	0092
357	San Martín de Porres	6	1	0386
358	San Mateo	6	1	0093
359	San Miguel	6	1	0094
360	San Nicolás	6	1	0291
361	San Pablo	6	1	0388
362	San Paulino	6	1	0170
363	San Pedro	6	1	0095
364	San Pedro Buenavista	6	1	0010
365	San Pedro Dzarapix	6	1	0393
366	San Rafael	6	1	0096
367	San Román	6	1	0204
368	San Sebastián	6	1	0012
369	San Tinaco	6	1	0396
370	San Vicente	6	1	0473
371	Santa Ana	6	1	0099
372	Santa Anita	6	1	0196
373	Santa Antonia	6	1	0504
374	Santa Bárbara	6	1	0254
375	Santa Brígida	6	1	0400
376	Santa Cecilia	6	1	0381
377	Santa Cruz	6	1	0134
378	Santa Cruz Amapola	6	1	0209
379	Santa Cruz Yaxché	6	1	0101
380	Santa Elena	6	1	0102
381	Santa Fe	6	1	0490
382	Santa Isabel	6	1	0103
383	Santa Lucía	6	1	0177
384	Santa Manuela	6	1	0482
385	Santa Marcelina	6	1	0408
386	Santa Margarita	6	1	0474
387	Santa María	6	1	0105
388	Santa María de Guadalupe	6	1	0182
389	Santa María X-Tulín	6	1	0139
390	Santa Paula	6	1	0409
391	Santa Rita	6	1	0221
392	Santa Rosa	6	1	0110
393	Santa Rosalía	6	1	0141
394	Santa Trinidad	6	1	0210
395	Santiaguito	6	1	0258
396	Santo Domingo	6	1	0003
397	Sayaluch	6	1	0151
398	Siete Estrellas	6	1	0414
399	Sis Há	6	1	0195
400	Sisbik	6	1	0505
401	Tepeyac	6	1	0431
402	Tilapia [Granja]	6	1	0468
403	Tobiye	6	1	0119
404	Tres Marías	6	1	0219
405	Tres Reyes	6	1	0118
406	Tres Reyes	6	1	0506
407	Tu-Mis Ché	6	1	0507
408	Unidad Juárez	6	1	0005
409	Villa de Guadalupe	6	1	0244
410	X-Azul	6	1	0176
411	X-Bec	6	1	0002
412	X-Bohmil	6	1	0433
413	X-Cacal	6	1	0434
414	X'caladzonot	6	1	0048
415	X-Canhá	6	1	0181
416	X-Huech	6	1	0435
417	Xiat	6	1	0127
418	X-Kambul	6	1	0436
419	X-Kanán	6	1	0122
420	Xkunkun Há	6	1	0509
421	X-Lacah	6	1	0437
422	X-Limón	6	1	0125
423	X-Limón Chen	6	1	0126
424	X-Limón Há	6	1	0476
425	Xluch	6	1	0483
426	Xmut Huá	6	1	0009
427	X-Pájaro	6	1	0145
428	X-Palma	6	1	0058
429	Xtrampa	6	1	0416
430	X-Tup	6	1	0438
431	X-Yach	6	1	0439
432	Yahcox	6	1	0128
433	Yaxché	6	1	0130
434	Yax-Há	6	1	0440
435	Yokdzonot	6	1	0132
436	Yoopop	6	1	0441
437	Zapata	6	1	0461
438	Cacalchén	7	1	0001
439	Candelaria	7	1	0048
440	Chech Mela	7	1	0023
441	Cruz Verde	7	1	0083
442	Dzoyolá	7	1	0058
443	El Girasol	7	1	0010
444	El Nance	7	1	0074
445	Kanku	7	1	0054
446	Kulinché	7	1	0073
447	La Candelaria	7	1	0080
448	La Unión [Rancho]	7	1	0092
449	Macachí	7	1	0056
450	Mi Querido Viejo	7	1	0091
451	Ninguno [COBAY]	7	1	0082
452	Ninguno [UAIM]	7	1	0069
453	Onil	7	1	0075
454	Puhá Segundo	7	1	0076
455	Sabacché	7	1	0042
456	Sahcabá	7	1	0003
457	San Antonio Puhá	7	1	0012
458	San Damián	7	1	0050
459	San Felipe	7	1	0019
460	San Francisco	7	1	0051
461	San José	7	1	0013
462	San José Cholul	7	1	0009
463	San Manuel Catzín	7	1	0035
464	San Miguel	7	1	0062
465	San Pablo	7	1	0060
466	San Pedro	7	1	0068
467	San Pedro Dzulá	7	1	0006
468	Santa Ana	7	1	0017
469	Santa Elena	7	1	0095
470	Santa Fe	7	1	0063
471	Santa Isabel	7	1	0064
472	Santa Rita	7	1	0015
473	Sigfrido	7	1	0065
474	Sigfrido Ramírez	7	1	0041
475	Silil	7	1	0078
476	Sitpach	7	1	0079
477	Tixkopoil	7	1	0033
478	Buenavista	8	1	0029
479	California	8	1	0034
480	Calotmul	8	1	0001
481	Ceh Puch	8	1	0003
482	Chan Santa Cruz	8	1	0063
483	Chan Sucilá	8	1	0064
484	Chechén	8	1	0057
485	Chen L	8	1	0093
486	Chuytab	8	1	0023
487	Galilea	8	1	0065
488	Ichyah	8	1	0080
489	Itzinté	8	1	0096
490	Kambul	8	1	0067
491	Kan-Chalún	8	1	0068
492	Kankabdzonot	8	1	0069
493	Komichén	8	1	0027
494	La Gran Lucha	8	1	0095
495	La Guadalupana	8	1	0066
496	La Ponderosa	8	1	0033
497	Las Palmas	8	1	0098
498	Los Laureles	8	1	0097
499	María José	8	1	0050
500	Mavira [Rancho]	8	1	0099
501	Nabulá	8	1	0070
502	Oriente [Crematorio]	8	1	0094
503	Piedras Negras	8	1	0018
504	Pocoboch	8	1	0006
505	Sak Akal (Laguna Blanca)	8	1	0091
506	San Agustín	8	1	0051
507	San Antonio	8	1	0071
508	San Antonio Yohdzonot	8	1	0073
509	San Carlos	8	1	0102
510	San Felipe	8	1	0074
511	San Fernando Tepich	8	1	0040
512	San Francisco	8	1	0059
513	San Isidro	8	1	0008
514	San José	8	1	0036
515	San Martín	8	1	0106
516	San Miguel	8	1	0037
517	San Pedro	8	1	0055
518	Santa Clara	8	1	0076
519	Santa Cruz	8	1	0079
520	Santa Elena	8	1	0075
521	Santa Fe	8	1	0107
522	Santa Michelle	8	1	0053
523	Santa Rita	8	1	0011
524	Santa Rosa	8	1	0108
525	Tahcabo	8	1	0013
526	Tikindzonot	8	1	0081
527	Tres Estrellas	8	1	0014
528	Tres Reyes	8	1	0061
529	Tux-Dzonot	8	1	0082
530	Tuzik	8	1	0024
531	Tzalam	8	1	0015
532	Tzuc	8	1	0109
533	Uxpib	8	1	0085
534	Xceh	8	1	0048
535	Xchay	8	1	0083
536	Xhuech	8	1	0110
537	Xkoh	8	1	0084
538	Xmit [Rancho]	8	1	0100
539	Xnoh On	8	1	0044
540	Yohactún	8	1	0087
541	Yohdzadz	8	1	0062
542	Yohdzonot Mascabicú	8	1	0030
543	Yohkop	8	1	0088
544	Yoh-Nicté	8	1	0052
545	Yokdzonot Dos	8	1	0111
546	Yokdzonot Meneses	8	1	0017
547	Cansahcab	9	1	0001
548	Chac Xul	9	1	0008
549	Chacanmutul	9	1	0007
550	Ekuney	9	1	0010
551	Kancabchén	9	1	0002
552	Kanisté	9	1	0013
553	Kinchacam	9	1	0015
554	Kindzib	9	1	0016
555	Kuichén de las Flores	9	1	0017
556	Sahcatzín	9	1	0006
557	San Antonio	9	1	0034
558	San Antonio Chince	9	1	0043
559	San Antonio Xiat	9	1	0003
560	San Francisco	9	1	0022
561	San José	9	1	0023
562	San Juan	9	1	0039
563	San Luis	9	1	0064
564	San Mateo	9	1	0005
565	San Ramón	9	1	0058
566	Santa Belina	9	1	0059
567	Santa María	9	1	0004
568	Santa Rita	9	1	0060
569	Santa Rosa	9	1	0027
570	Santa Teresa	9	1	0028
571	Tepich	9	1	0029
572	Texán	9	1	0030
573	Tzuc Tzalam	9	1	0032
574	Venustiano Carranza	9	1	0045
575	Campi [Granja]	10	1	0086
576	Cantamayec	10	1	0001
577	Chacanteil	10	1	0005
578	Chac-Dzan	10	1	0083
579	Chacmultún	10	1	0006
580	Chacxul	10	1	0008
581	Chan Chuchub	10	1	0052
582	Chichicab	10	1	0010
583	Chimay	10	1	0011
584	Cholul	10	1	0012
585	Cruz Yah	10	1	0004
586	Dolores	10	1	0053
587	Dzidzilché	10	1	0013
588	Huan-Ká	10	1	0055
589	Huntochac	10	1	0018
590	Huntulchac	10	1	0057
591	Husumal	10	1	0016
592	Jesús María	10	1	0017
593	Kankabchén	10	1	0019
594	Kocholá	10	1	0049
595	Kulkaba [Granja]	10	1	0020
596	La Luz	10	1	0059
597	La Trinidad	10	1	0070
598	Mopilá	10	1	0021
599	Nenelá	10	1	0023
600	Polkeil	10	1	0060
601	Sac-Lac	10	1	0061
602	Sahcadzonot	10	1	0054
603	Sambulá	10	1	0034
604	San Antonio	10	1	0088
605	San Antonio Cal-Ulmi	10	1	0038
606	San Esteban	10	1	0092
607	San Francisco	10	1	0040
608	San Francisco Hu-Kal	10	1	0056
609	San Ignacio	10	1	0094
610	San Isidro	10	1	0063
611	San José Tzab	10	1	0065
612	San Juan	10	1	0064
613	San Juan Chuc	10	1	0033
614	San Martín	10	1	0027
615	San Miguel	10	1	0082
616	San Miguel Yokdzonot	10	1	0076
617	San Pablo Holcá	10	1	0028
618	San Pedro	10	1	0067
619	San Pedro Hu-Kal	10	1	0068
620	Santa Candelaria	10	1	0058
621	Santa Catalina	10	1	0085
622	Santa Cruz	10	1	0041
623	Santa Elena	10	1	0089
624	Santa Trinidad	10	1	0081
625	Sinitún	10	1	0071
626	Subiná	10	1	0072
627	Tab-Ek	10	1	0039
628	Tixcacal Dzonot	10	1	0073
629	Tixcacal Pérez	10	1	0030
630	Trinidad	10	1	0096
631	Tzutzil	10	1	0035
632	Xauchel	10	1	0043
633	Xnicteil	10	1	0074
634	Yaxché	10	1	0036
635	Yuyumal	10	1	0077
636	Alborada	11	1	0076
637	Ángel	11	1	0063
638	Au Soleil Couchant	11	1	0064
639	Casa Blanca	11	1	0072
640	Casa de la Luna	11	1	0057
641	Casita de Piedra	11	1	0073
642	Celeste Vida	11	1	0077
643	Celestún	11	1	0001
644	Chac Canché	11	1	0002
645	Chiabal	11	1	0023
646	Chichitos	11	1	0024
647	Chuc Dzalam	11	1	0078
648	Eco Paraíso	11	1	0065
649	El Corcho	11	1	0021
650	El Tigre	11	1	0062
651	Flamingos	11	1	0059
652	Indio	11	1	0022
653	Kalac Huayuni	11	1	0028
654	La Asunción	11	1	0020
655	La Dueña	11	1	0052
656	La Paloma	11	1	0066
657	La Victoria	11	1	0009
658	Los Hermanos	11	1	0025
659	Monte Sol	11	1	0080
660	Nido de Tortuga	11	1	0056
661	Ninguno [Laboratorio de Camarón]	11	1	0068
662	Perlita Tampico	11	1	0061
663	Playa Gaviota	11	1	0074
664	Playa Maya	11	1	0070
665	Poltzimín	11	1	0008
666	Poolnuxi	11	1	0043
667	Punta Palmar	11	1	0003
668	Ralph Broom	11	1	0058
669	San Antonio	11	1	0047
670	San Carlos	11	1	0034
671	San Ramón	11	1	0081
672	Santa Cruz Xixim	11	1	0007
673	Santa Isabel	11	1	0044
674	Santa Julia	11	1	0046
675	Santa María	11	1	0032
676	Santa Rosa	11	1	0071
677	Sinkehuel	11	1	0033
678	Soledad	11	1	0069
679	Trian	11	1	0045
680	Villa del Mar	11	1	0075
681	Arizona	12	1	0137
682	Bahdzonot	12	1	0030
683	Candelaria	12	1	0140
684	Canoas	12	1	0141
685	Cenotillo	12	1	0001
686	Chacanhú	12	1	0105
687	Chen Delgado	12	1	0036
688	Chen Kekén	12	1	0020
689	Chun-Ox	12	1	0003
690	Coralillo	12	1	0173
691	Ebis	12	1	0168
692	Ebtún	12	1	0043
693	El Cielo	12	1	0159
694	El Platanal	12	1	0169
695	El Trébol	12	1	0155
696	Haydzonot	12	1	0012
697	K'anto [Unidad Agrícola]	12	1	0156
698	Kax-Ek	12	1	0130
699	Kimis	12	1	0142
700	La Herradura	12	1	0046
701	La Mariposa	12	1	0143
702	La Rejollada	12	1	0170
703	Las Garrapatas	12	1	0174
704	Las Palomas	12	1	0055
705	Las Yeguas	12	1	0129
706	Lindero	12	1	0176
707	Los Ángeles	12	1	0172
708	Los Laureles	12	1	0175
709	Noczal	12	1	0053
710	Pakchén	12	1	0124
711	Pozo Azul	12	1	0144
712	Rancho Pobre	12	1	0116
713	Reforma Agraria	12	1	0015
714	San Alfredo	12	1	0145
715	San Andrés	12	1	0122
716	San Antonio	12	1	0146
717	San Antonio X-Nuc	12	1	0041
718	San Felipe	12	1	0147
719	San Félix Medina	12	1	0183
720	San Francisco	12	1	0184
721	San Hipólito	12	1	0066
722	San Ignacio	12	1	0067
723	San Isidro	12	1	0149
724	San José	12	1	0186
725	San José Yunchén	12	1	0103
726	San Juan	12	1	0069
727	San Judas Tadeo	12	1	0071
728	San Luis	12	1	0187
729	San Manuel	12	1	0150
730	San Marcos	12	1	0109
731	San Miguel	12	1	0074
732	San Pedro Pacbihool	12	1	0123
733	San Román	12	1	0077
734	San Silvestre	12	1	0033
735	Santa Ana	12	1	0154
736	Santa Carmen	12	1	0188
737	Santa Clara	12	1	0114
738	Santa Clarita	12	1	0190
739	Santa Cruz	12	1	0078
740	Santa Elena	12	1	0079
741	Santa Fe	12	1	0120
742	Santa María	12	1	0082
743	Santa Pilar	12	1	0083
744	Santa Rita	12	1	0110
745	Santa Rosa	12	1	0085
746	Taan Mul	12	1	0167
747	Texas	12	1	0087
748	Texas el Grande	12	1	0099
749	Tixbacab	12	1	0009
750	Tuciná	12	1	0021
751	Unión	12	1	0089
752	Uzil	12	1	0192
753	Villa Flor	12	1	0157
754	Villahermosa	12	1	0193
755	X-Arcos Chen	12	1	0131
756	X-Ayín Número Uno	12	1	0138
757	X-Ayinchén	12	1	0158
758	Xcunyá	12	1	0026
759	X-Huul	12	1	0091
760	Xkanhá	12	1	0194
761	X-Lobos	12	1	0013
762	X-Noc Ac	12	1	0052
763	X-Noc Ac	12	1	0148
764	X-Takay	12	1	0095
765	Yohdzonot Número Dos	12	1	0133
766	Yokdzaz	12	1	0100
767	Yomac	12	1	0195
768	Águilas del Sureste	22	1	0016
769	Canul Uno	22	1	0095
770	Cecilia Guadalupe	22	1	0096
771	CIDECO	22	1	0097
772	Conkal	22	1	0001
773	Dilio	22	1	0102
774	Doble R	22	1	0129
775	Dos Arbolitos	22	1	0045
776	Dos Potrillos	22	1	0098
777	Ebenezer	22	1	0101
778	El Guzmán	22	1	0086
779	El Pastor	22	1	0065
780	El Polvorín	22	1	0099
781	El Reto	22	1	0092
782	El Roble	22	1	0069
783	El Ruedo	22	1	0059
784	El Sol [Pescados y Mariscos]	22	1	0066
785	Estefanía	22	1	0100
786	Hermanos Aguilar Góngora	22	1	0087
787	Instituto Tecnológico Agropecuario Dos	22	1	0060
788	Instituto Tecnológico Agropecuario Dos	22	1	0061
789	Isaac Uno	22	1	0105
790	Ixchel	22	1	0126
791	Kantoyna	22	1	0002
792	La Aurora	22	1	0073
793	La Guadalupana	22	1	0021
794	La Triple S	22	1	0081
795	Lac-Chen	22	1	0063
796	Las Palomas	22	1	0035
797	Los Compadres	22	1	0043
798	Los Nopales	22	1	0062
799	Los Potrillos	22	1	0130
800	Los Tres Potrillos	22	1	0112
801	Los Vergara	22	1	0113
802	Malas Pulgas	22	1	0114
803	Manuel Reyes Zaldívar [Lienzo Charro]	22	1	0064
804	Matamoros	22	1	0033
805	Ninguno [Deshuesadero]	22	1	0044
806	Ninguno [RDE]	22	1	0115
807	Pastores	22	1	0029
808	Pet-Kanché	22	1	0030
809	Pet-Kanchén	22	1	0034
810	Ramayo Dos	22	1	0022
811	Ramiro Flores	22	1	0036
812	Romi	22	1	0070
813	Rubí Guadalupe	22	1	0071
814	San Agustín	22	1	0037
815	San Alberto	22	1	0031
816	San Andrés	22	1	0116
817	San Antonio	22	1	0117
818	San Antonio Holactún	22	1	0003
819	San Carlos Ramayo	22	1	0017
820	San Diego	22	1	0025
821	San Diego Cutz	22	1	0004
822	San Francisco	22	1	0072
823	San Germán	22	1	0118
824	San Juan	22	1	0074
825	San Judas Tadeo	22	1	0120
826	San Lorenzo	22	1	0012
827	San Miguel	22	1	0121
828	San Pastor	22	1	0076
829	San Rafael	22	1	0077
830	San Rafael	22	1	0078
831	San Román	22	1	0122
832	Santa Cecilia	22	1	0089
833	Santa Cruz Xcuyún	22	1	0079
834	Santa Elisia	22	1	0020
835	Santa Isabel	22	1	0023
836	Santa María Rosas	22	1	0005
837	Santa Rosa	22	1	0123
838	Santa Úrsula	22	1	0032
839	Santo Domingo Kumán	22	1	0010
840	Tekat [Granja Avícola]	22	1	0085
841	Tres Cerditos	22	1	0038
842	Tres Marías	22	1	0125
843	Unión	22	1	0082
844	Valdez	22	1	0083
845	Viridiana	22	1	0084
846	Xchel	22	1	0088
847	X-Cuyum	22	1	0006
848	Xkantún	22	1	0009
849	Xlapak	22	1	0131
850	Yaaxtún	22	1	0127
851	Yun Kaax	22	1	0128
852	Chebalam	23	1	0013
853	Cuncunul	23	1	0001
854	Diseño Urbano e Ingeniería	23	1	0050
855	Divino Niño	23	1	0034
856	Hulmal	23	1	0045
857	La Mestiza	23	1	0047
858	Samal	23	1	0005
859	San Agustín	23	1	0051
860	San Antonio	23	1	0006
861	San Diego	23	1	0025
862	San Francisco	23	1	0009
863	San Juan	23	1	0048
864	San Juan de las Palmas	23	1	0039
865	San Pablo	23	1	0049
866	San Pedro	23	1	0037
867	Santa Bárbara	23	1	0014
868	Santa Clara de las Hadas	23	1	0032
869	Santa Cruz	23	1	0044
870	X-Akabchén	23	1	0040
871	X'mahcaba	23	1	0033
872	Chunkanán	24	1	0003
873	Cuchbalam	24	1	0002
874	Cuzamá	24	1	0001
875	Eknakán	24	1	0005
876	El Dorado [Rancho]	24	1	0029
877	Lunche Uno	24	1	0027
878	Noh Yabacú	24	1	0030
879	Nohchakán	24	1	0006
880	San Alberto	24	1	0025
881	San Antonio Dzecuzama	24	1	0004
882	San Eulogio	24	1	0031
883	San Francisco Sisal	24	1	0008
884	Santa Cruz	24	1	0016
885	Yaxkukul	24	1	0009
886	Bakché	13	1	0002
887	Chacsinkín	13	1	0001
888	Chan Huayab	13	1	0010
889	San Dionisio	13	1	0004
890	San Miguel	13	1	0019
891	Santa María	13	1	0005
892	Suctún	13	1	0006
893	Tinum	13	1	0008
894	X-Box	13	1	0009
895	Xhulucán	13	1	0013
896	Xkanan	13	1	0011
897	Xno-Huayab	13	1	0012
898	Yucatán Primero	13	1	0007
899	Chan Ichmul	14	1	0002
900	Chankom	14	1	0001
901	Checmil	14	1	0034
902	Muchucuxcáh	14	1	0083
903	Nicte-Há	14	1	0003
904	Pambá	14	1	0004
905	Rancho Nuevo	14	1	0045
906	San Antonio	14	1	0048
907	San Felipe	14	1	0087
908	San Isidro	14	1	0006
909	San José	14	1	0052
910	San Juan	14	1	0024
911	San Juan Xkalakdzonot	14	1	0035
912	San Prudencio	14	1	0007
913	Santa Brígida	14	1	0022
914	Santa Rosa	14	1	0077
915	Ticimul	14	1	0011
916	Tzukmuc	14	1	0012
917	Xanlá	14	1	0013
918	X-Bohom	14	1	0014
919	X-Cocail	14	1	0016
920	Xkalakdzonot	14	1	0015
921	Xkatún	14	1	0018
922	X-Kopchén	14	1	0043
923	Xkopteil	14	1	0017
924	X-Mahas	14	1	0085
925	Xtohil	14	1	0019
926	Yokdzonot	14	1	0028
927	Aguada Polol	15	1	0002
928	Chapab	15	1	0001
929	Chuleb	15	1	0007
930	Citincabchén	15	1	0003
931	Hunabchén	15	1	0004
932	Ninguno [UAIM]	15	1	0026
933	Puyuytun	15	1	0008
934	Rosa Alvarado	15	1	0025
935	San Antonio	15	1	0023
936	San Bernabé	15	1	0009
937	San Cristóbal	15	1	0010
938	San Ignacio	15	1	0013
939	San Pedro	15	1	0029
940	Techoh	15	1	0016
941	Xaybé	15	1	0017
942	Yaxché	15	1	0019
943	Acapulco	16	1	0401
944	Actunxoch	16	1	0497
945	Akabchén	16	1	0616
946	Akachén	16	1	0431
947	Akché	16	1	0496
948	Aroba	16	1	0230
949	Blanca Flor	16	1	0323
950	Buenavista	16	1	0276
951	Calvario	16	1	0541
952	Canabé	16	1	0407
953	Chacán	16	1	0560
954	Chachadzonot	16	1	0279
955	Chacté	16	1	0596
956	Champolín	16	1	0008
957	Chan Kom	16	1	0561
958	Chan Pistemax	16	1	0462
959	Chan Santa Rosa (Chan Akachén)	16	1	0530
960	Chanyokdzonot	16	1	0281
961	Chechmil	16	1	0066
962	Chemax	16	1	0001
963	Chen X-Pil	16	1	0014
964	Chenchén	16	1	0523
965	Chenep (Chioplé)	16	1	0013
966	Chiquix	16	1	0285
967	Chocholá	16	1	0583
968	Cholul	16	1	0016
969	Chulután	16	1	0019
970	Chumpich	16	1	0139
971	Chun Yaxché	16	1	0598
972	Chunjujub	16	1	0584
973	Cocoyol	16	1	0006
974	Colonchán	16	1	0399
975	Coxlei (Kochila)	16	1	0362
976	Cruzché	16	1	0288
977	Cuatro Ha	16	1	0594
978	Cuernavaca (X-Haas)	16	1	0599
979	Cutzán	16	1	0585
980	Dothilá	16	1	0289
981	Dzalá (Yohdzonot)	16	1	0345
982	Dzibichén	16	1	0292
983	Dzibil	16	1	0436
984	Dzidziché	16	1	0562
985	Dzidzilché	16	1	0293
986	Dzinic Balam	16	1	0563
987	Dzitnup	16	1	0294
988	Dzonkeil	16	1	0600
989	Dzonot	16	1	0115
990	Dzuca	16	1	0347
991	Echeverría	16	1	0534
992	El Polvorín	16	1	0525
993	Estrella	16	1	0487
994	Grano de Oro	16	1	0299
995	Hoteoch	16	1	0460
996	Hoxcab	16	1	0300
997	Juan Diego	16	1	0601
998	Jujilchén	16	1	0602
999	Kanasín	16	1	0587
1000	Kankabal	16	1	0392
\.


--
-- Data for Name: Municipio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Municipio" ("iIdMunicipio", "vMunicipio", "iActivo", "vClave") FROM stdin;
1	Abalá	1	001
2	Acanceh	1	002
3	Akil	1	003
4	Baca	1	004
5	Bokobá	1	005
6	Buctzotz	1	006
7	Cacalchén	1	007
8	Calotmul	1	008
9	Cansahcab	1	009
10	Cantamayec	1	010
11	Celestún	1	011
12	Cenotillo	1	012
13	Chacsinkín	1	016
14	Chankom	1	017
15	Chapab	1	018
16	Chemax	1	019
17	Chichimilá	1	021
18	Chicxulub Pueblo	1	020
19	Chikindzonot	1	022
20	Chocholá	1	023
21	Chumayel	1	024
22	Conkal	1	013
23	Cuncunul	1	014
24	Cuzamá	1	015
25	Dzán	1	025
26	Dzemul	1	026
27	Dzidzantún	1	027
28	Dzilam de Bravo	1	028
29	Dzilam González	1	029
30	Dzitás	1	030
31	Dzoncauich	1	031
32	Espita	1	032
33	Halachó	1	033
34	Hocabá	1	034
35	Hoctún	1	035
36	Homún	1	036
37	Huhí	1	037
38	Hunucmá	1	038
39	Ixil	1	039
40	Izamal	1	040
41	Kanasín	1	041
42	Kantunil	1	042
43	Kaua	1	043
44	Kinchil	1	044
45	Kopomá	1	045
46	Mama	1	046
47	Maní	1	047
48	Maxcanú	1	048
49	Mayapán	1	049
50	Mérida	1	050
51	Mocochá	1	051
52	Motul	1	052
53	Muna	1	053
54	Muxupip	1	054
55	Opichén	1	055
56	Oxkutzcab	1	056
57	Panabá	1	057
58	Peto	1	058
59	Progreso	1	059
60	Quintana Roo	1	060
61	Río Lagartos	1	061
62	Sacalum	1	062
63	Samahil	1	063
64	San Felipe	1	065
65	Sanahcat	1	064
66	Santa Elena	1	066
67	Seyé	1	067
68	Sinanché	1	068
69	Sotuta	1	069
70	Sucilá	1	070
71	Sudzal	1	071
72	Suma	1	072
73	Tahdziú	1	073
74	Tahmek	1	074
75	Teabo	1	075
76	Tecoh	1	076
77	Tekal de Venegas	1	077
78	Tekantó	1	078
79	Tekax	1	079
80	Tekit	1	080
81	Tekom	1	081
82	Telchac Pueblo	1	082
83	Telchac Puerto	1	083
84	Temax	1	084
85	Temozón	1	085
86	Tepakán	1	086
87	Tetiz	1	087
88	Teya	1	088
89	Ticul	1	089
90	Timucuy	1	090
91	Tinum	1	091
92	Tixcacalcupul	1	092
93	Tixkokob	1	093
94	Tixmehuac	1	094
95	Tixpéhual	1	095
96	Tizimín	1	096
97	Tunkás	1	097
98	Tzucacab	1	098
99	Uayma	1	099
100	Ucú	1	100
101	Umán	1	101
102	Valladolid	1	102
103	Xocchel	1	103
104	Yaxcabá	1	104
105	Yaxkukul	1	105
106	Yobaín	1	106
\.


--
-- Data for Name: ODS; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."ODS" ("iIdOds", "vOds") FROM stdin;
1	Fin de la pobreza
2	Hambre cero
3	Salud y bienestar
4	Educación de calidad
5	Igualdad de género
6	Agua limpia y saneamiento
7	Energía asequible y no contaminante
8	Trabajo decente y crecimiento económico
9	Industria, innovación e infraestructura
10	Reducción de las desigualdades
11	Ciudades y comunidades sostenibles
12	Producción y consumo responsables
13	Acción por el clima
14	Vida submarina
15	Vida de ecosistemas terrestres
16	Paz, justicia e instituciones sólidas
17	Alianzas para lograr los objetivos
\.


--
-- Data for Name: Obra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Obra" ("iIdObra", "vNombre", "vDescripcion", "iIdResponsable", "iIdEjecutor", "dFechaValidacion", "iIdApertura", "iAnioEjecucion", "dFechaCaptura", "iEstatus", "iIdPartida", "vDescripcionImpacto", "vBeneficiarios", "nImporteOriginal", "iAnioRecurso", "iIdCategoria", "vDomicilio", "iAtencionPrioritaria", "iIdUnidad", "iIdUgi", "iIdUbp", "vNombreModificado", "vConvenioFed", "vConveniodEjecutor", "vConvenioBeneficiario", "vClaveEscuela", "iParcial", "vDescripcionAdjudicacion", "vRegistroConstructor", "vEmpresa", "vRepresentante", "iTipo", "nNumeroContrato", "nImporteContrato", "dFechaInicio", "dFechaTermino", "iNumeroEmpleos", "iAvanceFisico", "iAvanceFinanciero", "nAmpliacion", "nMontoAmpliacion", "iDiasRetraso", "iEstado", "dNuevaFechaTermino", "vObservaciones", "nAportacionFiditrac", "nAportacionCMIC", "iTerminada", "nMontoPagado", "vEconomias", "vMetas", "vCierreAdministrativo", "dEntregaBeneficiario", "dEntregaContraloria", "nFiniquitoSOP") FROM stdin;
\.


--
-- Data for Name: ObraFinanciamiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."ObraFinanciamiento" ("iIdObra", "iIdFinanciamiento", "nMonto") FROM stdin;
\.


--
-- Data for Name: ObraLocalidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."ObraLocalidad" ("iIdObra", "iIdLocalidad") FROM stdin;
\.


--
-- Data for Name: Oficio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Oficio" ("iIdOficio", "vDescripcion", "vRuta", "iIdObra", "iActivo") FROM stdin;
\.


--
-- Data for Name: PED2019Eje; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."PED2019Eje" ("iIdEje", "vEje") FROM stdin;
1	Yucatán con Economía Inclusiva
2	Yucatán con Calidad de Vida y Bienestar Social
3	Yucatán Cultural con Identidad para el Desarrollo
4	Yucatán Verde y Sustentable
5	Igualdad de género oportunidades y no discriminación
6	Innovación, conocimiento y tecnología
7	Paz, Justicia y Gobernabilidad
8	Gobierno Abierto, Eficiente y Finanzas Sanas
9	Ciudades y Comunidades Sostenibles
10	Desarrollo regional
\.


--
-- Data for Name: PED2019Estrategia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."PED2019Estrategia" ("iIdEstrategia", "vEstrategia", "iIdObjetivo") FROM stdin;
1	Fortalecer la profesionalización de las empresas para el comercio local, nacional e internacional con enfoque de sostenibilidad y responsabilidad social.	1
2	Impulsar la tecnificación digital en las actividades comerciales.	1
3	Fortalecer la productividad y competitividad empresarial. 	2
4	Impulsar la comercialización de los productos locales.	2
5	Impulsar las ventajas competitivas del estado.	3
6	Fomentar esquemas que eleven la competitividad del talento en Yucatán.	3
7	Promocionar la propuesta de valor del estado.	4
8	Fomentar la reducción de costos de inversión extranjera a través de un sistema de consolidación de inversiones para el estado de Yucatán 	4
9	Fortalecer la producción sostenible entre las empresas del sector manufacturero e industrial.	5
10	Inducir las condiciones para el desarrollo industrial integral.	5
11	Impulsar el desarrollo de las zonas y proyectos industriales sostenibles.	6
12	Impulsar la inclusión y responsabilidad social en el sector industrial	6
13	Fomentar la especialización de los prestadores de servicios turísticos del estado orientados a la sostenibilidad.	7
14	Impulsar la diversificación de los productos y  servicios turísticos sostenibles	7
15	Promover la imagen y los atractivos turísticos del estado a nivel nacional e internacional.	8
16	Fortalecer los segmentos de mercado turístico, existentes y potenciales.	8
17	 Desarrollar la calidad de los productos y servicios turísticos del estado.	9
18	Fomentar una economía turística incluyente en las comunidades del estado con potencial turístico.	9
19	Promover la inclusión laboral productiva.	10
20	Impulsar la regularización de la seguridad social de la población trabajadora.	10
21	Impulsar esquemas que aumenten la productividad laboral en el estado.	11
22	Promover la alta productividad de las empresas.	11
23	Generar capacidades de emprendimiento inclusivo y sostenible.	12
24	Impulsar el emprendimiento en los grupos vulnerables.	12
25	Impulsar acciones que permitan el emprendimiento local en igualdad de oportunidades en el mercado interno, nacional e internacional.	12
26	Fortalecer la capacidad del sector agropecuario de manera sostenible.	13
27	Fomentar el uso de la tecnología en el sector agropecuario.	13
28	Fomentar la sostenibilidad ambiental y sanidad en las actividades agropecuarias del estado.	13
29	Impulsar un sector pecuario productivo y sostenible.	14
30	Promover la mejora genética de las especies pecuarias del estado. 	14
31	Consolidar un sector agroalimentario productivo que garantice la seguridad alimentaria en el estado.	15
32	Fomentar la calidad de la producción agrícola.	15
33	Impulsar la competitividad sostenible de los productos pesqueros y acuícolas.	16
34	Promover el consumo interno de productos acuícolas y pesqueros para mejorar la calidad alimenticia de los sectores más desprotegidos.	16
35	Promover la investigación y transferencia tecnológica para el desarrollo sustentable de la pesca y la acuacultura.	16
36	Fomentar acciones para proteger los ecosistemas en las zonas donde se desarrollan actividades pesqueras y acuícolas.	16
37	Fortalecer la gestión hospitalaria y el desarrollo del capital humano en salud con enfoque de inclusión e interculturalidad.	17
38	Impulsar una estrategia integral de formación para la mejora continua de la calidad de los servicios de salud del estado.	17
39	Impulsar el desarrollo y uso de infraestructura sostenible, así como de las tecnologías de la información, en las instituciones de salud de todos los municipios del estado.	17
40	Fomentar acciones de promoción y prestación de servicios de la salud entre la población en situación de vulnerabilidad.	18
41	Fortalecer acciones de prevención y atención integral de enfermedades para reducir los daños a la salud.	18
42	Fortalecer la prevención y atención integral de los trastornos mentales y riesgo suicida para favorecer el bienestar biopsicosocial del individuo y la sociedad.	18
43	Fortalecer la protección contra riesgos sanitarios en establecimientos y puntos sujetos a control sanitario para prevenir enfermedades asociadas.	18
44	Impulsar la participación de autoridades locales y comunitarias en el mejoramiento de las determinantes sociales en salud para incidir positivamente en la salud pública.	18
45	Impulsar la atención integral para las personas con malnutrición y desnutrición severa y moderada.	19
46	Promover hábitos alimenticios con alto valor nutricional con énfasis en las comunidades marginadas.	19
47	Fomentar mecanismos que garanticen el acceso a una vida sana a la población con inseguridad alimentaria.	20
48	Incentivar el autoconsumo, principalmente en zonas de alta inseguridad alimentaria.	20
49	Impulsar mecanismos que reduzcan las carencias sociales en las comunidades indígenas.	21
50	Fomentar esquemas que eliminen las brechas sociales de personas maya hablantes potenciando el respeto a los derechos y sus tradiciones.	21
51	Fortalecer la infraestructura educativa básica y media superior, priorizando a las comunidades con mayor rezago educativo.	22
52	Fomentar acciones de alfabetización integral en las comunidades con mayor rezago educativo.	22
53	Fortalecer la calidad de la educación en todos sus niveles.	23
54	Impulsar mecanismos que garanticen el derecho a la educación laica, gratuita, de calidad y libre de discriminación.	23
55	Impulsar esquemas de financiamiento para la adquisición, construcción, ampliación y mejoramiento de vivienda.	24
56	Fortalecer las acciones que mejoran la calidad y los espacios de la vivienda.	24
57	Promover acciones de infraestructura básica que permitan el acceso de la población a servicios de calidad para la vivienda.	25
58	Elevar la calidad del entorno y el acceso a servicios básicos en la vivienda.	25
59	Impulsar acciones dirigidas a la protección laboral y social de la población, que permitan llevar una vida digna.	26
60	Fortalecer esquemas que incrementen la cobertura y el acceso al sistema de seguridad social para la población en situación de pobreza.	26
61	Impulsar un sistema de seguridad social que garantice el bienestar del adulto mayor.	26
62	Fortalecer la infraestructura cultural de calidad incluyente y resiliente.	27
63	Fortalecer la oferta cultural incluyente y accesible para toda la población.	27
64	Impulsar la adopción del modelo de economía naranja, logrando con ello el encadenamiento de ideas para su posterior transformación en bienes y servicios culturales cuyo valor este determinado por su contenido de propiedad intelectual.	27
65	Promocionar los eventos y servicios culturales.	28
66	Facilitar el acceso a los eventos, productos y servicios artísticos y culturales.	28
67	Fomentar la producción literaria y el hábito de la lectura.	28
68	Promover las tradiciones, lenguaje, costumbres, valores y todas las formas de expresión de la identidad y la cultura yucateca.	29
69	Fomentar el desarrollo de las actividades artesanales.	29
70	Fomentar el surgimiento de nuevos creadores de arte.	30
71	Impulsar a los creadores de arte y promotores de cultura de la entidad.	30
72	Fortalecer el aprendizaje musical y la educación artística de calidad en el sistema educativo.	31
73	Procurar la vinculación entre el sistema educativo y profesionales del arte.	31
74	Impulsar la formación de profesionales de las artes.	32
75	Consolidar la oferta educativa superior en artes.	32
76	Fomentar el conocimiento del patrimonio material, natural e inmaterial del estado.	33
77	Fortalecer el patrimonio cultural.	33
78	Fortalecer la preparación de los talentos deportivos de forma incluyente.	34
79	Fortalecer el nivel de desarrollo de los deportistas de alto rendimiento.	34
80	Fomentar la cultura física como estilo de vida saludable.	35
81	Fomentar la cultura de recreación física.	35
82	Facilitar el acceso a los eventos deportivos.	36
83	Promover la realización de eventos deportivos.	36
84	Fortalecer acciones para la conservación de las áreas naturales protegidas.	37
85	Impulsar acciones a favor de la protección y aprovechamiento sustentable de la biodiversidad.	37
86	Fortalecer los mecanismos de regulación y capacitación para la administración y conservación de los recursos naturales del estado. 	37
87	Impulsar acciones de reforestación mediante el manejo sustentable de especies endémicas que incrementen la superficie arbórea.	38
88	Fortalecer acciones de prevención que disminuyan la deforestación.	38
89	Impulsar medidas de adaptación y mitigación ante el cambio climático.	39
90	Impulsar acciones que reduzcan el impacto de los desastres naturales.	39
91	Fomentar una economía baja en emisiones de carbono en Yucatán.	40
92	Fomentar la gestión integral de la calidad del aire.	40
93	Fortalecer la cultura de reutilización de aguas residuales, para disminuir la demanda del agua.	41
94	Mejorar la infraestructura y el funcionamiento en torno al abastecimiento y tratamiento del agua.	41
95	Promover la innovación y la economía circular en el saneamiento del agua.	41
96	Impulsar el uso responsable del agua para disminuir su contaminación y desperdicio.	42
97	Impulsar mecanismos e instrumentos de monitoreo e inspección de la calidad del agua. 	42
98	Impulsar una cultura del adecuado manejo de residuos que disminuya los riesgos ambientales.	43
99	Impulsar acciones que contribuyan a la reducción de residuos sólidos.	44
100	Promover acciones que contribuyan a la reutilización de materiales de desecho.	44
101	Impulsar el desarrollo tecnológico de energías limpias.	45
102	Impulsar la generación de energía de fuentes renovables y la eficiencia energética compatible con el entorno social y ambiental.	45
103	Fomentar la generación de energías limpias.	46
104	Priorizar acciones que reduzcan costos por consumo de energéticos. 	46
105	Fortalecer las acciones que incrementen la conservación de las especies marinas.	47
106	Impulsar acciones de protección de playas y mares que aumenten su conservación.	47
107	Fortalecer el sistema de transporte público para disminuir los tiempos de traslado de la población.	48
108	Impulsar acciones que permitan el acceso al transporte público atendiendo las necesidades de las personas en situación de vulnerabilidad.	48
109	Fortalecer la infraestructura vial urbana que incremente las alternativas de movilidad en las ciudades.	49
110	Impulsar la cultura vial para la reducción de accidentes.	49
111	Promover sistemas de movilidad urbana asequibles y sustentables.	49
112	Impulsar la mejora continua de los servicios y capacitación del personal de salud para garantizar el acceso y ejercicio del derecho a la salud de las mujeres en forma incluyente.	50
113	Promover mecanismos para la prevención y atención del embarazo adolescente en todo el Sistema de Salud.	50
114	Reducir la deserción, abandono y rezago educativo de las mujeres.	51
115	Impulsar una mayor participación y presencia de las mujeres en los campos de las ciencias, ingenierías, y tecnologías.	51
116	Promover la autonomía y el empoderamiento de las mujeres para el desarrollo económico sostenible. 	52
117	Impulsar el acceso de las mujeres a cargos de toma de decisiones y un mayor involucramiento en las decisiones públicas.	52
118	Fortalecer la coordinación interinstitucional para prevenir, atender y sancionar  prácticas que vulneren los derechos de las mujeres.	53
119	Promover mecanismos para involucrar a la sociedad en la prevención de las violencias hacia las mujeres.	53
120	Implementar acciones que garanticen la seguridad y la salud integral de las mujeres en situación de violencia. 	53
121	Facilitar el acceso de las mujeres a la justicia.	53
122	Fortalecer los sistemas de información sobre las violencias hacia las mujeres.	53
123	Fomentar la igualdad de oportunidades de los derechos de las personas en situación de vulnerabilidad.	54
124	Impulsar la igualdad de oportunidades de bienestar social de las personas en situación de vulnerabilidad.	54
125	Promover oportunidades de una vida digna para las personas en situación de vulnerabilidad.	54
126	Impulsar acciones que contribuyan con el ejercicio de los derechos políticos, sociales y culturales en condiciones de igualdad y no discriminación.	54
127	Mejorar la cobertura de la educación superior en el estado de manera sostenible e inclusiva.	55
128	Vincular de manera sostenible y permanente el sector productivo con el educativo para satisfacer la demanda actual y emergente de capital humano de las empresas.	55
129	Impulsar de manera sostenible e inclusiva la formación temprana de la ciencia.	55
130	Fortalecer la pertinencia de la educación superior en el estado de manera sostenible e inclusiva.	55
131	Mejorar de manera permanente y sostenible la calidad de los posgrados.	56
132	Fortalecer de manera sostenible e inclusiva la eficiencia terminal de los estudiantes de educación superior.	56
133	Impulsar la generación de conocimiento en ciencia, tecnología, artes y humanidades.	57
134	Extender de manera sostenible e inclusiva la vinculación de las ciencias, la tecnología, las artes y las humanidades con instituciones nacionales e internacionales. 	57
135	Impulsar la aplicación de la ciencia y la tecnología en la solución de problemas estratégicos de manera permanente y sostenible.	57
136	Impulsar de manera permanente y sostenible la innovación para el desarrollo del estado.	58
137	Favorecer de manera sostenible e inclusiva el desarrollo de invenciones en los sectores público, privado y social.	58
138	Fortalecer de manera sostenible la infraestructura para el conocimiento científico, tecnológico e innovación.	58
139	Fortalecer de manera sostenible la prevención del delito con un enfoque de derechos humanos y especial énfasis en la igualdad de género y la interculturalidad.	59
140	Impulsar la mediación comunitaria con perspectiva de género.	59
141	Fortalecer la profesionalización y dignificación de los elementos policiales del estado de forma incluyente y sostenible.	59
142	Reforzar las acciones de seguridad vial en el estado.	59
143	Fortalecer la seguridad pública  con énfasis en las regiones de mayor vulnerabilidad y en apego a los derechos humanos.	60
144	Implementar acciones que fomenten el cumplimiento de la Ley en favor de la seguridad de las y los habitantes.	60
145	Fortalecer la infraestructura y organización institucional de procuración de justicia en el estado. 	61
146	Mejorar  la calidad en la atención a las y los usuarios del Sistema de Justicia.	61
147	Fortalecer los mecanismos de acceso a la justicia de los grupos en situación de vulnerabilidad.	61
148	Fortalecer el marco normativo de procuración de justicia en el estado,con enfoque de derechos humanos. 	62
149	Fortalecer las instituciones de justicia se constituyan como instituciones sólidas y de vanguardia.	62
150	 Fortalecer la cultura de la transparencia en las instituciones de seguridad y justicia en el estado.	63
151	Impulsar acciones que aumenten la certeza jurídica de las personas de forma incluyente y sostenible.	63
152	Promover acciones para el desarrollo integral de los municipios del estado.	63
153	Acercar los servicios de las instituciones vinculadas con la seguridad, justicia y certeza jurídica de forma incluyente y sostenible.	63
154	Fortalecer la coordinación para la cooperación internacional y entre gobiernos subnacionales para el desarrollo incluyente y sostenible.	64
155	Impulsar la participación y organización de  eventos de carácter internacional.	64
156	Facilitar la accesibilidad, consulta y procesamiento de la información para la toma de decisiones.	65
157	Fomentar la participación y colaboración ciudadana para que la Administración Pública Estatal realice sus acciones en apego a los principios de transparencia y rendición de cuentas.	65
158	Vigilar la correcta aplicación de los recursos públicos	66
159	Fortalecer los mecanismos de prevención y sanción ante actos de corrupción.	66
160	Fomentar la incorporación del enfoque a resultados en el proceso de planeación, programación, presupuestación, control, seguimiento y evaluación.	67
161	Favorecer los mecanismos de seguimiento y evaluación del desempeño.	67
162	Impulsar la política de mejora regulatoria para mejorar la atracción de inversiones y el bienestar social en el Estado de Yucatán y sus municipios.	68
163	Impulsar la gestión por procesos para mejorar la eficiencia y calidad en la administración pública estatal.	68
164	Fortalecer la gestión de los recursos humanos y patrimoniales del Gobierno del Estado garantizando su sostenibilidad.	68
165	Fortalecer las capacidades de recaudación del Gobierno del Estado.	69
166	Mejorar la gestión del gasto público.	69
167	Fomentar la inversión pública y privada sostenible.	70
168	Establecer mecanismos de cobertura vial que permita el acceso a todas las comunidades del estado.	71
169	Fortalecer la infraestructura ferroviaria sostenible.	71
170	Fortalecer la infraestructura logística de transporte a través del rescate y modernización integral y sostenible de los puertos y aeropuertos del estado.	71
171	Fortalecer el acceso a las redes y servicios de telecomunicaciones en las comunidades del estado.	72
172	Fortalecer la infraestructura tecnológica con enfoque de sostenibilidad.	72
173	Impulsar la participación de la industria de telecomunicaciones en los modelos educativos.	72
174	Impulsar un esquema de ordenamiento territorial de los asentamientos humanos que favorezca el desarrollo sostenible de las ciudades y comunidades.	73
175	Impulsar acciones para el cumplimiento de la normatividad urbana en coordinación con los sectores público, privado, social y académico.	73
176	Impulsar la elaboración de planes de ordenamiento ecológico local.	73
177	Estructurar los proyectos de infraestructura mediante una planeación consciente y racional con base en la rentabilidad económica y social.	73
178	Establecer áreas prioritarias de inversión que permitan enfocar los recursos de los programas federales, estatales y municipales desde una perspectiva territorial.	74
179	Fortalecer programas integrales de bienestar social regional que permitan mejorar la calidad de vida y la igualdad de condiciones, derechos y oportunidades de la población.	74
180	Fortalecer los programas culturales para potencializar el acceso a los servicios y actividades culturales en todas las regiones.	74
181	Promover buenas prácticas de sustentabilidad que garanticen la reducción de los impactos en el medio ambiente.	74
182	Desarrollar políticas de base territorial que permitan disminuir la incidencia delictiva en los municipios y regiones del Estado.	74
183	Apoyar a los municipios en la implantación y operación del Presupuesto Basado en Resultados y del Sistema de Evaluación del Desempeño PBR-SED.	74
184	Constituir un proceso de planificación territorial bajo un marco integrado que considere los diferentes instrumentos de planeación con la óptica de sustentabilidad para la provisión óptima de bienes y servicios públicos.	74
185	Favorecer la integración meso regional entre Yucatán, Campeche y Quintana Roo que permitan favorecer el desarrollo de la península.	75
\.


--
-- Data for Name: PED2019LineaAccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."PED2019LineaAccion" ("iIdLineaAccion", "vLineaAccion", "iIdEstrategia", "iIdOds") FROM stdin;
1	Profesionalizar a las empresas a través del fomento a las buenas prácticas comerciales y un enfoque de mejora continua.	1	8
2	Impulsar alianzas estratégicas en materia comercial con empresas especializadas y el sector académico.	1	8
3	Promover la responsabilidad social en el sector comercial y las empresas locales.	1	8
4	Establecer programas de inversión en tecnología para los productos comerciales.	2	8
5	Fomentar herramientas para el comercio digital de compra y venta de productos yucatecos en línea.	2	8
6	Dotar de equipo básico digital a las empresas para su crecimiento.	2	8
7	Establecer programas de capacitación de herramientas digitales.	2	8
8	Impulsar convenios de colaboración entre micro, pequeñas y medianas con las grandes empresas para el desarrollo de cadenas productivas.	3	8
9	Fomentar en las empresas el análisis de mercado previo, durante y después de la inversión y producción.	3	8
10	Impulsar la capacitación a las empresas en materia de productividad y aprovechamiento estratégico del sector comercial.	3	8
11	Gestionar estímulos e incentivos a las empresas para promover su formalización.	3	8
12	Impulsar esquemas de financiamiento accesible para las empresas en apoyo de sus procesos operativos.	3	8
13	Simplificar el marco regulatorio, los trámites de apertura de empresas y el acceso a apoyos financieros.	3	8
14	Promover las fortalezas y valores de los productos yucatecos.	4	8
15	Fomentar el crecimiento de la cadena de suministros local.	4	8
16	Favorecer el vínculo entre las empresas locales, nacionales e internacionales para la comercialización de sus productos.	4	8
17	Motivar la participación en exposiciones comerciales para la expansión de los productos yucatecos.	4	8
18	Incentivar la innovación en las empresas para que sus productos y servicios sean más competitivos.	5	17
19	Promover la producción de insumos acordes con la demanda del mercado	5	17
20	Incentivar la conformación de órganos ciudadanos, académicos y empresariales en materia de competitividad para implementar y dar seguimiento a buenas prácticas.	5	17
21	Promover el desarrollo de las zonas estratégicas  en el estado que respondan a sus vocaciones particulares.	5	17
22	Poner en marcha campañas para la atracción de talento en Yucatán.	6	17
23	Promover el establecimiento de incentivos en el sector privado que permitan incrementar la inversión en capital humano.	6	17
24	Elaborar un plan de gestión de capacidades técnicas que cubra las demandas del mercado local.	6	17
25	Difundir las bondades estratégicas y geográficas del estado	7	17
26	Promocionar los casos de éxito de las empresas que invierten en Yucatán.	7	17
27	Promover la relevancia, valor y diferenciación del estado de forma coordinada con los sectores público, privado, social y académico.	7	17
28	Gestionar estímulos fiscales rentables y atractivos a las inversiones internacionales	8	17
29	Impulsar el potencial de recursos energéticos y la infraestructura necesaria para la atracción de inversiones.	8	17
30	Propiciar un marco regulatorio eficiente para la atracción de inversiones en el estado.	8	17
31	Facilitar mediante herramientas digitales el proceso de inversión en Yucatán.	8	17
32	Impulsar esquemas de acompañamiento en las empresas para proveer la demanda de insumos industriales.	9	9
33	Establecer programas de sensibilización hacia la sostenibilidad industrial.	9	9
34	Estimular el diseño de procesos y productos industriales innovadores.	9	9
35	Facilitar la modernización de infraestructura logística para la movilización eficiente de productos industriales	10	9
36	Impulsar la disponibilidad energética para la realización de procesos de producción eficientes en el sector industrial.	10	9
37	Promover la constitución y modernización de parques industriales sostenibles e incluyentes.	10	9
38	Promover el aprovechamiento de las Tecnologías de Información y Comunicación (TICs) en la industria del estado.	10	9
39	Promover el progreso industrial sostenible en las zonas estratégicas del estado.	11	9
40	Reforzar los financiamientos a empresas y emprendedores del sector industrial con enfoque sostenible.	11	9
41	Garantizar la capacidad técnica industrial a través de la vinculación con el sector académico.	11	9
42	Promover la creación de grupos empresariales con enfoque de impulso a grupos en situación de vulnerabilidad.	12	9
43	Mejorar las condiciones de trabajo para los grupos en situación de vulnerabilidad por medio de programas de equidad en el sector industrial.	12	9
44	Diseñar mecanismos de regulación ambiental para fomentar la sostenibilidad de las empresas del sector secundario. 	12	9
45	Promover incentivos para desarrollar una cultura sostenible dentro del sector secundario.	12	9
46	Implementar talleres de sostenibilidad y de profesionalización para los prestadores de servicios turísticos del estado.	13	8
47	Promover distintivos y certificados de calidad a las empresas turísticas del estado.	13	8
48	Impulsar la adopción de sellos de calidad y certificaciones en los restaurantes que usan productos locales.	13	8
49	Consolidar los instrumentos y sistemas de información estadística y geográfica en materia turística.	13	8
50	Vincular al sector empresarial turístico con organismos de educación para la integración de mano de obra especializada y certificada al mercado laboral.	13	8
51	Estimular el diseño de nuevos proyectos turísticos sostenibles en el estado.	14	8
52	Fomentar la creación de productos y servicios turísticos sustentables e innovadores.	14	8
53	Impulsar el aprovechamiento de nichos de mercado novedosos y con alta demanda.	14	8
54	Promover alianzas estratégicas con empresas turísticas nacionales e internacionales.	14	8
55	Fomentar la participación del estado en eventos de promoción turística nacionales e internacionales.	15	8
56	Promocionar la imagen de la cultura maya en las campañas de promoción nacional e internacional.	15	8
57	Realizar campañas de promoción turística a través de diferentes medios nacionales e internacionales.	15	8
58	Impulsar el uso de herramientas tecnológicas para la difusión turística en medios electrónicos.	15	8
59	Promover la apertura de más rutas aéreas que se conecten a lugares estratégicos.	15	8
518	Proponer esquemas integrales de movilidad urbana.	111	11
60	Establecer vínculos con los ayuntamientos para mejorar la imagen turística del estado.	15	8
61	Reforzar las acciones de atracción de turistas de negocios, con énfasis en la calidad y diversidad de los productos y servicios locales.	15	8
62	Restaurar la infraestructura de servicios para el turismo sostenible.	16	8
63	Consolidar el segmento de turismo de naturaleza en los municipios turísticos.	16	8
64	Poner en marcha acciones integrales de atracción y comercialización de congresos y convenciones nacionales e internacionales.	16	8
65	Diseñar programas de comercialización de productos y servicios para el turismo de lujo.	16	8
66	Facilitar la prestación de servicios de movilidad turística sostenible a través del vínculo entre el sector académico, empresarial y público.	16	8
67	Reforzar el desarrollo de centros turísticos en zonas con alto patrimonio cultural.	16	8
68	Promocionar la oferta de todos los segmentos turísticos consolidados y potenciales con un enfoque particular para cada tipo.	16	8
69	Promover que los residentes de la península consuman los productos y servicios turísticos de Yucatán.	16	8
70	Promover el turismo médico para convertirse en una opción más de servicios y productos turísticos que generen empleos y beneficios para el estado.	16	8
71	Impulsar la celebración de festivales, exposiciones y eventos turísticos de talla internacional.	17	8
72	Adecuar los paradores turísticos del estado para que sean modernos, accesibles e incluyentes.	17	8
73	Adecuar la infraestructura turística a las nuevas demandas y necesidades del mercado con especial énfasis en la accesibilidad	17	8
74	Rescatar los espacios con alto valor turístico para los visitantes nacionales e internacionales.	17	8
75	Diseñar herramientas digitales que faciliten la difusión de los atractivos turísticos en segmentos preferentes.	17	8
76	 Coordinar la realización de eventos turísticos y gastronómicos en los municipios del estado en conjunto con los sectores público, privado y social.	17	8
77	Promover las ferias y tradiciones del estado en localidades con alto potencial turístico.	17	8
78	Desarrollar el turismo alternativo y comunitario en los municipios con mayor potencial.	18	8
79	Otorgar facilidades de acceso a la oferta turística a los yucatecos y nacionales con énfasis a la población en situación de vulnerabilidad.	18	8
80	Fomentar la accesibilidad en los servicios turísticos del estado.	18	8
81	Facilitar el establecimiento de nuevas rutas turísticas sostenibles en las comunidades del estado respetando su identidad cultural.	18	8
82	Promover a los artesanos, comerciantes y productores turísticos y gastronómicos locales en ferias y eventos turísticos nacionales e internacionales.	18	8
83	Vincular los sectores público, privado, social y académico para mejorar el acceso laboral incluyente y productivo	19	8
84	Fomentar el establecimiento de condiciones justas, equitativas y satisfactorias de trabajo en las empresas.	19	8
85	Promover los beneficios del sentido de identidad y pertenencia como consecuencia de mejores condiciones laborales.	19	8
86	Facilitar la inserción en el mercado laboral de todos los grupos sociales.	19	8
87	Promocionar los beneficios de la formalidad laboral entre la población trabajadora	20	8
88	Coadyuvar con el sector privado para que las empresas impulsen la seguridad social laboral.	20	8
89	Impulsar la coordinación efectiva entre los distintos órdenes de gobierno para mejorar los procesos de incorporación a la seguridad social.	20	8
90	Capacitar a las empresas en materia de productividad laboral.	21	8
91	Implementar tecnología que mejore la productividad de las empresas.	21	8
92	Promover la formación de capital humano en las áreas de alto impacto económico acorde con la demanda actual y emergente.	21	8
93	Crear un certificado de calidad para las empresas con alta productividad.	22	8
94	Otorgar estímulos a las empresas que cuenten con procesos de calidad y demuestren ser productivas.	22	8
95	Promocionar las mejores prácticas de productividad en las empresas.	22	8
96	Promover una cultura del emprendimiento desde la educación básica.	23	8
97	Fomentar la formación de aptitudes empresariales en estudiantes de educación media superior y superior.	23	8
98	Promover la aplicación de buenas prácticas en materia de emprendimiento.	23	8
99	Ofrecer capacitación con valor curricular, para acciones de emprendimiento en el hogar, que permitan conciliar la vida laboral y personal.	23	8
100	Celebrar convenios de colaboración entre organizaciones de apoyo al ecosistema emprendedor para la formación de capacidades de emprendimiento.	23	8
101	Realizar un seguimiento a las acciones de emprendimiento a fin de detectar necesidades de formación, fortalezas y áreas de oportunidad.	23	8
102	Impulsar la formación de equipos multidisciplinarios y con diferentes niveles de especialización técnica para la formación de capacidades empresariales.	23	8
103	Desarrollar programas formativos de habilidades proactivas para aumentar la eficiencia y la modernización continua en las empresas.	23	8
104	Crear programas de inversión para emprendedores con esquemas de financiamiento vinculados a los sectores público, privado, social y académico.	23	8
105	Desarrollar acciones de fortalecimiento a emprendedores con enfoque de inclusión.	24	8
106	Incorporar habilidades de liderazgo y herramientas de empoderamiento en los procesos de acompañamiento y formación.	24	8
107	Promover redes que impulsen de manera focalizada el emprendimiento inclusivo a través del acompañamiento, servicios y comercialización de sus productos.	24	8
108	Establecer espacios físicos o virtuales que permitan visibilizar las acciones de los emprendedores locales en el ecosistema de emprendimiento de la entidad.	25	8
109	Impulsar el uso de las Tecnologías de la Información y Comunicación (TICs) para vincular a los emprendedores en la cadena de valor que les permita mejorar la competitividad empresarial.	25	8
110	Promover el uso de tecnología e innovación para acceder a los financiamientos	25	8
111	Promover la integración de cadenas de valor para aumentar la productividad, calidad y rentabilidad entre los emprendedores.	25	8
112	Gestionar insumos para los productores del sector agropecuario.	26	2
113	Generar acciones para promover el consumo de productos agropecuarios locales. 	26	2
114	Impulsar esquemas de financiamiento y reducción de costos financieros para el desarrollo del sector agropecuario.	26	2
115	Impulsar acciones de infraestructura que faciliten el movimiento de productos agropecuarios.	26	2
116	Inducir la implementación de procesos productivos que favorezcan la producción sostenible.	26	2
117	Favorecer la comercialización estratégica de productos agropecuarios locales en los mercados locales e internacionales.	26	2
118	Desarrollar acciones para la industrialización de productos agropecuarios de manera sostenible	26	2
119	Fomentar la creación de mecanismos para el aprovechamiento de la denominación de origen de productos locales o indicación geográfica. 	26	2
120	Impulsar la certificación de la calidad de los productos agropecuarios.	26	2
121	Desarrollar acciones para el aprovechamiento de productos agropecuarios de manera sostenible	26	2
122	Promover la investigación y el desarrollo de innovaciones en los procesos agropecuarios.	26	2
123	Impulsar la mecanización y tecnificación del sector agropecuario.	27	2
124	Apoyar a productores y emprendedores en procesos y técnicas que permitan mejorar la calidad de su producción.	27	2
125	Dotar de herramientas tecnológicas al sector agropecuario para mejorar sus procesos productivos.	27	2
126	Promover el uso de insumos que minimicen el impacto ambiental. 	28	2
127	Incentivar prácticas que ayuden a la conservación de los recursos hídricos del estado.	28	2
128	Gestionar herramientas para los productores a efecto de prevenir y combatir plagas y enfermedades en el sector agropecuario.	28	2
129	Realizar un control de seguimiento y vigilancia de sanidad a las unidades productivas así como reforzar los comités de sanidad estatal.	28	2
130	Incentivar la producción de las especies con mayor valor y rendimiento.	29	2
131	Proporcionar cursos de inocuidad pecuaria sostenible.	29	2
132	Otorgar apoyos para la producción pecuaria.	29	2
133	Impulsar la creación de centros de transferencia y tecnología genética pecuaria.	30	2
134	Promover la asistencia técnica y la capacitación en procesos reproductivos que mejoren la genética de las especies pecuarias.	30	2
135	Otorgar apoyos para el mejoramiento genético del sector pecuario. 	30	2
136	Apoyar la producción agrícola de los cultivos con mayor rentabilidad en el estado.	31	2
137	Reforzar la cadena de suministros agroalimentaria estatal.	31	2
138	Consolidar programas de producción y consumo responsable en temas agroalimentarios	31	2
139	Favorecer el cultivo de productos agrícolas con mayor demanda en la entidad.	31	2
140	Promover mecanismos que propicien el trato directo entre los productores agrícolas y los consumidores finales.	31	2
141	Reforzar la inocuidad y diversificación de la producción agrícola.	32	2
142	Estimular las inversiones que favorezcan la calidad y disponibilidad de productos agrícolas.	32	2
143	Proporcionar capacitación sobre prevención de riesgos en materia agrícola.	32	2
144	Modernizar la infraestructura para la pesca a fin de dar mayor valor agregado a los productos pesqueros.	33	2
145	Brindar asesoría a las empresas pesqueras y acuícolas del estado.	33	2
146	Reforzar la cadena productiva y los canales de comercialización del sector pesquero de forma eficiente.	33	2
147	Impulsar campañas de concientización de los tiempos de veda y fomento de la acuacultura en el estado.	33	2
148	Incentivar el emprendimiento para fomentar la creación de unidades económicas del sector pesquero y acuícola.	33	2
149	Establecer mecanismos de control y seguimiento de embarcaciones pesqueras y unidades acuícolas.	33	2
150	Gestionar campañas de concientización sobre el valor nutrimental de los productos acuícolas y pesqueros en el estado.	34	2
151	Promover el desarrollo de proyectos de granjas acuícolas en las poblaciones rurales.	34	2
152	Fomentar la cultura de consumo de productos pesqueros y acuícolas entre la población rural.	34	2
153	Gestionar apoyos justos para mejorar la calidad de las condiciones de los pescadores.	34	2
154	Consolidar la producción de especies marinas nativas mediante la elaboración de estudios que garanticen la capacidad productiva.	35	2
155	Suscribir convenios con instituciones educativas para capacitar a los productores pesqueros y acuícolas del estado.	35	2
156	Impulsar el uso de tecnología para las actividades pesqueras y acuícolas sostenibles.	35	2
157	Crear campañas de concientización sobre la preservación del medio ambiente de la zona costera.	36	2
158	Realizar un monitoreo de la calidad de las aguas subterráneas para identificar zonas con potencial acuícola en el estado.	36	2
159	Identificar zonas de anidamiento y reproducción de las especies marítimas para reforzar la vigilancia.	36	2
160	Organizar las unidades de primer nivel y la atención pre hospitalaria con medicamentos suficientes, equipos especializados y atención de los padecimientos con mayor frecuencia en las regiones de mayor vulnerabilidad social.  	37	3
161	Reorganizar el recurso humano de salud con esquemas que amplíen la cobertura en el estado y aseguren la atención en hogares y comunidades.  	37	3
162	Expandir los horarios de atención médica para brindar servicios  en el primer nivel de atención durante las 24 horas del día, los siete días de la semana. 	37	3
163	Incentivar la actualización de los conocimientos y habilidades del personal de salud con la participación de organismos oficialmente reconocidos y especializados en temas de prevención y atención integral.	38	3
164	Consolidar la calidad del servicio médico mediante la ampliación y certificación periódica del personal de salud. 	38	3
499	Reforzar programas de educación ambiental enfocados a la conservación costera. 	106	14
165	Promover el apego de los establecimientos de salud públicos y privados a los estándares de calidad en materia de salud.	38	3
166	Fortalecer la relación médico-paciente en los tres niveles de atención especializada de forma incluyente.	38	3
167	Incentivar la aplicación de la biotecnología roja en los procesos médicos. 	38	3
168	Promover una red de hospitales que cuenten con las herramientas y tecnologías robóticas, para brindar un servicio especializado y de vanguardia al cuidado de la salud.	38	3
169	Consolidar la infraestructura y equipamiento de las unidades médicas y establecimientos de apoyo para la prestación de servicios de manera incluyente y sostenible.	39	3
170	Expandir la cobertura de conectividad de voz y datos en las unidades médicas y establecimientos pertenecientes al Sistema Estatal de Salud. 	39	3
171	Modernizar la infraestructura y  promover el uso y aprovechamiento de sistemas informáticos integrales en el Sistema Estatal de Salud. 	39	3
172	Consolidar el uso de las tecnologías de la información para la capacitación continua del personal de salud, así como la prestación de servicios médicos a distancia.	39	3
173	Impulsar la atención integral en las unidades médicas del sector salud a personas que viven violencia familiar y/o sexual con énfasis en grupos en situación de vulnerabilidad.	40	3
174	Fomentar la atención integral a las mujeres, en especial durante el periodo preconcepcional, embarazo, parto y puerperio, para disminuir el riesgo de enfermedades obstétricas y ginecológicas.	40	3
175	Promover en las unidades médicas la orientación y consejería sobre planificación familiar. 	40	3
176	Desarrollar mecanismos de prevención y atención integral que reduzcan la mortalidad materna e infantil.	40	3
177	Promover nuevas habilidades y formas de comportamiento entre la población en situación de vulnerabilidad que permita el mejoramiento de las condiciones de salud en el estado.	40	3
178	Reforzar los esquemas de atención en el sector público para la detección y atención oportuna de las enfermedades que más afectan a la población en situación de vulnerabilidad.	40	3
179	Reforzar las acciones de prevención y atención integral de enfermedades crónico degenerativas, respiratorias e infecto contagiosas, con énfasis en las más frecuentes.	41	3
180	Promover la cobertura de vacunación universal con esquema completo. 	41	3
181	Reforzar las acciones de prevención y control de las enfermedades transmitidas por vector y zoonosis. 	41	3
182	Impulsar acciones de promoción de la salud, prevención y atención integral de enfermedades metabólicas asociadas a la nutrición, para el combate al sobrepeso y la obesidad.	41	3
183	Reforzar los mecanismos de prevención, detección, atención integral y seguimiento de las personas que viven con el Virus de la Inmunodeficiencia Humana (VIH), sida y otras Infecciones de Transmisión Sexual (ITS).	41	3
184	Reforzar las acciones de promoción de la salud en la población para la adopción habitual de medidas de autocuidado de la salud, donación de sangre y adopción de conductas seguras en la vida cotidiana.	41	3
185	Reforzar las acciones de prevención, atención y control de trastornos mentales, padecimientos psicosociales y por uso de sustancias, con especial atención en la población en situación de vulnerabilidad.	42	3
186	Desarrollar acciones de mejora en las unidades de atención de salud mental con enfoque a los derechos humanos de los pacientes.	42	3
187	Promover acciones de sensibilización en los diferentes órdenes de gobierno y la sociedad sobre salud mental y la inclusión de personas con trastornos mentales.	42	3
188	Reforzar la vigilancia y control sanitario de establecimientos que ofrecen bienes y servicios de uso y/o consumo humano así como los asociados a factores ambientales y de salud ocupacional.	43	3
189	Reforzar las acciones de saneamiento básico derivados de la ocurrencia de emergencias y desastres en el estado.	43	3
190	Consolidar las acciones de regulación de la publicidad y etiquetado de alimentos, bebidas y sustancias adictivas legales. 	43	3
191	Fomentar la actualización del marco legal que permita la regulación sanitaria de establecimientos no formales.	43	3
192	Gestionar el trabajo colaborativo en las comunidades, escuelas y municipios que propicien la certificación como entornos saludables que mejoran la condición de vida de la población.	44	3
193	Promover la colaboración con las y los auxiliares de salud comunitarias y parteras tradicionales en el reforzamiento de acciones de prevención y control de las enfermedades en las comunidades.	44	3
194	Reforzar la colaboración interinstitucional en la operación de puntos de control de niveles de alcohol en conductores de vehículos como medida en la prevención de accidentes viales.	44	3
195	Generar acciones que garanticen la sostenibilidad alimentaria de las personas en condición de desnutrición.	45	2
196	Incentivar la sana alimentación de las y los lactantes y de la niñez en el desarrollo de la primera infancia.	45	2
197	Elaborar campañas que incentiven el consumo de alimentos con alta calidad nutricia y erradiquen conductas alimentarias que generan desnutrición.	45	2
198	Coordinar acciones entre los sectores académico, privado, público y personas de las comunidades para abatir el hambre.	45	2
199	Proporcionar capacitación sobre los hábitos de una buena alimentación y la importancia del desarrollo infantil.	46	2
200	Apoyar a las personas y/u organizaciones que propicien la sana alimentación en las comunidades de mayor carencia alimentaria en el estado.	46	2
201	Promover la entrega de desayunos y paquetes alimenticios con alta calidad nutricia, priorizando a las comunidades con mayor marginación y carencia alimentaria, previniendo las enfermedades relacionadas con la desnutrición.	46	2
202	Impulsar, en coordinación con las organizaciones de la sociedad civil, una campaña de manejo de excedentes y de las pérdidas post cosecha con el objetivo de disminuir la malnutrición y desnutrición.	46	2
203	Impartir capacitaciones sobre la selección e inocuidad de cultivos con alto valor calórico.	47	2
204	Promover la investigación sobre la diversidad genética de las semillas, las plantas y los animales de granja para el procesamiento de alimentos nutritivos e inocuos.	47	2
205	Gestionar una mayor inversión en infraestructura rural, servicios de extensión, desarrollo tecnológico y en bancos de granos o de animales de crianza.	47	2
206	Promover la producción agropecuaria sostenible y la autogestión alimentaria, priorizando a las familias indígenas con mayor inseguridad alimentaria. 	48	2
207	Brindar capacitación sobre técnicas de producción eficiente y sostenible en las comunidades rurales con un enfoque incluyente.	48	2
208	Expandir el sistema educativo a las comunidades indígenas para reducir el rezago.	49	10
209	Realizar acciones que garanticen la seguridad alimentaria y nutricional en los pueblos indígenas.	49	10
210	Extender la cobertura y calidad de los servicios de salud en las comunidades indígenas.	49	10
211	Proporcionar la infraestructura adecuada y de calidad para las viviendas en comunidades indígenas.	49	10
212	Reforzar el acceso a los servicios básicos para las viviendas en comunidades indígenas.	49	10
213	Promover el acceso a la seguridad social sostenible para las comunidades indígenas.	49	10
214	Impulsar intérpretes traductores que proveen asistencia multidisciplinaria a la población maya hablante.	50	10
215	Realizar campañas de respeto a los derechos de los maya hablantes con un lenguaje incluyente y accesible.	50	10
216	Organizar consejos comunitarios que supervisen el cumplimiento de los derechos de la población maya hablante.	50	10
217	Reforzar la medicina tradicional maya a través de un registro único de mujeres y hombres que ejercen esta milenaria práctica en Yucatán.	50	10
218	Incorporar la perspectiva de género en la asignación de apoyos y recursos de los programas federales dirigidos a la población indígena.	50	10
219	Ampliar la cobertura de los centros educativos, principalmente en las comunidades indígenas.	51	4
220	Rehabilitar los espacios educativos con infraestructura accesible e inclusiva.	51	4
221	Impulsar acciones de construcción y mantenimiento en las escuelas, que satisfagan las necesidades de los usuarios.	51	4
222	Implementar acciones de equipamiento en escuelas del interior del estado.	51	4
223	Consolidar los espacios radioeléctricos y televisivos para ofertar la educación básica y media superior en comunidades de difícil acceso.	51	4
224	Desarrollar acciones de alfabetización para la atención de jóvenes y adultos en rezago educativo.	52	4
225	Organizar grupos de enseñanza continua entre la comunidad con técnicas de aprendizaje que prioricen la atención de la primera infancia, adultos mayores y personas con discapacidad.	52	4
226	Generar apoyos para las organizaciones que combaten el analfabetismo.	52	4
227	Impartir asesoría extra clases en comunidades con altos niveles de rezago educativo y/o personas con discapacidad.	52	4
228	Establecer programas de regularización educativa en las comunidades que presentan mayor rezago educativo.	52	4
229	Reforzar la profesionalización integral del personal docente para la educación multigrado y la innovación de los procesos pedagógicos, incluyendo contenidos de emprendimiento desde nivel básico.	53	4
230	Coordinar acciones que vinculen la educación media superior y superior con el mercado laboral, mediante el reforzamiento de la educación dual y profesionalización técnica.	53	4
231	Promover programas para el desarrollo socioemocional de las y los estudiantes.	53	4
232	Desarrollar mecanismos innovadores que promuevan la mejora en el desempeño de las y los estudiantes.	53	4
233	Adaptar el aprendizaje en función al uso de tecnologías de la información y comunicación.	54	4
234	Reforzar la atención y calidad de los centros que brindan educación especial y los centros de atención múltiple con enfoque de inclusión, prioritariamente aquellas asociadas con discapacidad y/o con aptitudes sobresalientes.	54	4
235	Instruir a los padres de familia y personal docente sobre el pleno desarrollo integral de las personas con necesidades educativas especiales.	54	4
236	Proporcionar materiales académicos para las y los estudiantes de educación básica y media superior, principalmente a las personas de comunidades indígenas.	54	4
237	Extender la oferta de becas para las y los estudiantes de todos los niveles educativos, priorizando a las madres adolescentes, la niñez con alguna discapacidad y la población en situación de vulnerabilidad.	54	4
238	Establecer acciones para mejorar las capacidades de las y los trabajadores sociales en las instituciones educativas, con énfasis en las zonas con altos niveles de rezago.	54	4
239	Asegurar la educación integral con un  esquema de participación social que favorezca las decisiones libres, responsables e informadas de niñas, niños y adolescentes, sobre el ejercicio de su sexualidad y salud sexual y reproductiva.	54	4
240	Promover acciones que fortalezcan la educación intercultural bilingüe y el uso de la lengua maya en escuelas públicas de educación básica.	54	4
241	Gestionar recursos para la adquisición, construcción, ampliación y mejoramiento de viviendas, principalmente en comunidades marginadas.	55	11
242	Establecer mecanismos de coordinación con los diferentes órdenes y niveles de gobierno para reforzar las acciones de vivienda.	55	11
243	Fomentar la adquisición, construcción y ampliación de viviendas adecuadas, especialmente para personas con discapacidad 	55	11
244	Desarrollar acciones que faciliten el acceso a planes de financiamiento de viviendas dignas y de bajo costo, para mujeres y  grupos en situación de vulnerabilidad.	55	11
245	Promocionar la participación de organizaciones no gubernamentales en el financiamiento para la construcción, ampliación y mejoramiento de vivienda.	55	11
246	Promover el financiamiento de desarrollos habitacionales con materiales y tecnologías que que reduzcan el consumo de energía eléctrica, emisión de CO2 y residuos contaminantes.	55	11
247	Promover el financiamiento de proyectos de desarrollos habitacionales que cumplan con la normatividad de desarrollo sustentable.	55	11
248	Establecer programas de acceso y financiamiento de viviendas sustentables o amigables con el medio ambiente.	56	11
249	Realizar acciones de vivienda con materiales duraderos, priorizando las familias en situación de pobreza y marginación.	56	11
250	Promover la investigación de nuevas propuestas para el uso de materiales alternativos y energías renovables para la construcción de viviendas.	56	11
251	Implementar programas de reubicación de viviendas asentadas en zonas de alto riesgo o no aptas para uso habitacional.	56	11
252	Promover acciones de prevención y mantenimiento de viviendas regulares con mayor exposición y vulnerabilidad a fenómenos naturales.	56	11
253	Consolidar la red de agua potable, drenaje y alcantarillado en zonas habitacionales que presenten mayor rezago.	57	11
254	Implementar acciones de electrificación que garanticen a la población el acceso a energía continua y suficiente.	57	11
255	Desarrollar acciones que permitan el acceso de la población a combustibles amigables con la salud y el medio ambiente.	57	11
256	Incentivar la participación del sector privado en la provisión de servicios básicos de calidad, suficientes y accesibles para las viviendas del estado.	57	11
257	Promover el desarrollo de nuevas opciones de financiamiento para constructores del sector privado que ofrezcan viviendas de bajo costo y con servicios básicos de calidad.	57	11
258	Promover asentamientos humanos en zonas seguras y con acceso a servicios básicos.	57	11
259	Fomentar el uso de materiales alternativos y energías renovables para dotar de servicios básicos a las viviendas.	57	11
519	Fortalecer la legislación estatal en materia de movilidad sustentable.	111	11
260	Planear redes de infraestructura que permitan la ampliación de la cobertura de los servicios de agua potable, alcantarillado y electrificación con un enfoque de desarrollo sostenible y con prioridad en las zonas marginadas.	58	11
261	Promover una normativa que garantice el desarrollo de proyectos de viviendas seguras, dignas, saludables y amigables con el medio ambiente.	58	11
262	Poner en marcha la reubicación de asentamientos en condiciones de riesgo ante fenómenos naturales, focos de contaminación o riesgos derivados de la acción humana.	58	11
263	Organizar grupos comunitarios que favorezcan el desarrollo y el bienestar social.	59	10
264	Salvaguardar el patrimonio de las familias que habitan principalmente en zonas de riesgo.	59	10
265	Asesorar a la población en situación de vulnerabilidad y de comunidades indígenas sobre el derecho a prestaciones y garantías laborales.	59	10
266	Mejorar la cobertura y beneficios del sistema de seguridad social de los trabajadores al servicio de los poderes públicos estatales y de los municipios.	59	10
267	Elaborar campañas de afiliación al sistema de salud, principalmente a la población que vive en comunidades indígenas.	60	10
268	Crear ventanillas o campañas de difusión para asesorar a las personas sobre la adquisición de un seguro.	60	10
269	Promover un sistema de protección social universal de amplio alcance en el estado.	60	10
270	Promover acciones que incentiven la participación y bienestar del adulto mayor.	61	10
271	Promover afiliaciones a la seguridad social de personas adultas mayores en comunidades con alto grado de pobreza y/o marginación.	61	10
272	Promover la seguridad social de los adultos mayores para mejorar su calidad de vida.	61	10
273	Impulsar el desarrollo de espacios incluyentes en los municipios para la realización de actividades artísticas y culturales.	62	8
274	Adecuar la infraestructura cultural existente en los municipios del estado, garantizando su resiliencia y sostenibilidad, así como la accesibilidad de personas con discapacidad, adultos mayores y mujeres embarazadas.	62	8
275	Diseñar normas y lineamientos claros y transparentes para el uso adecuado y óptimo de la infraestructura cultural.	62	8
276	Optimizar espacios e infraestructura existente para la realización de actividades y eventos artísticos y culturales.	62	8
277	Implementar acciones, como circuitos culturales, que favorezcan  la descentralización de los servicios artísticos y culturales hacia zonas de Mérida con baja oferta cultural y municipios del interior del estado, garantizando la inclusión y accesibilidad.	63	8
278	Estimular la diversificación de la oferta cultural apoyando producciones de grupos independientes y emitiendo convocatorias incluyentes.	63	8
279	Incentivar la creación de productos y servicios artísticos y culturales, con enfoque de igualdad de género. 	63	8
280	Estimular el desarrollo y consolidación de niños y jóvenes creadores mediante recintos culturales infantiles, creación de compañías de danza y teatro infantil, así como la realización de eventos y festivales.	63	8
281	Incentivar proyectos y eventos culturales que busquen la transformación social y sensibilización en inclusión, igualdad y atención a grupos en situación de vulnerabilidad.	63	8
282	Diseñar un sistema de información cultural que permita medir cuantitativamente y cualitativamente la economía cultural e industrias creativas	64	8
283	Incorporar a las políticas públicas, programas y acciones la validación de la dimensión comercial de la cultura y la creatividad, y su articulación con tecnología, infraestructura, acceso a mercados, entre otros	64	8
284	Implementar acciones de vinculación entre el sector cultural e industrias creativas con el sector turístico y económico.	64	8
285	Motivar la innovación dentro de las industrias culturales y creativas, que estimulen a los artistas y creadores locales.	64	8
286	Utilizar herramientas de las Tecnologías de la Información y la Comunicación para acercar los eventos, productos y servicios culturales a la población.      	65	12
287	Desarrollar acciones de sensibilización del valor del arte y la cultura para contribuir a crear nuevos públicos.       	65	12
288	Propiciar una vinculación interinstitucional entre los tres órdenes de gobierno para impulsar el arte y la cultura.  	65	12
289	Generar información que ayude a mejorar la realización de los eventos artísticos y culturales a partir de la opinión y participación incluyente de la población.	66	12
290	Implementar acciones de movilidad para facilitar el consumo de bienes y servicios culturales.	66	12
291	Impulsar la incorporación  de grupos artísticos de municipios y comunidades a la agenda cultural del estado	66	12
292	Incentivar el consumo de la población de eventos, productos y servicios culturales mediante apoyos o subvenciones.	66	12
293	Motivar la generación, edición y publicación de medios escritos (libros, revistas, periódicos, entre otros), procurando la producción incluyente y con contenido dirigido a grupos en situación de vulnerabilidad, así como la generación de obras inéditas en diferentes géneros literarios, de investigación y divulgación científica.	67	12
294	Crear colecciones incluyentes en formatos económicos.	67	12
392	Generar plataformas de comunicación para la difusión de la conservación y el desarrollo sustentable en el territorio. 	85	15
295	Desarrollar acciones que garanticen la distribución de los libros editados y coeditados en formato impreso y digital abarcando todas las regiones del estado, así como el mercado nacional e internacional.	67	12
296	Promover la realización de ferias y festivales del libro y lectura en los municipios del estado.                                                                                                                                                                                            	67	12
297	Reforzar las salas de lectura y bibliotecas en los municipios del estado con acciones de mantenimiento, equipamiento y acervo bibliográfico.	67	12
298	Estimular el acceso a la lectura en lugares no convencionales como áreas hospitalarias, cárceles, orfanatos, entre otros.	67	12
299	Desarrollar actividades de fomento a la lectura dirigidos a toda la población, pero con énfasis a niños, personas con discapacidad y grupos específicos que tengan limitaciones para acceder a medios escritos.  	67	12
300	Estimular la investigación y difusión de las manifestaciones de la cultura tradicional en Yucatán.	68	10
301	Fomentar espacios de difusión en medios de comunicación de la cultura maya, su lengua, tradiciones, costumbres e historia.	68	10
520	Incentivar la transición del uso del automóvil hacia medios de transporte más sustentables.	111	11
302	Impulsar acciones de profesionalización para los gestores y artistas de la cultura e identidad yucateca en sus propias localidades.	68	10
303	Incentivar la difusión de la identidad cultural, tradiciones, historias y costumbres mediante agentes locales, preferentemente mujeres mayahablantes.	68	10
304	Incentivar el desarrollo de eventos incluyentes que conlleven al rescate de los juegos y deportes tradicionales.	68	10
305	Implementar acciones que promuevan el orgullo por la cultura tradicional.	68	10
306	Exponer las expresiones artísticas, tradiciones y costumbres en un ámbito internacional a través de intercambios, colaboraciones y fusiones artísticas.	68	10
307	Reestructurar la agenda cultural de las comunidades y colonias, implementando eventos y actividades en lengua maya.	68	10
308	Incentivar los eventos y actividades de cultura tradicional como trova, teatro regional, bailes, festividades, fiestas patronales, entre otros.	68	10
309	Establecer una vinculación entre los sectores cultural y educativo para implementar proyectos y programas educativos que favorezcan la preservación y desarrollo de las culturas populares y la cultura maya.	68	10
310	Estimular la creación de proyectos viables y autogestivos de la artesanía local.	69	10
311	Fomentar el interés por la creación artesanal en los jóvenes para preservar las costumbres y tradiciones.	69	10
312	Reforzar los canales de distribución y comercialización de productos artesanales en zonas estratégicas del estado.	69	10
313	Impulsar la creación de espacios físicos y virtuales para exposiciones y muestras artesanales.	69	10
314	Impartir talleres de formación y actualización artística garantizando la inclusión social y equidad.	70	11
315	Otorgar apoyos que incentiven la formación y profesionalización de nuevos artistas y creadores.	70	11
316	Consolidar los programas de educación y formación artística formal desde el grado inicial hasta posgrado con una perspectiva incluyente.	70	11
317	Implementar acciones que faciliten la empleabilidad de nuevos artistas.	70	11
318	Consolidar los centros de educación artística públicos y privados encargados de generar nuevos artistas.	70	11
319	Facilitar a los artistas y creadores el acceso a espacios incluyentes de expresión de las artes.	71	11
320	Estimular la creación de manifestaciones artísticas enfocadas en las artes.	71	11
321	Establecer esquemas de colaboración público-privada que permitan apoyar a compañías, artistas y creadores dedicados al teatro, artes escénicas, música, danza, artes visuales, multimedia  y demás manifestaciones artísticas contemporáneas.	71	11
322	Incentivar la profesionalización continua de los artistas y creadores mediante la vinculación de programas de profesionalización del sector privado y de los tres órdenes de gobierno.	71	11
323	Diagnosticar la calidad de la educación artística utilizando criterios y lineamientos pedagógicos de reconocida calidad nacional e internacional.	72	4
324	Reforzar la educación artística con enfoque intercultural e incluyente.	72	4
325	Promover la realización de actividades artísticas extraescolares.	72	4
326	Habilitar los Centros Culturales como un foro que ayude a la enseñanza y apreciación  del arte en la educación básica. 	72	4
327	Consolidar las capacidades docentes de las y los maestros de educación artística, haciendo hincapié en la adopción de enfoques pedagógicos actuales, administración eficiente del tiempo y nuevas estrategias de enseñanza-aprendizaje.	72	4
328	Crear espacios para el intercambio y crecimiento profesional de los docentes de educación artística. 	72	4
329	Motivar las manifestaciones artísticas de profesionales y profesionistas en el sistema educativo.	73	4
330	Incentivar la realización de eventos artísticos y culturales en instituciones educativas.	73	4
331	Estimular que los profesionales del arte participen con las escuelas de educación básica para mejorar los programas y procedimientos de impartición artística.	73	4
332	Promover los programas de educación superior en artes como opción de formación integral.	74	4
333	Apoyar los procesos de evaluación y acreditación de licenciaturas en artes en el estado.	74	4
334	Desarrollar acciones que garanticen el acceso incluyente a la formación artística.	74	4
335	Incentivar la labor social del estudiante en artes para que fomente la cultura y su valoración.	74	4
336	Implementar acciones que mejoren la calidad de la enseñanza en artes.	74	4
337	Proporcionar apoyos o estímulos para los estudiantes en artes para que motiven su permanencia y egreso.	74	4
338	Consolidar la formación de cuerpos académicos de educación superior en artes.	74	4
339	Gestionar apoyos y estímulos a las instituciones y escuelas que tengan programas de enseñanza superior de arte.	75	4
340	Estimular el emprendimiento y autoempleo de los estudiantes en artes.	75	4
341	Vincular a los estudiantes de educación superior en artes con el mercado laboral.	75	4
342	Fortalecer las capacidades de gestión y administrativas de las instituciones de educación superior en arte involucrando a la  sociedad civil principalmente en el patronazgo.	75	4
343	Conformar una instancia ciudadana con participación de distintos actores sociales, públicos y privados que coadyuve a promover, difundir y preservar el patrimonio cultural del estado.    	76	11
344	Promover mecanismos de intercambio de información sobre el estado y ubicación del patrimonio cultural entre académicos, gestores e instituciones de la Administración Pública.	76	11
345	Promover en los municipios y comisarías la realización de eventos, exhibiciones y actividades que promuevan el conocimiento y  conservación del patrimonio cultural.	76	11
346	Actualizar la normatividad vigente en materia de protección y promoción del patrimonio cultural.	77	11
347	Sensibilizar a los ayuntamientos a que tengan una participación activa en la protección e integración de su patrimonio.	77	11
348	Gestionar la preservación de los acervos bibliográficos, hemerográficos, documentales y audiovisuales. 	77	11
349	Incentivar acciones y proyectos encaminados a la preservación del patrimonio cultural.	77	11
350	Proporcionar mantenimiento a museos que promuevan y difundan el patrimonio cultural.	77	11
351	Generar nuevos contenidos en la red de museos que promuevan el patrimonio cultural.	77	11
352	Crear esquemas de identificación de talentos deportivos a través de un equipo multidisciplinario así como incentivar el desarrollo y permanencia de los talentos deportivos con enfoque incluyente.	78	3
353	Proporcionar seguimiento y acompañamiento permanente a los talentos deportivos con enfoque incluyente.	78	3
354	Reforzar los programas de exhibición de las disciplinas deportivas tradicionales y de deporte adaptado en escuelas de nivel preescolar y primaria.	78	3
355	Establecer convenios de coordinación de impulso al deporte entre las escuelas y centros educativos que faciliten los entrenamientos de los atletas con un enfoque incluyente.	78	3
356	Diseñar metodologías deportivas que abarquen todas las etapas de formación de un deportista.	78	3
357	Proporcionar una atención integral a los deportistas de alto rendimiento con enfoque incluyente, y usando evidencia científica que coadyuve al desarrollo de los deportistas.	79	3
358	Consolidar las capacidades de médicos y entrenadores de los deportistas de alto rendimiento con un enfoque incluyente.	79	3
359	Crear programas de entrenamiento adecuados para los deportistas de alto rendimiento con un enfoque incluyente.	79	3
360	Gestionar infraestructura deportiva incluyente y de calidad para la práctica de deporte de alto rendimiento.	79	3
361	Diseñar esquemas financieros que sirvan como base para proporcionar estímulos a entrenadores y deportistas, en especial a los que ganen medallas en competencias de alto rendimiento.	79	3
362	Consolidar la infraestructura deportiva existente garantizando su sostenibilidad para la realización de eventos y actividades físicas incluyentes.	80	3
363	Promover la generación de espacios deportivos incluyentes y resilientes.	80	3
364	Crear convenios de colaboración con la iniciativa privada para realizar acciones de rescate y mantenimiento de espacios públicos, así como implementar programas de activación física.	80	3
365	Crear una campaña para la activación física en municipios aprovechando la infraestructura existente (parques y campos).	80	3
366	Motivar la práctica deportiva y activación física desde la infancia.	80	3
367	Promover la vinculación laboral entre profesionistas y especialistas del deporte con los programas de activación física.	80	3
368	Diseñar programas especiales de activación física de acuerdo con padecimientos de salud.	80	3
369	Promover estrategias de prevención de la salud con acciones que impulsen la activación física de las y los estudiantes de educación básica	80	3
370	Apoyar a diferentes actores que promuevan o fomenten el deporte recreativo.	81	3
371	Crear academias que promuevan la práctica de deportes de conjunto, con un enfoque incluyente y que abarque a todos los municipios del interior del estado.	81	3
372	Utilizar técnicas alternativas que generen el interés por el deporte.	81	3
373	Crear modelos de inclusión en la realización de actividades deportivas para personas con alguna discapacidad.	81	3
374	Motivar la realización de  torneos y eventos deportivos	81	3
375	Fomentar esquemas de traslado económico y seguro para los aficionados a eventos deportivos profesionales.	82	12
376	Promover que el sistema de transporte proporcione una atención prioritaria en eventos deportivos masivos.	82	12
377	Crear sinergias con instituciones públicas y privadas para la promoción de eventos deportivos.	82	12
378	Implementar acciones que faciliten el acceso incluyente y seguro a los eventos deportivos.	82	12
379	Implementar acciones que fomenten la identidad de la población con los equipos profesionales.	83	12
380	Crear alianzas con instituciones públicas y privadas para la realización de eventos deportivos.	83	12
381	Incentivar la creación de eventos deportivos profesionales procurando la participación de equipos o jugadores  nacionales e internacionales.	83	12
382	Considerar las áreas naturales protegidas en los programas de desarrollo urbano como instrumentos básicos del ordenamiento territorial.	84	15
383	Estimular la creación de consejos comunitarios de vigilancia en todas las áreas protegidas del estado.	84	15
384	Elaborar y dar seguimiento a programas de rehabilitación, reforestación y revegetación de los diversos ecosistemas presentes en las áreas naturales protegidas.	84	15
385	Administrar las áreas naturales protegidas estatales para garantizar su protección.	84	15
386	Promover el manejo sustentable de los recursos naturales endémicos que incrementen la reforestación.	84	15
387	Implementar acciones de conservación de la superficie con vegetación.	84	15
388	Realizar la vinculación con los tres órdenes de gobierno para implementar acciones de arborización con participación ciudadana, en las áreas naturales protegidas. 	84	15
389	Promover sistemas silvopastoriles  que permitan el uso sustentable del suelo.	85	15
390	Guiar la participación de los municipios en la protección y respeto de las zonas sujetas a conservación, promoviendo la vigilancia en los territorios de su competencia. 	85	15
391	Proteger y aprovechar de manera sustentable la biodiversidad en el estado.	85	15
393	Reforzar la protección de animales de crianza, domésticos, de trabajo o de situación de calle.	85	15
394	Gestionar la creación de refugios de animales y veterinarias municipales	85	15
395	Promocionar campañas de educación y respeto hacia todos los animales en los municipios.	85	15
396	Establecer programas de rescate y atención de especies en peligro de extinción que habitan en el estado.	85	15
397	Plantear mecanismos de retribución económica que permitan el financiamiento de proyectos para la conservación de la naturaleza.	85	15
398	Promover una legislación en favor de la protección y bienestar animal.	86	15
399	Gestionar que los municipios cuenten con instrumentos de regulación ecológica que permitan la protección, preservación, restauración y aprovechamiento sustentable de los recursos naturales.	86	15
400	Promover la cultura del medio ambiente desde la edad escolar para generar conciencia.	86	15
401	Planificar el uso público de todos aquellos espacios protegidos que tengan entre sus objetivos facilitar el disfrute cultural, educativo y recreativo de la naturaleza.	86	15
402	Gestionar recursos y financiamientos a favor de programas de manejo sustentable de los recursos naturales.	86	15
403	Generar conocimientos en materia de gestión del ordenamiento ecológico territorial entre  los ayuntamientos.	86	15
404	Promover acciones que protejan el sistema kárstico del estado. 	86	15
405	Proponer una cultura forestal de sensibilización, organización y capacitación en los municipios.	86	15
406	Promover la investigación científica enfocada al conocimiento y protección de la biodiversidad y los recursos naturales del estado.	86	15
407	Dirigir la cooperación técnica ambiental con instituciones de educación superior, centros de investigación, organismos y agencias nacionales e internacionales.	86	15
408	Implementar mecanismos de control y vigilancia del uso de agroquímicos en la agricultura.	86	15
409	Impulsar la producción y uso de plantas nativas para la  arborización con principal atención a zonas prioritarias.	87	15
410	Promover la producción de plantas nativas que presten servicios ambientales a las comunidades.	87	15
411	Promover una mayor cobertura forestal a través de especies nativas.	87	15
412	Reforzar la normatividad legal aplicable a las acciones que contrarresten la deforestación en el estado. 	88	15
413	Realizar la vinculación con los tres órdenes de gobierno para implementar acciones de arborización de terrenos y edificios públicos con participación ciudadana.	88	15
414	Implementar programas enfocados a reducir la pérdida de cobertura forestal del estado. 	88	15
415	Regular el cambio de uso del suelo en terrenos forestales para su conservación y uso adecuado para evitar la degradación.	88	15
416	Elaborar diagnósticos para determinar cuáles son las áreas prioritarias para la conservación.	88	15
417	Impulsar la recuperación, restauración y reforestación de los ecosistemas que han sufrido algún cambio.	88	15
418	Reforzar los consejos ciudadanos para implementar acciones contra el cambio climático.	89	13
419	Promover la realización de estudios e investigaciones sobre posibles efectos derivados del cambio climático.	89	13
420	Promover entre la población la adopción de medidas de adaptación y mitigación ante el cambio climático.	89	13
421	Incorporar a los planes de estudio de todos los niveles educativos la enseñanza de medidas para la prevención, adaptación y mitigación de los efectos del cambio climático. 	89	13
422	Reforzar el fondo estatal de apoyo contra desastres naturales.	90	13
423	Plantear acciones para la concientización de la población que habita en zonas de riesgo de fenómenos meteorológicos.	90	13
424	Fomentar una cultura de prevención y respuesta eficaz ante desastres naturales en coordinación con los municipios del estado. 	90	13
425	Incorporar instrumentos para la gestión de riesgos que permitan accionar ante las posibles consecuencias de los fenómenos naturales adversos. 	90	13
426	Promover la restauración de las barreras naturales para disminuir los impactos de eventos meteorológicos extremos en la zona costera.	90	13
427	Poner en marcha mecanismos de mitigación con el sector agropecuario, industrial, comercial y de servicios, a fin de reducir sus emisiones de carbono.	91	13
428	Promover el uso de ecotecnias para reducir la huella ecológica y mejorar el aprovechamiento sustentable de los recursos naturales.	91	13
429	Generar incentivos económicos para la aplicación de modelos agropecuarios ecológicos y sostenibles. 	91	13
430	Promover prácticas que propicien la reducción de los gases de efecto invernadero.	91	13
431	Impulsar desde las compras y el consumo de la administración pública estatal, una economía baja en carbono.	91	13
432	Identificar y monitorear las fuentes de emisiones contaminantes.	92	13
433	Actualizar el  marco regulatorio vigente en materia de prevención y control de la contaminación atmosférica.	92	13
434	Sensibilizar a la ciudadanía sobre la calidad del aire, los efectos de la contaminación en la salud y en los ecosistemas, así como los riesgos por exposición.	92	13
435	Desarrollar un sistema integral de verificación de la calidad del aire que incluya el sistema de transporte y el sector industrial.	92	13
436	Promover la investigación y la innovación tecnológica como base de las políticas para mejorar la calidad del aire.	92	13
437	Establecer mecanismos para el monitoreo de la calidad del aire prioritariamente en los municipios más afectados por la contaminación. 	92	13
438	Promover el fortalecimiento y actualización de la legislación en materia hídrica en el estado.	93	6
439	Diseñar programas para la reutilización de las aguas residuales tratadas para uso general y en el sector industrial.	93	6
440	Identificar fuentes de financiamiento para fortalecer los sistemas de saneamiento y tratamiento de agua con visión a largo plazo.	94	6
441	Ampliar la infraestructura existente para el suministro de agua potable en el estado.	94	6
442	Establecer mecanismos de cooperación entre el gobierno estatal y la iniciativa privada, para desarrollar proyectos de materia de tratamiento de aguas residuales.	94	6
443	Establecer programas, procesos de mantenimiento y mejoramiento de las plantas de tratamiento de aguas residuales existentes.	94	6
444	Elaborar un plan de innovación tecnológica para el fomento al tratamiento de las descargas de aguas residuales.	95	6
445	Promover la regulación de las plantas de tratamiento residuales, para que incorporen las tecnologías que resulten más convenientes para la población.	95	6
446	Invertir en nuevas tecnologías que mejoren la calidad del agua tratada. 	95	6
447	Vincular a las universidades y centros de investigación para el desarrollo tecnológico en el mejoramiento del tratamiento de agua.	95	6
448	Desarrollar esquemas de economía circular para el cuidado del agua	95	6
449	Promover planes y normas que regulen el uso eficiente del agua.	96	6
450	Poner en marcha acciones para hacer más eficiente y mejorar la calidad en el servicio de potabilización del agua.	96	6
451	Desarrollar procesos de fortalecimiento y capacitación de los municipios para la gestión sustentable del agua.	96	6
452	Incrementar la captación y aprovechamiento del agua pluvial.	96	6
453	Promover en el sector empresarial, especialmente en la industria hotelera y de servicios turísticos, el uso eficiente del agua, reducción de emisiones contaminantes y reciclaje.	96	6
454	Promover la concientización de la población sobre el uso responsable y eficiente del agua así como el pago oportuno del servicio.	96	6
455	Promover prácticas sustentables en las actividades agrícolas eficientes en el uso del agua.	96	6
456	Estandarizar indicadores de medición de la calidad del agua en el manto acuífero. 	97	6
457	Implementar esquemas de vigilancia ciudadana del agua.	97	6
458	Promover la creación de un sistema de monitoreo e inspección de la calidad del agua a nivel estatal. 	97	6
459	Promover una cultura de sustentabilidad en torno al manejo integral de residuos desde la educación formal y no formal.	98	12
460	Organizar la gestión de los residuos sólidos y especiales de acuerdo con una lógica regional en los municipios, a fin de sumar las capacidades institucionales y hacer uso eficiente de los recursos.	98	12
461	Formular acciones que aumenten el valor de los residuos recolectados dentro de economías de escala.	98	12
462	Capacitar al sector empresarial, gubernamental y a la sociedad para la elaboración adecuada de sus planes de manejo de residuos.	98	12
463	Desarrollar esquemas de saneamiento y recuperación de sitios afectados por el inadecuado manejo de los residuos.	98	12
464	Estimular la participación de la población para elaborar iniciativas ciudadanas para el manejo integral de los residuos.	98	12
465	Promover la inversión privada en el manejo integral de los residuos sólidos y especiales.	98	12
466	Implementar mecanismos de monitoreo y evaluación del cumplimiento de los reglamentos y normas ambientales.	98	12
467	Regular el uso del plástico en el estado.	99	12
468	Promover en las empresas que fabrican bolsas y productos de plástico la adopción de tecnologías biodegradables	99	12
469	Incentivar a las empresas para que adopten una cultura de reducción de residuos.	99	12
470	Incentivar el uso de los empaques ambientalmente responsables en los establecimientos comerciales.	99	12
471	Reforzar y mejorar los esquemas de organización y comunicación dirigida a la reducción y aprovechamiento de los residuos.	99	12
472	Gestionar la infraestructura para aumentar el aprovechamiento y valorización de los residuos.	100	12
473	Diseñar campañas para la separación de residuos sólidos y la reutilización de materiales de desecho.	100	12
474	Fortalecer el marco jurídico, para establecer trabajo comunitario de recolección de residuos sólidos y limpieza de calles como sanción a quienes arrojan residuos sólidos en espacios públicos.	100	12
475	Incentivar y regular a la industria para el manejo sustentable de los residuos sólidos y especiales.	100	12
476	Promover el reciclaje inclusivo en los municipios.	100	12
477	Promover la innovación e investigación para eficientar la valorización de los residuos.	100	12
478	Promover proyectos en el sector empresarial que contribuyan al desarrollo tecnológico de energías limpias.	101	7
479	Vincular los diferentes sectores para la implementación conjunta de proyectos de eficiencia energética y energías limpias.	101	7
480	Promover la investigación y capacitación en torno a las energías renovables.	101	7
481	Promover proyectos de energías renovables en los ámbitos industrial y residencial.	102	7
482	Promover el desarrollo de inventarios de energías limpias en el estado. 	102	7
483	Favorecer la implementación de energías limpias en el gobierno estatal.	102	7
484	Promover la generación y gestión de energía distribuida y autónoma a través de fuentes renovables.	102	7
485	Promover con las autoridades competentes, la inclusión en la evaluación y autorización de proyectos de generación de energías renovables. 	102	7
486	Promover el uso de dispositivos compatibles con las energías limpias a la población.	103	7
487	Promover la operación de proyectos  de inversión privada de energías no contaminantes.	103	7
488	Establecer incentivos para que los sectores social y privado hagan uso de energías limpias. 	103	7
489	Establecer mecanismos de cooperación que ofrezcan alternativas viables con relación a los costos de insumos energéticos.	104	7
490	Promover proyectos científicos y tecnológicos, dirigidos a reducir la demanda energética e incrementar el uso de energías renovables.	104	7
491	Poner en marcha proyectos e inversiones dirigidas a un aprovechamiento sustentable de la energía en el estado.	104	7
492	Promover la acuacultura como medio para evitar la sobreexplotación de las especies marinas.	105	14
493	Reforzar la vigilancia del cumplimiento de los periodos de veda de las especies marinas.	105	14
494	Implementar programas de manejo y conservación de especies marinas en el estado.	105	14
495	Reforzar la regulación y vigilancia sobre la pesca recreativa y deportiva. 	105	14
496	Establecer programas de conservación y promover el rescate y cuidado de los manglares y playas de las zonas costeras.	106	14
497	Difundir campañas de limpieza de playas que fomenten la participación ciudadana.	106	14
498	Establecer programas de vigilancia permanente de las playas para evitar su contaminación. 	106	14
500	Implementar alternativas para la pesca, a fin de reducir la presión sobre especies submarinas en peligro o en riesgo. 	106	14
501	Promover programas y proyectos que favorezcan la restauración y conservación de la zona costera.	106	14
502	Reforzar el manejo adecuado de las zonas costeras protegidas.	106	14
503	Impulsar el desarrollo de infraestructura específica para optimizar el Sistema de Transporte Urbano.	107	11
504	Elaborar programas de capacitación para los prestadores de servicio de transporte público.	107	11
505	Establecer lineamientos que permitan castigar el acoso femenil dentro del sistema de transporte público.	108	11
506	Gestionar el incremento de las unidades de transporte público adecuadas para las personas con discapacidad.	108	11
507	Implementar acciones que prioricen al peatón y modifiquen la infraestructura de calles y banquetas, dando especial atención a personas con discapacidad.	109	11
508	Elaborar programas de ampliación de la infraestructura urbana para movilidad no motorizada.	109	11
509	Elaborar programas que promuevan el uso de medios de transporte  sustentables como alternativa a los medios de transporte motorizados. 	109	11
510	Formular programas de desarrollo urbano que contribuyan y fortalezcan la movilidad integral en todos los municipios del estado.	109	11
511	Promover esquemas de capacitación y cultura vial para operadores y usuarios del transporte público.	110	11
512	Adecuar las vialidades en zonas de alto riesgo para los peatones con enfoque incluyente.	110	11
513	Promover la realización de estudios y proyectos que contribuyan a mejorar la cultura vial y la reducción de accidentes.	110	11
514	Crear programas e instrumentos para mejorar la accesibilidad universal a corto, mediano y largo plazo.	111	11
515	Elaborar un plan de movilidad urbana sustentable bajo en carbono.	111	11
516	Reforzar el transporte interurbano de calidad y con accesibilidad universal.	111	11
517	Motivar el desarrollo de propuestas ciudadanas de movilidad asequible, segura y no contaminante, en los instrumentos de planeación.	111	11
521	Incorporar la conectividad urbana y la provisión de redes de transporte público como exigencia para nuevas urbanizaciones.	111	11
522	Revisar y actualizar los instrumentos normativos existentes en materia de movilidad urbana sustentable.	111	11
523	Facilitar el acceso a los métodos anticonceptivos de la población para el ejercicio de sus derechos sexuales y reproductivos.	112	5
524	Propiciar las condiciones para la atención integral, especializada y con perspectiva de género de salud de las mujeres, con énfasis en mujeres con alguna discapacidad, en situación de pobreza extrema o marginación.	112	5
525	Fortalecer los esquemas de atención con enfoque intercultural en el sector público para la detección y atención oportuna de las enfermedades que más afectan a las mujeres.	112	5
526	Facilitar el acceso a los servicios médicos y psicológicos a niñas, mujeres adultas y adultas mayores con discapacidad, en situación de pobreza extrema o marginación.	113	5
527	Impulsar campañas de prevención del embarazo adolescente, con un enfoque inclusivo y con atención especial en zonas marginadas y en el interior del estado.	113	5
528	Facilitar el acceso a servicios médicos y psicológicos a niñas y adolescentes embarazadas para garantizar su salud.	113	5
529	Sensibilizar sobre el paradigma de los nuevos modelos de masculinidad en la prevención del embarazo adolescente.	113	5
530	Impulsar convenios de colaboración y fomentar la coordinación con los sectores público, privado, social y académico para fortalecer la educación inclusiva, con atención especial  en comunidades alejadas.	114	5
531	Promover esquemas especiales de alfabetización para mujeres adolescentes y adultas.	114	5
532	Impulsar acciones afirmativas que permitan a las mujeres embarazadas o madres solteras continuar con su formación educativa.	114	5
533	Promover el enfoque de género en los procesos de diseño de programas educativos de todos los niveles de atención.	114	5
534	Impulsar acciones interinstitucionales en el ámbito educativo para la prevención del embarazo adolescente.  	114	5
535	Facilitar apoyos e incentivos para fortalecer la permanencia escolar de las niñas y mujeres, con énfasis en zonas de alto riesgo de deserción educativa.	114	5
536	Promover la inclusión de mujeres estudiantes, docentes, personal administrativo y con discapacidad en el sector educativo, en especial en profesiones altamente masculinizadas.	115	5
537	Establecer escenarios propicios para la integración de las mujeres indígenas y mayahablantes a la educación. 	115	5
538	Fomentar acciones para eliminar estereotipos de género en la educación.	115	5
539	Implementar programas de capacitación que permitan a las mujeres fortalecer sus habilidades en el uso de tecnologías.	115	5
540	Crear una estrategia coordinada para combatir la pobreza y fomentar la inserción laboral de las mujeres.	116	5
541	Impulsar una bolsa de trabajo inclusiva para mujeres con discapacidad y adultas mayores.	116	5
542	Impulsar acciones en beneficio de mujeres con bajo nivel de escolaridad, madres solteras, mujeres adultas mayores o con discapacidad en beneficio de su autonomía financiera.	116	5
543	Implementar acciones que favorezcan las condiciones de competitividad para las mujeres emprendedoras y generadoras de empleo.	116	5
544	Promover redes comunitarias de mujeres productoras y comerciantes que fortalezcan el desarrollo económico.	116	5
545	Impulsar alianzas, entre el sector público y privado, que premien la responsabilidad social empresarial y permitan a las mujeres conciliar su vida familiar y laboral.	116	5
546	Promover un salario equitativo entre mujeres y hombres por trabajo de igual valor.	116	5
547	Promover apoyos para mujeres que presten cuidados no remunerados a personas dependientes conocidos como “cuidados prolongados”.	116	5
548	Promover redes que impulsen de manera focalizada el emprendimiento de las mujeres a través del acompañamiento, servicios eficientes y comercialización de sus productos.	116	5
549	Incrementar la participación política de las mujeres, especialmente para los puestos públicos de toma de decisiones e impartición de justicia.	117	5
550	Promover la corresponsabilidad de los hogares para facilitar la participación de las mujeres en la toma de decisiones familiares, educativas y económicas.	117	5
551	Promover la transversalización de la perspectiva de género en todos los ciclos de las políticas públicas.	117	5
552	Fortalecer las instituciones enfocadas a transversalizar  la perspectiva de género en la entidad.	117	5
553	Fomentar el empoderamiento de las mujeres para aumentar la resiliencia y adaptación al cambio climático.	117	5
554	Impulsar  el acceso de las mujeres a la información y participación en decisiones ambientales  y manejo sustentable de los recursos.	117	5
555	Capacitar a las y los titulares de las dependencias y entidades para la institucionalización e incorporación de la perspectiva de género.	118	5
556	Promover la incorporación de buenas prácticas para la igualdad laboral y no discriminación con el objetivo de que los sectores públicos y privados obtengan certificaciones en la materia.	118	5
557	Fortalecer a las instancias de las mujeres en los municipios para prevenir la violencia de género hacia las mujeres y la desigualdad entre mujeres y hombres.	118	5
558	Impulsar mecanismos para la prevención, atención y denuncia de la violencia sexual en instituciones educativas e instancias públicas y privadas que trabajen con niñas, niños y adolescentes.	118	5
559	Fortalecer las capacidades en el sistema educativo estatal para la atención de conductas de riesgo y prevención de violencia de género.	118	5
560	Impulsar que las empresas apliquen medidas para prevenir y erradicar el acoso laboral, la discriminación por razones de género y cualquier otra práctica que vulnere los derechos de las mujeres.	118	5
561	Promover la cultura de la denuncia a través de medios accesibles con especial atención en  personas con discapacidad y mayahablantes.	119	5
562	Fortalecer la participación ciudadana, así como de los medios de comunicación, para rechazar la normalización de la violencia en contra de las mujeres.	119	5
563	Fomentar campañas permanentes en contra del acoso y violencia contra las mujeres  en espacios públicos.	119	5
564	Fortalecer la protección de los derechos de las niñas y mujeres adolescentes en especial el derecho a una vida libre de violencia.	119	5
565	Promover el uso de las Tecnologías de la Información y la Comunicación para acercar los servicios de emergencia ante situaciones de violencia.	119	5
566	Generar campañas dirigidas a hombres que promuevan las masculinidades no violentas y el involucramiento activo en la prevención de la violencia contra las mujeres.	119	5
567	Promover la capacitación de los elementos de seguridad pública para que sus intervenciones en situaciones de violencia hacia las mujeres sean efectivas, apegadas a la ley y oportunas.	120	5
568	Facilitar la accesibilidad segura de las mujeres a los centros municipales de atención a la violencia contra las mujeres, en especial en las comunidades mayas.	120	5
569	Impulsar la capacitación del personal médico en la aplicación de las normas mexicanas dirigidas a brindar servicios de calidad y prevenir prácticas de discriminación o actos de violencia contra las mujeres.	120	5
570	Fortalecer los órganos estatales encargados de promover los derechos humanos de las mujeres y establecer acciones para la igualdad de género.	120	5
571	Impulsar los servicios itinerantes de primer contacto ante situaciones de violencia, especialmente en comunidades alejadas.	120	5
572	Promover acciones multidisciplinarias para la atención integral de las causas y efectos de la violencia de género	120	5
573	Impulsar la capacitación de los jueces de paz de los municipios en perspectiva de género, con énfasis en la atención de casos de violencia hacia las mujeres y violencia intrafamiliar.	121	5
574	Fortalecer la legislación estatal para que contribuya a la igualdad de oportunidades y al acceso de las mujeres a una vida libre de violencia y armonizarla con las normas generales y tratados internacionales.	121	5
575	Fortalecer el trabajo operativo a través de protocolos que eviten la revictimización de las mujeres en el sistema de justicia.	121	5
576	Fortalecer los servicios de defensoría de oficio con personal especializado en género y derechos humanos.	121	5
577	Impulsar redes interinstitucionales que generen datos con desagregación estadística por sexo y edad, para la toma de decisiones públicas en favor de las mujeres, en especial en el tema de prevención, atención, sanción y combate de violencia y defensa de los derechos.	122	5
578	Impulsar mecanismos de vinculación interinstitucional que fortalezca el banco de datos estatal sobre violencia para que sea alimentado por las instancias competentes.	122	5
579	Evaluar y certificar los registros estadísticos e informáticos que concentran datos sobre la violencia contra las mujeres. 	122	5
580	Estimular las habilidades y conocimientos técnicos y operativos para el trabajo de las y los jóvenes en situación de vulnerabilidad	123	10
581	Promover acciones de autoempleo y financiamiento que proyecten el desarrollo empresarial de la población en situación de vulnerabilidad.	123	10
582	Promover acuerdos y convenios en el sector público y grupos empresariales para la incorporación al empleo de personas en situación de vulnerabilidad, en especial de personas con discapacidad y adultas mayores.	123	10
583	Asesorar a la población en situación de vulnerabilidad sobre los derechos de los trabajadores para garantizar su protección laboral.	123	10
584	Fomentar acciones especiales para que la población con alguna discapacidad tenga acceso a empleo de calidad.	123	10
585	Instrumentar medidas que combatan el trabajo infantil y permitan el disfrute a los niños de los derechos de la niñez	123	10
586	Fortalecer las áreas gubernamentales encargadas de garantizar los derechos de las personas con alguna discapacidad o en situación de vulnerabilidad	123	10
587	Facilitar el acceso y la permanencia de personas con alguna discapacidad a una educación integral y de calidad.	124	10
588	Facilitar espacios culturales y deportivos incluyentes para el esparcimiento e interacción de los grupos en situación de vulnerabilidad.	124	10
589	Incentivar a organizaciones que elaboren proyectos o acciones de desarrollo comunitario, de combate a las desigualdades sociales y acceso incluyente.	124	10
590	Promover la participación de la sociedad civil en la implementación de acciones innovadoras que destacan la inclusión social al desarrollo comunitario.	124	10
591	Promover un sistema de asistencia y apoyo para personas en situación de dependencia, con énfasis en las que presenten carencia alimentaria.	124	10
592	Impulsar el desarrollo comunitario que permita la inclusión de los grupos en situación de  vulnerabilidad en el bienestar social	124	10
593	Fomentar programas de apoyo para personas en situación de vulnerabilidad que faciliten el acceso a la vivienda y servicios básicos.	124	10
594	Facilitar el acceso a la seguridad social y servicios de salud a personas con alguna discapacidad o en situación de vulnerabilidad.	124	10
595	Fortalecer los espacios públicos para que sean accesibles y que cumplan con los estándares de calidad e inclusión para las personas con alguna discapacidad.	125	10
596	Impulsar acciones de equipamiento en las unidades básicas de rehabilitación integral para personas con discapacidad. 	125	10
597	Fortalecer la infraestructura, equipamiento y tecnología asistiva para la población en situación de vulnerabilidad, con énfasis en las personas con alguna discapacidad.	125	10
598	Impulsar el uso de las Tecnologías de la Información y Comunicación para la asistencia a la población en situación de vulnerabilidad y/o dependencia.	125	10
599	Capacitar al personal de las instituciones públicas y privadas en temas de sensibilización y acompañamiento a la inclusión social de la población en situación de vulnerabilidad con énfasis en las personas con alguna discapacidad y de comunidades indígenas.	125	10
600	Reforzar el marco normativo vigente para aplicar sanciones más estrictas en casos de discriminación y violencia contra la mujer, así como a grupos en situación de vulnerabilidad	125	10
601	Fortalecer las capacidades del personal de las instituciones encargadas de la atención de la violencia hacia las mujeres, niñas, niños, adolescentes y otras en situación de vulnerabilida	125	10
602	Impulsar mecanismos educativos de concientización, que propicien la igualdad y la no discriminación hacia los grupos en situación de vulnerabilidad. 	126	10
603	Promover una cultura de tolerancia y no discriminación en todos los niveles educativos.	126	10
604	Promover la educación cívica y el respeto a la Ley en todas las esferas institucionales, con énfasis en la familia y escuelas.	126	10
605	Implementar campañas de difusión de los derechos civiles, políticos, económicos, sociales, culturales y ambientales.	126	10
606	Diseñar campañas de fomento para la inclusión social en la participación política del estado y el ejercicio pleno de los derechos.	126	10
651	Gestionar la transferencia de conocimientos y tecnología que permitan atender con mayor efectividad los problemas sociales.	135	9
607	Coordinar una vinculación efectiva entre los niveles de educación media superior y la educación superior para mejorar la absorción y la pertinencia de los programas educativos.	127	9
608	Impulsar mecanismos de difusión en línea que faciliten el conocimiento de la oferta educativa existente en el nivel superior.	127	9
609	Proveer de Tecnologías de la Información y la Comunicación a las instituciones de educación superior para fomentar la innovación de los métodos educativos.	127	9
610	Generar las condiciones adecuadas para que más programas educativos del nivel superior puedan ser impartidos en línea. 	127	9
611	Procurar la inversión en infraestructura física y equipamiento acorde a los requerimientos actuales de cada institución de educación superior.	127	9
612	Promocionar a las universidades como centros de capacitación certificados para la profesionalización basada en competencias laborales de calidad.	128	9
613	Impulsar programas de formación continua para personal de las empresas, con especial énfasis en las capacidades científicas y técnicas.	128	9
614	Generar alianzas entre las universidades, centros de investigación, instituciones tecnológicas e iniciativa privada para la formación de capital humano de calidad acorde a la demanda del mercado laboral.	128	9
615	Desarrollar una oferta de educación superior adaptada a la demanda de la industria 4.0 .	128	9
616	Impulsar bolsas de trabajo y seguimiento de egresados para la comunidad de estudiantes de nivel superior, a través de convenios de colaboración entre el sector privado y las instituciones de educación superior. 	128	9
617	Promover mecanismos de asesoría especializada del sector educativo hacia el sector privado, con especial énfasis en temas científicos y técnicos.	128	9
618	Vincular la participación de las universidades en la solución de problemas locales a través de la investigación aplicada.	128	9
619	Ampliar la cobertura de programas de fomento científico que beneficie a un mayor número de niñas, niños y jóvenes.	129	9
620	Implementar mecanismos de divulgación de la ciencia a la población en general que generen interés por la misma.	129	9
621	Diseñar programas de ciencias, tecnología, ingeniería y matemáticas para desarrollar las destrezas o habilidades de los estudiantes en estas disciplinas.	129	9
622	Diseñar planes de estudio adecuados a las vocaciones regionales actuales y emergentes.	130	9
623	Fortalecer los planes de estudio existentes para su actualización de acuerdo a las necesidades de los sectores económico, social y ambiental, y con enfoque de sostenibilidad.	130	9
624	Diseñar mecanismos que permitan flexibilizar los procesos de terminación y titulación en los programas educativos.	130	9
625	Establecer mecanismos para una adecuada formación, profesionalización y actualización docente en las Instituciones de educación superior a través de programas de ciencias, tecnología, ingeniería y matemáticas.	130	9
626	Fomentar la acreditación de programas y procesos eeducativos del nivel superior con base en estándares de calidad y pertinencia.	130	9
627	Promover canales de colaboración entre las instituciones de educación superior del estado con las instituciones del orden nacional e internacional.	131	9
628	Fortalecer los programas de idiomas y de vinculación al extranjero.	131	9
629	Facilitar la modernización y adaptación de las Instituciones de educación superior a las nuevas tecnologías y necesidades educativas.	131	9
630	Establecer esquemas adecuados para orientar los programas de posgrado en el estado hacia el cumplimiento de estándares de calidad reconocidos por organismos nacionales e internacionales.	131	9
631	Diseñar esquemas para fortalecer los programas de licenciatura a fin de contar con egresados del nivel con mejor calidad para su incorporación a estudios de posgrado.	131	9
632	Diseñar esquemas para fortalecer la formación de recursos humanos en el extranjero, que contribuyan a elevar la calidad de la planta docente de estudios de posgrado en el estado.	131	9
633	Diseñar esquemas para fortalecer los programas de tutorías existentes en las instituciones de educación superior a fin de garantizar su efectividad.	132	9
634	Promover en conjunto con el sector empresarial, mecanismos que permitan a los estudiantes, insertarse en la industria desde su formación en las instituciones de educación superior.	132	9
635	Fortalecer los programas de apoyos que contribuyan a evitar la deserción en la educación superior. 	132	9
636	Fortalecer de manera sostenible la normatividad de la gestión institucional para asegurar el funcionamiento pertinente de las instituciones de educación superior.	132	9
637	Favorecer la incorporación de organismos independientes en la evaluación de los programas educativos.	132	9
638	Diseñar mecanismos de capacitación y evaluación integral del desempeño docente que asegure la mejora continua.	132	9
639	Consolidar el Sistema de Investigación, Innovación y Desarrollo Tecnológico del Estado de Yucatán (SIIDETEY).	133	9
640	Incentivar las actividades de investigación y desarrollo en sectores estratégicos como agrobiotecnología, energías sustentables, salud, manejo de los recursos naturales entre otros.	133	9
641	Promover la formación de recursos humanos altamente calificados en el campo de la investigación y desarrollo tecnológico.	133	9
642	Establecer esquemas de apoyo financiero a proyectos de investigación científica y tecnológica.	133	9
643	Impulsar convenios de colaboración entre centros de investigación, instituciones de educación superior y empresas privadas para el desarrollo de tecnología.	134	9
644	Diseñar mecanismos de movilidad de recursos humanos hacia las instituciones de educación superior internacionales.	134	9
645	Estimular la participación del sector privado en las estrategias de movilidad internacional que se impulsen desde el estado, con énfasis en ciencia, tecnología , artes y humanidades.	134	9
646	Diseñar un sistema de estancias estudiantiles en empresas para la colaboración en proyectos de investigación y desarrollo.	134	9
647	Fortalecer los procesos de investigación y desarrollo tecnológico en los campos de artes y humanidades.	134	9
648	Sensibilizar a la población sobre la importancia del aprovechamiento científico y tecnológico en las artes y humanidades.	134	9
649	Impulsar el desarrollo de un clúster de innovación y desarrollo de Tecnologías de la Información y la Comunicación en el estado.	134	9
650	Gestionar fondos para la colaboración pública y privada en la implementación de proyectos científicos y tecnológicos.	135	9
652	Promover los avances y beneficios de la ciencia y tecnología en los municipios y en todos los niveles educativos.	135	9
653	Promover la formación de recursos humanos altamente calificados en el campo de la investigación, desarrollo científico, artes y humanidades.	135	9
654	Impulsar la investigación enfocada al sector salud para generar tecnología y biotecnología de vanguardia.	135	9
655	Proponer espacios para el intercambio entre ciencia, tecnología, sociedad y cultura.	136	9
656	Implementar esquemas de financiamiento para el fortalecimiento de infraestructura tecnológica.	136	9
657	Generar aciones que faciliten la atracción y retención de talento en sectores de alta complejidad económica.	136	9
658	Establecer mecanismos para incrementar el número de empresas e instituciones científicas y tecnológicas en el estado.	136	9
659	Desarrollar instrumentos de transferencia y difusión tecnológica en sectores estratégicos para el estado.	136	9
660	Incentivar a las empresas que generen investigación y desarrollo tecnológico en sus procesos de producción de manera sustentable y sostenible.	137	9
661	Otorgar facilidades a los investigadores e Instituciones de educación superior para gestionar y registrar sus invenciones.	137	9
662	Impulsar esquemas de sensibilización y difusión sobre la propiedad intelectual e industrial.	137	9
663	Generar acciones para la vinculación efectiva entre los centros de investigación, instituciones de educación superior y la industria, en torno a la generación de propiedad intelectual e industrial.	137	9
664	Estimular la generación y el aprovechamiento de invenciones o procesos novedosos en el sector público.	137	9
665	Consolidar el sistema de incubadoras en el estado mediante acciones de equipamiento e intercambio académico, científico y tecnológico.	138	9
666	Fortalecer el Parque Científico y Tecnológico del estado mediante esquemas efectivos de colaboración e impulso académico.	138	9
667	Estimular la inversión pública y privada en acciones de innovación, investigación científica y transferencia tecnológica.	138	9
668	Reforzar la consolidación de los ecosistemas de innovación, así como también, la infraestructura científica en zonas estratégicas del estado	138	9
669	Fortalecer las campañas de prevención del delito de forma inclusiva y sostenible con énfasis en zonas de mayor incidencia delictiva. 	139	16
670	Instrumentar acciones de manera coordinada con todos los órdenes de gobierno en materia de prevención del delito.	139	16
671	Diseñar sistemas de información estadística y geográfica para el mejoramiento de toma de decisiones en la prevención del delito.	139	16
672	Establecer operativos especiales para el combate a los delitos de robo a casa habitación, fraude, extorsión y robo parcial de vehículo.	139	16
673	Realizar acciones para la detección de portación de armas de fuego sin autorización y arma blanca.	139	16
674	Diseñar un programa de prevención social del delito que contemple las causas y particularidades de cada región, municipio y localidad.	139	16
675	Incentivar la participación ciudadana y de organizaciones para la prevención social del delito  que genere comunidades y ciudades resilientes.	139	16
676	Estimular la proximidad social entre las instituciones de seguridad y la ciudadanía para la detección oportuna de riesgos para su atención prioritaria.	139	16
677	Diseñar mecanismos de seguimiento a los requerimientos en materia de prevención social del delito derivados de la participación ciudadana.	139	16
678	Realizar actividades que fomenten la armonía comunitaria y la cohesión social en beneficio de la seguridad a nivel localidad.	139	16
679	Impulsar acciones específicas que promuevan la cultura de la paz, eviten la violencia en todos los niveles educativos.	139	16
680	Establecer programas integrales interinstitucionales de combate de adicciones con enfoque intercultural y de prevención del delito.	139	16
681	Capacitar a la población para la prevención, detección y denuncia de los delitos cibernéticos.	139	16
682	Reforzar las capacidades de los jueces de paz en el interior del estado para la aplicación de la mediación en conflictos de convivencia comunitaria.	140	16
683	Promover la aplicación de esquemas de mediación en el ámbito escolar para la resolución de conflictos.	140	16
684	Implementar modelos de mediación para la resolución de conflictos bajo el principio de igualdad de derechos.	140	16
685	Impulsar la instrumentación del servicio profesional de carrera policial.	141	16
686	Reforzar la capacitación inicial y continua de los policías estatales y municipales, así como su especialización.	141	16
687	Capacitar a mandos medios y altos dentro de nuevos modelos estratégicos para la investigación policial y el combate a la delincuencia.	141	16
688	Promover los programas de renivelación académica de los elementos policiales.	141	16
689	Establecer un programa de mejoramiento de las condiciones sociales, económicas y de vida de los policías del estado.	141	16
690	Impulsar campañas de seguridad vial y respeto a las normas de tránsito y vialidad.	142	16
691	Reforzar los operativos de vigilancia vial y de alcoholimetría para prevenir accidentes de tránsito.	142	16
692	Impulsar acciones para la modernización de la infraestructura de tránsito en materia de señalización y semaforización.	142	16
693	Promover la realización de pláticas de educación vial en las escuelas y entre las organizaciones civiles.	142	16
694	Reforzar la infraestructura, equipamiento y tecnología de las instituciones de seguridad, en beneficio de la población y visitantes del estado.	143	16
695	Reforzar las acciones de vigilancia en la entrada y salida del estado con aprovechamiento tecnológico.	143	16
696	Fortalecer las capacidades del sistema de videovigilancia en toda la entidad y sus carreteras con énfasis especial en zonas de mayor presencia delictiva.	143	16
697	Reforzar el estado de fuerza policial, así como sus capacidades  técnicas y habilidades con enfoque basado en derechos humanos para la atención de situaciones de auxilio a la población, especialmente en localidades y municipios de mayor incidencia delictiva.	143	16
698	Establecer nuevos esquemas para la prevención y combate de robos a casa  habitación y comercios.	143	16
699	Diseñar mecanismos eficientes de vigilancia policiaca en zonas de alto riesgo delictivo con especial énfasis en la protección de las mujeres y población en situación de vulnerabilidad.	143	16
749	Desarrollar una política de datos abiertos que oriente la generación, sistematización y difusión de información.	156	17
700	Implementar acciones para el uso de nuevas Tecnologías de la Información y la Comunicación en materia de prevención y seguridad pública.	143	16
701	Establecer esquemas para la reinserción social efectiva de personas que han cometido algún tipo de delito.	143	16
702	Modernizar los centros de reinserción social del estado para garantizar el respeto de los  derechos humanos de las personas en detención.	143	16
703	Fortalecer los mecanismos de registro vehicular para contar con un padrón que ayude a la recuperación e identificación de vehículos involucrados en conflictos con la Ley.	144	16
704	Impulsar mecanismos para vigilar el cumplimiento de la Ley por parte de los elementos de los cuerpos de seguridad pública.	144	16
705	Impulsar acciones de infraestructura y equipamiento en materia de procuración de justicia.	145	16
706	Fortalecer los recursos humanos y capacidades técnicas de las instituciones de justicia, especialmente las relacionadas con las tareas de investigación y el ministerio público.	145	16
707	Fortalecer los mecanismos alternativos de solución de controversias a través de personal calificado y con perspectiva de género.	145	16
708	Consolidar el Sistema Acusatorio  Penal y el Sistema Oral de Justicia para agilizar su ejercicio en todas las áreas de derecho en especial en materia mercantil. 	145	16
709	Diseñar mecanismos administrativos para el control de reincidencias en materia de comisión de delitos.	145	16
710	Impulsar la simplificación de los procesos de denuncia para disminuir tiempos.	146	16
711	Mejorar el proceso de denuncia del delito para que mantenga parámetros de calidad en servicio y atención.	146	16
712	Impulsar el desarrollo del sistema de acceso a la información de estatus de personas en conflicto con la ley en los ministerios públicos.	146	16
713	Aplicar tecnologías de información para agilizar los procesos de denuncia.	146	16
714	Reforzar las capacidades de la defensoría legal en la entidad.	146	16
715	Ampliar los servicios de asesoría gratuita en otras materias como la civil, en especial para la población en situación de vulnerabilidad.	146	16
716	Facilitar la atención oportuna en los procesos de denuncia a mujeres víctimas de violencia doméstica o de género.	146	16
717	Adecuar los espacios físicos para la prestación de servicios de defensoría pública.	147	16
718	Instrumentar esquemas de capacitación en materia de procuración de justicia y defensoría legal con énfasis a grupos en situación de vulnerabilidad.	147	16
719	Promover campañas dirigidas a la cultura de respeto y difusión de derechos humanos.	147	16
720	Promover campañas de respeto a los valores humanos y principios de convivencia familiar.	147	16
721	Gestionar acciones de capacitación en lengua maya en el ámbito del Sistema de Justicia Penal.	147	16
722	Implementar acciones para profesionalización de peritos del Sistema de Justicia Penal.	147	16
723	Actualizar las leyes en materia de procuración de justicia para hacer eficiente su aplicación y desanimar la reincidencia delictiva.	148	16
724	Implementar acciones para el fortalecimiento de los juzgados en materia de procuración de justicia.	148	16
725	Mejorar los mecanismos legales para la calificación e identificación de delitos del fuero común.	148	16
726	Diseñar acciones para la reducción de la impunidad en casos de violencia familiar y de género.	148	16
727	Promover la presencia de policías ministeriales o investigadores en los municipios, en especial los de mayor incidencia delictiva.	149	16
728	Facilitar el acceso a los servicios de la Fiscalía y ministerios públicos a los ciudadanos del interior del estado.	149	16
729	Fortalecer la atención a víctimas del delito y la reparación del daño a las mismas.	149	16
730	Impulsar normas y  mecanismos que agilicen las notificaciones personales, garanticen la adecuada ejecución de las sentencias y reduzcan los tiempos y costos en la tramitación de juicios.	149	16
731	Establecer mecanismos claros para la rendición de cuentas y transparencia en los procesos de seguridad y justicia en beneficio de los derechos humanos.	150	16
732	Implementar acciones para difusión de datos abiertos a la ciudadanía en materia de seguridad y justicia con desagregación por género.	150	16
733	Promover campañas de difusión de la acciones implementadas  de la agenda gubernamental en materia de seguridad, justicia y gobernabilidad, así como de los resultados obtenidos.	150	16
734	Establecer acciones que fomenten la certeza jurídica de los habitantes en especial los relacionados al derecho a la identidad, el patrimonio y aquellos que condicionen acceso a otros derechos humanos.	151	16
735	Reforzar el derecho de propiedad de las mujeres.	151	16
736	Impulsar la modernización y actualización de los servicios relacionados al derecho a la identidad y el patrimonio de los habitantes.	151	16
737	Gestionar convenios con los municipios para el uso de recursos en coparticipación para  acciones que mejoren la calidad de vida de los habitantes .	152	16
738	Coadyuvar con los municipios en la implementación del modelo de Presupuesto basado en Resultados y del Sistema de Evaluación del Desempeño.	152	16
739	Fortalecer la coordinación en materia de protección civil, igualdad de género, inclusión social y otros temas vinculados con los derechos económicos, sociales, culturales y ambientales con los municipios.	152	16
740	Crear programas itinerantes que permitan a la población, especialmente la apartada geográficamente o con vulnerabilidad acceder a trámites y servicios relacionados con la seguridad, justicia y certeza jurídica.	153	16
741	Estimular capacidades e infraestructura para la descentralización de los servicios de seguridad, justicia y certeza jurídica.	153	16
742	Promover mecanismos de cooperación que permitan adaptar, adoptar e implementar las mejores prácticas internacionales para el logro de las metas de los Objetivos de Desarrollo Sostenible.	154	16
743	Fortalecer los instrumentos jurídicos estatales que contemplan la cooperación internacional.	154	16
744	Fortalecer a las instituciones estatales responsables de los asuntos nacionales e internacionales.	154	16
745	Gestionar el acercamiento entre autoridades internacionales y el gobierno del estado para fomentar la cooperación internacional.	155	16
746	Fomentar la inclusión de los municipios en las acciones de eventos de carácter internacional.	155	16
747	Desarrollar sistemas y aplicaciones inclusivas para la generación, conservación y gestión de la información pública.	156	17
748	Fortalecer las capacidades técnicas, financieras y normativas de los sistemas de información.	156	17
750	Generar información pertinente, de calidad e incluyente que atienda las necesidades de los sectores público, privado, académico y social.	156	17
751	Establecer reglas de operación y padrones de beneficiarios claros, transparentes y consensuados de los bienes y servicios públicos.	156	17
752	Implementar catálogos que faciliten el registro, integración, consolidación y explotación de la información financiera, estadística y geográfica de la Administración Pública Estatal.	156	17
753	Implementar esquemas que promuevan el enfoque de derechos humanos en la implementación del gobierno abierto y el acceso a la información.	156	17
754	Desarrollar esquemas que faciliten la participación y corresponsabilidad de la ciudadanía en la vigilancia, control y evaluación del quehacer gubernamental.	157	17
755	Consolidar las políticas de transparencia en el quehacer gubernamental con el apoyo de los sectores privado, académico y social.	157	17
756	Implementar acciones que garanticen el derecho de acceso a la información y protección de datos personales conforme a los ordenamientos jurídicos aplicables.	157	17
757	Implementar herramientas tecnológicas con base en estándares internacionales para llevar a cabo contrataciones abiertas y que sean aplicables en las etapas de planeación, licitación, adjudicación, contratación e implementación.	157	17
758	Establecer incentivos para el uso y aprovechamiento de datos abiertos entre los sectores público, privado, académico y social.	157	17
759	Establecer mecanismos de supervisión y control de los recursos públicos con la participación de la ciudadanía.	158	16
760	Establecer mecanismos de vinculación con órganos estatales de control de otras entidades federativas para el intercambio de buenas prácticas y herramientas que faciliten la inspección o verificación de los recursos públicos.	158	16
761	Implementar un Código de Ética que rija la conducta ética y establezca un conjunto de valores y principios que dirijan el cumplimiento de los deberes y obligaciones de todos los servidores públicos del Gobierno del Estado de Yucatán.	158	16
762	Capacitar a los órganos de control interno asignados en las dependencias y entidades de la Administración Pública Estatal para mejorar la fiscalización de los recursos públicos.	158	16
763	Fortalecer la normatividad para los procesos de adquisiciones y actuación de los servidores públicos que participen en los mismos.	158	16
764	Desarrollar un sistema integral, transparente y accesible de las compras, concursos y licitaciones que realice el Gobierno del Estado.	158	16
765	Fortalecer la realización de auditorías y la publicación de resultados conforme a la norma aplicable.	158	16
766	Presentar las declaraciones patrimonial y de intereses de los servidores públicos de todos los niveles del Poder Ejecutivo del estado de Yucatán.	158	16
767	Promover la cultura de la legalidad y anticorrupción entre los sectores público, privado y social. 	159	16
768	Consolidar las Dependencias, Entidades y Órganos dedicados a prevenir y sancionar la corrupción atendiendo las obligaciones y responsabilidades en la normatividad vigente.	159	16
769	Implementar acciones que permitan identificar actos de corrupción en los trámites y servicios que presta el Gobierno del Estado a la población.	159	16
770	Establecer mecanismos transparentes de sanción a servidores públicos que incurran en actos de corrupción.	159	16
771	Reorientar la programación del presupuesto asegurando su  alineación con la planeación del desarrollo.	160	17
772	Promover la implementación del modelo de Presupuesto basado en Resultados y el Sistema de Evaluación del Desempeño en los municipios.	160	17
773	Consolidar los mecanismos de diseño e implementación de los programas presupuestarios.	160	17
774	Consolidar las capacidades institucionales para implementar el diseño basado en evidencia en los programas presupuestarios con enfoque a resultados.	160	17
775	Fortalecer las capacidades de las y los servidores públicos dedicados a la implementación de la gestión para resultados en el desarrollo y la incorporación de enfoques transversales al ciclo de la gestión pública. 	160	17
776	Mejorar los mecanismos para la evaluación social de proyectos de preinversión, priorizando la evaluación de aquellos que sean socialmente rentables, incluyentes y sostenibles.	160	17
777	Consolidar el Sistema de Seguimiento y Evaluación del Desempeño para la mejora de la gestión pública y la rendición de cuentas.	161	17
778	Evaluar el desempeño de los programas y proyectos de gobierno y su impacto en la población.	161	17
779	Presupuestar las políticas, programas y proyectos de la Administración Pública con base en el análisis de la información que resulte del seguimiento y evaluación.	161	17
780	Establecer  mecanismos de cooperación con organismos nacionales e internacionales para fortalecer la gestión pública estatal.	161	17
781	Desarrollar acciones que mejoren la calidad de la atención de los servidores públicos a la ciudadanía.	162	16
782	Establecer mecanismos de colaboración con organismos nacionales e internacionales que permitan  incorporar mejores prácticas en materia de gobernanza y mejora regulatoria.	162	16
783	Desarrollar estrategias de simplificación y digitalización de los trámites y servicios del estado con criterios claros y transparentes para su realización.	162	16
784	Impulsar el Sistema Estatal de Mejora Regulatoria así como sus herramientas	162	16
785	Reforzar las capacidades técnicas y profesionales de los servidores públicos de una manera incluyente.	162	16
786	Consolidar herramientas que evalúen el impacto económico y social generado por la normatividad estatal y municipal.	162	16
787	Actualizar el marco normativo en materia de mejora regulatoria.	162	16
788	Implementar la simplificación administrativa y reducción de cargas burocráticas.	163	16
789	Implementar procesos de innovación y gestión del conocimiento en las dependencias y entidades.	163	16
790	Desarrollar instrumentos de gobierno digital que impulsen la eficiencia y mejora de la Administración Pública Estatal.	163	16
791	Documentar los procesos y procedimientos de la Administración Pública con énfasis en los que impactan directamente a la ciudadanía; así como crear metodologías para asegurar la calidad en los procesos de la administración pública.	163	16
792	Modernizar la gestión de los bienes patrimoniales para optimizar el valor del patrimonio del Gobierno del Estado.	164	16
793	Desarrollar procesos de gestión del patrimonio vinculados integralmente a las distintas fases de la administración de los recursos.	164	16
794	Reforzar la gestión de los recursos humanos, garantizando la inclusión e igualdad,  mediante esquemas de contratación acorde a las necesidades de las Dependencias y Entidades, y mecanismos para evaluar e incentivar el desempeño de los servidores públicos.	164	16
795	Consolidar la base tributaria en el estado y los municipios.	165	17
796	Impulsar mecanismos que prevengan y sancionen la evasión fiscal.	165	17
797	Establecer mecanismos de simplificación de pago de obligaciones fiscales estatales, así como de productos, derechos y aprovechamientos, tomando como base las Tecnologías de la Información y la Comunicación.	165	17
798	Reforzar los mecanismos de relación y colaboración fiscal entre la Federación, el Gobierno del Estado y los municipios.	165	17
799	Establecer esquemas que permitan vincular determinados ingresos estatales hacia la ejecución de proyectos con alto impacto económico y social.	165	17
800	Desarrollar nuevos esquemas de recaudación fiscal.	165	17
801	Consolidar la capacidad recaudatoria de la Administración Pública Estatal para disminuir y erradicar la evasión fiscal.	165	17
802	Promover la austeridad en el gasto corriente para favorecer el gasto en capital humano e inversión.	166	17
803	Ordenar el sistema de pensiones de los trabajadores del Gobierno del Estado bajo los principios de sostenibilidad, viabilidad y responsabilidad financiera.	166	17
804	Implementar programas, proyectos y acciones focalizadas que potencialicen y aprovechen la inversión de los entes públicos y privados.	166	17
805	Administrar de forma eficiente y responsable la deuda pública.	166	17
806	Transferir oportunamente los recursos correspondientes a las entidades, municipios y organismos del Estado de Yucatán para el cumplimiento de sus objetivos y fines.	166	17
807	Establecer mecanismos que faciliten la integración de un presupuesto ciudadano, incluyente y que responda a las necesidades de desarrollo.	166	17
808	Establecer herramientas y protocolos para la toma de decisiones en materia presupuestal.	166	17
809	Supervisar que se cumplan las obligaciones de disciplina financiera.	166	17
810	Planificar la inversión conjunta público-privada que contemple proyectos estratégicos para la economía.	167	17
811	Optimizar el gasto en inversión pública para alcanzar las metas físicas y financieras en materia de infraestructura.	167	17
812	Diseñar un plan para la construcción y recuperación de espacios públicos de convivencia en las ciudades y comunidades del estado.	167	17
813	Desarrollar un programa de posicionamiento estratégico para las localidades que ofrecen espacios adecuados y atractivos.	167	17
814	Promover la creación y adecuación de infraestructura pública con énfasis en el cumplimiento de las disposiciones en materia de inclusión.	167	17
815	Generar parámetros e indicadores para mejorar la infraestructura de la obra pública en el estado.	167	17
816	Estimular vínculos con la sociedad civil para el desarrollo de la obra pública sostenible.	167	17
817	Impulsar y fortalecer el alcance de la red de drenaje y alcantarillado del estado.	167	17
818	Impulsar la conexión con las viviendas a la red pública en materia de desalojo de residuos. 	167	17
819	Construir carreteras para las localidades más apartadas del estado.	168	9
820	Reconstruir la carpeta asfáltica de las vialidades, priorizando aquellas que presenten un alto nivel de daño e inseguridad.	168	9
821	Promover la modernización de la infraestructura vial en el interior del estado.	168	9
822	Realizar trabajos de conservación en las vialidades rurales.	168	9
823	Desarrollar circuitos carreteros  que permitan la conectividad vial de acuerdo a su actividad económica: agrícola, ganadera, turística e industrial.	168	9
824	Impulsar esquemas innovadores de transporte público de acuerdo a las características de las ciudades.	168	9
825	Fortalecer la red existente de transporte público y promover el diseño universal.	168	9
826	Promover la construcción de nuevos tramos ferroviarios sostenibles.	169	9
827	Modernizar la red ferroviaria estatal de forma conjunta y coordinada con los distintos órdenes de gobierno.	169	9
828	Consolidar los centros logísticos multimodales para el movimiento de carga por ferrocarril.	169	9
829	Implementar acciones de desazolve de puertos y zonas de embarcación.	170	9
830	Promover acciones de conservación, adaptación integral y mejoramiento sostenible de la infraestructura aeroportuaria y portuaria, en coordinación con los diferentes órdenes de gobierno.	170	9
831	Desarrollar espacios para el almacenamiento y movimiento logístico de mercancías.	170	9
832	Consolidar la operación de medios de transporte, seguros y sostenibles.	170	9
833	Reforzar la infraestructura de almacenamiento existente de forma equilibrada en las regiones del estado.	170	9
834	Consolidar la infraestructura digital sostenible en las comunidades.	171	9
835	Proporcionar internet gratuito en lugares y espacios públicos.	171	9
836	Gestionar convenios de desarrollo con otros órdenes de gobierno para fortalecer el despliegue de la infraestructura de telecomunicaciones.	171	9
837	Realizar estudios para determinar las zonas de mayor necesidad de acceso a las redes y servicios de telecomunicación en el estado.	171	9
838	Instruir sobre el uso responsable del internet a través de la capacitación con enfoque de integralidad.	171	9
839	Reforzar la calidad y el óptimo desempeño de los servicios electrónicos del gobierno.	172	9
840	Diseñar espacios de almacenamiento, integración, intercambio y aprovechamiento de recursos digitales.	172	9
841	Facilitar el acceso a herramientas tecnológicas que favorezcan la modernización digital.	172	9
842	Promover la inversión para el desarrollo de infraestructura tecnológica digital sostenible.	172	9
843	Desarrollar lineamientos que ofrezcan oportunidades de desarrollo sostenible e incluyente.	173	9
844	Generar alianzas con las instituciones educativas de nivel superior para elaborar programas de desarrollo sostenible e incluyente.	173	9
845	Promover el desarrollo de programas educativos integrales apoyados por las empresas e instituciones del sector de telecomunicaciones.	173	9
846	Gestionar convenios con asociaciones civiles, industrias e instituciones que apoyen la alfabetización digital.	173	9
847	Desarrollar acciones integrales que maximicen los esfuerzos educativos en el servicio de la red digital incluyente.	173	9
848	Diseñar programas de ordenamiento territorial en condiciones adecuadas de seguridad física y patrimonial.	174	11
849	Realizar intervenciones que reduzcan la vulnerabilidad y riesgo de los asentamientos humanos en las comunidades de mayor marginación.	174	11
850	Instituir la perspectiva de género, la accesibilidad universal y la seguridad como componentes transversales de la planeación urbana.	174	11
851	Implementar acciones en colaboración con los municipios para la delimitación territorial.	174	11
852	Promover el crecimiento urbano vertical y la adaptación del territorio.	174	11
853	Reforzar la vinculación con los municipios para la elaboración de proyectos que garanticen la calidad del espacio público y la  inclusión de espacios verdes.	175	11
854	Establecer observatorios de planeación y ordenamiento territorial con enfoque de sostenibilidad.	175	11
855	Fortalecer programas institucionales en los municipios y con vinculación a las dependencias en materia de ordenamiento territorial.	175	11
856	Gestionar la actualización del marco normativo en materia de ordenamiento territorial y desarrollo urbano para la regulación sostenible del territorio.	175	11
857	Instruir a los municipios en materia de regulación ecológica que permita la protección, preservación, restauración y aprovechamiento de los recursos naturales.	176	11
858	Conservar los recursos naturales locales a través de lineamientos en materia urbanística sustentable.	176	11
859	Diseñar programas regionales y metropolitanos que propicien el desarrollo urbano de las ciudades y comunidades con base en su potencialidad de patrimonio natural y ecológico.	176	11
860	Identificar áreas de oportunidad que propicien la generación de inversiones en energías limpias.	176	11
861	Promover que los proyectos de infraestructura se apeguen a una planeación sostenible con rentabilidad financiera y de impacto socioeconómico.	177	11
862	Incorporar el enfoque de sostenibilidad y desarrollo urbano sustentable en la prestación de los servicios públicos.	177	11
863	Planificar adecuadamente el desarrollo de las zonas destinadas al progreso económico del estado en coordinación con los sectores público, privado, social y académico.	177	11
864	Promover acciones para la sustitución del uso de leña y carbón dentro de las viviendas.	177	11
865	Promover esquemas asequibles de financiamiento a la vivienda para los trabajadores del estado y grupos en situación de vulnerabilidad. 	177	11
866	Definir áreas de atención prioritarias que coordinen los esfuerzos federales, estatales y municipales para la promoción de la inversión privada.	178	0
867	Establecer una cartera de proyectos estratégicos de inversión pública con impacto regional para su gestión y financiamiento conjunto.	178	0
868	Fortalecer los mecanismos de carácter participativo en las decisiones de inversión pública a nivel regional para priorizar aquellas acciones que sean de mayor interés para las comunidades.	178	0
869	Proporcionar la colaboración entre los municipios para la provisión de servicios públicos desde una perspectiva regional.	178	0
870	Incluir la perspectiva regional en la definición de las políticas de seguridad alimentaria, considerando las capacidades endógenas de las comunidades para mejorar la calidad de vida.	179	0
871	Ampliar la cobertura regional en educación preescolar y superior, para disminuir el rezago educativo.	179	0
872	Promover acciones coordinadas en materia de desarrollo económico y social para el bienestar de la etnia maya.	179	0
873	Promover la atención regional equilibrada de los servicios de salud públicos mediante la provisión de espacios, infraestructura, equipamiento, insumos y personal para un servicio de calidad.	179	0
874	Acercar los servicios públicos a las comunidades mayas considerando una perspectiva intercultural que permita mejorar su calidad de vida.	179	0
875	Incrementar la oferta de actividades culturales en los municipios del estado.	180	0
876	Fortalecer los programas de apoyo a la promoción cultural a las actividades que se realicen en los municipios.	180	0
877	Crear un inventario del patrimonio cultural de las regiones del estado.	180	0
878	Fortalecer la infraestructura cultural con enfoque regional.	180	0
879	Promover el uso de medios alternos de movilidad para garantizar la reducción de emisiones de gases contaminantes.	181	0
880	Establecer programas de educación ambiental en todo el estado.	181	0
881	Crear un sistema de información regional, que permita realizar el seguimiento y la evaluación a las políticas ambientales a nivel regional y municipal.	181	0
882	Establecer mecanismos de conservación y promoción para el uso sostenible de los recursos naturales.	181	0
883	Potencializar las aptitudes geográficas del estado para el desarrollo de energías renovables.	181	0
884	Involucrar a las comunidades del interior del estado como un elemento indispensable en el desarrollo de programas para el combate al delito.	182	0
885	Incorporar en las acciones de prevención y persecución del delito sistemas de información y herramientas de análisis con enfoque territorial.	182	0
886	Fortalecer la cobertura de servicios de la infraestructura regional en materia de seguridad.	182	0
887	Realizar convenios de colaboración entre estado y los municipios para la implementación del PBR-SED y su orientación al cumplimiento de los ODS.	183	0
888	Asesorar y capacitar a los Ayuntamientos en la implementación del PBR-SED.	183	0
889	Evaluar los avances de los municipios del estado en la implementación del PBR-SED.	183	0
890	Asesorar y capacitar a los municipios en el cumplimiento Ley de Disciplina Financiera de las Entidades Federativas y los Municipios.	183	0
891	Armonizar los diversos instrumentos de planeación regional, particularmente entre el ordenamiento territorial y del desarrollo urbano para eficientar la planeación del desarrollo.	184	0
892	Fomentar la elaboración e implementación de los programas de ordenamiento ecológico local que integren un enfoque de manejo del paisaje para garantizar la compatibilidad de las diversas actividades que se desarrollan de acuerdo a la vocación del territorio.	184	0
893	Mejorar la infraestructura del transporte en el Estado considerando una perspectiva regional en su cobertura para eficientar la movilidad en las comunidades.	184	0
894	Incrementar la cobertura regional en el otorgamiento de trámites y servicios públicos estatales que permita descentralizar su provisión desde Mérida para un acceso eficiente de las comunidades.	184	0
895	Promover proyectos de inversión pública con impacto meso regional que permitan el desarrollo de las comunidades en el área de influencia de los mismos.	185	0
896	Favorecer el intercambio comercial y turístico en la península de Yucatán, para dinamizar la economía entre las regiones fronterizas.	185	0
897	Establecer mecanismos de colaboración para la atención de retos comunes en materia de desarrollo en la península.	185	0
\.


--
-- Data for Name: PED2019Objetivo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."PED2019Objetivo" ("iIdObjetivo", "vObjetivo", "iIdTema") FROM stdin;
1	Aumentar la actividad comercial sostenible del estado	1
2	Incrementar la productividad de las empresas comerciales en el estado	1
3	Aumentar la competitividad del estado 	2
4	Incrementar la inversión extranjera en el estado	2
5	Incrementar la actividad económica sostenible del sector secundario	3
6	Incrementar la productividad del sector industrial sostenible	3
7	Aumentar el valor de los productos y servicios turísticos con enfoque de sostenibilidad en Yucatán	4
8	Incrementar la afluencia de visitantes a Yucatán	4
9	Incrementar la estadía turística en Yucatán	4
10	Incrementar la calidad del empleo en Yucatán 	5
11	Aumentar la productividad laboral en el estado	5
12	Aumentar la independencia económica de la población del estado de Yucatán	6
13	Mejorar la actividad económica del sector agropecuario con enfoque sostenible	7
14	Incrementar el valor de la producción del sector pecuario con enfoque de sostenibilidad	7
15	Incrementar el valor de la producción agrícola en el estado con enfoque de sostenibilidad	7
16	Incrementar el valor de la producción pesquera en el estado con enfoque de sostenibilidad	8
17	Incrementar el acceso incluyente y de calidad al Sistema Estatal de Salud	9
18	Mejorar la condición de salud de la población en el estado	9
19	Disminuir toda forma de desnutrición en la población del estado de Yucatán	10
20	Incrementar la seguridad e inocuidad alimentaria sostenible de la población del estado de Yucatán	10
21	Disminuir la pobreza y pobreza extrema en los pueblos indígenas de Yucatán	11
22	Disminuir el rezago educativo de la población del estado	12
23	Mejorar la calidad del sistema educativo estatal	12
24	Mejorar la calidad de la vivienda en Yucatán 	13
25	Mejorar los servicios básicos en las viviendas del estado	13
26	Incrementar el acceso a la seguridad social con enfoque de sostenibilidad de la población yucateca	14
27	Incrementar la producción de bienes y servicios culturales	15
28	Aumentar el consumo cultural y la participación de la población en espacios y eventos culturales	15
29	Preservar las tradiciones e identidad cultural	16
30	Incrementar las creaciones artísticas	17
31	Mejorar la cobertura de la educación artística en la educación básica con un enfoque integral e incluyente	18
32	Aumentar la formación de profesionales de las artes	18
33	Preservar el patrimonio cultural del estado	19
34	Mejorar el desempeño de los deportistas yucatecos en competencias de alto rendimiento	20
35	Aumentar la activación física de la población en todas las edades, grupos sociales y regiones del estado	20
36	Aumentar la presencia de la población en eventos deportivos profesionales	20
37	Preservar los recursos naturales protegidos del estado de Yucatán	21
38	Mejorar la protección del ecosistema terrestre del estado	21
39	Disminuir la vulnerabilidad del estado ante los efectos del cambio climático	22
40	Mejorar la calidad del aire en Yucatán	22
41	Mejorar el saneamiento de aguas residuales en Yucatán	23
42	Mejorar la calidad del agua en el estado 	23
43	Mejorar el manejo de los residuos en Yucatán	24
44	Reducir la generación de residuos en Yucatán	24
45	Incrementar la generación de energía no contaminante en Yucatán 	25
46	Mejorar el acceso a energías limpias en el estado 	25
47	Incrementar la protección del ecosistema marino del estado de Yucatán	26
48	Incrementar el acceso a sistemas de transporte seguros, asequibles y eficientes en Yucatán	27
49	Mejorar las condiciones de desplazamientos y accesibilidad en Yucatán	27
50	Reducir las brechas de género en salud	28
51	Reducir las brechas de género en educación	28
52	Incrementar la autonomía y empoderamiento de las mujeres	28
53	Reducir la incidencia de las violencias hacia las mujeres en el estado	28
54	Incrementar la igualdad de oportunidades de los grupos en situación de vulnerabilidad	29
55	Incrementar la formación de capital humano con competencias y habilidades productivas y técnicas	30
56	Mejorar la calidad de la educación superior en el estado	30
57	Incrementar el aprovechamiento del conocimiento científico y tecnológico en el estado	31
58	Fortalecer las condiciones para la innovación, ciencia y tecnología en el estado	31
59	Preservar altos niveles de paz en la entidad	32
60	Disminuir la incidencia delictiva en el estado	32
61	Mejorar el desempeño de las instituciones de procuración de justicia en el estado	33
62	Disminuir la impunidad en el estado	33
63	Mejorar la estabilidad de las instituciones y su apego al estado de derecho en Yucatán en beneficio de los derechos humanos, en especial de las personas en situación de vulnerabilidad	34
64	Aumentar la cooperación nacional e internacional de Yucatán	34
65	Mejorar la calidad, oportunidad y disponibilidad de la información para la toma de decisiones	35
66	Disminuir la incidencia de corrupción en la Administración Pública Estatal	35
67	Mejorar la calidad del gasto público con base en evidencia rigurosa	36
68	Mejorar la efectividad en la gestión pública  a través de la mejora  regulatoria	37
69	Mejorar la sostenibilidad de las finanzas públicas	38
70	Incrementar la inversión en obra pública sostenible y accesible	39
71	Incrementar la conectividad sostenible e incluyente en los municipios del estado	40
72	Incrementar el acceso a las redes y servicios de telecomunicaciones sostenibles e incluyentes en las ciudades y comunidades del estado	41
73	Mejorar la planeación territorial con un enfoque sostenible en el estado	42
74	Disminuir la desigualdad territorial en el acceso a los derechos económicos, sociales, culturales y ambientales entre de las regiones que conforman el estado de Yucatán	43
75	Disminuir la desigualdad meso regional en el acceso a los derechos económicos, sociales, culturales y ambientales en la península de Yucatán	43
\.


--
-- Data for Name: PED2019Tema; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."PED2019Tema" ("iIdTema", "vTema", "iIdEje") FROM stdin;
1	Desarrollo comercial y fortalecimiento de las empresas locales.	1
2	Competitividad e inversión extranjera.	1
3	Desarrollo industrial.	1
4	Impulso al turismo.	1
5	Capital humano generador de desarrollo y trabajo decente.	1
6	Fomento empresarial y al emprendimiento.	1
7	Desarrollo agropecuario.	1
8	Desarrollo pesquero.	1
9	Salud y bienestar	2
10	Hambre cero	2
11	Pueblos indígenas	2
12	Educación integral de calidad	2
13	Acceso a la vivienda	2
14	Seguridad social	2
15	Acceso universal a la cultura	3
16	Cultura tradicional	3
17	Bellas artes	3
18	Educación artística y cultural	3
19	Patrimonio cultural	3
20	Fomento al deporte	3
21	Conservación de recursos naturales 	4
22	Acción por el clima	4
23	Agua limpia y saneamiento	4
24	Manejo integral de residuos	4
25	Energía asequible y no Contaminante	4
26	Vida submarina	4
27	Movilidad sustentable	4
28	Igualdad de género	5
29	Inclusión social y atención a grupos en situación de  vulnerabilidad	5
30	Educación superior y enseñanza científica y técnica 	6
31	Conocimiento científico, tecnológico e innovación	6
32	Paz	7
33	Justicia	7
34	Gobernabilidad	7
35	Gobierno abierto y combate a la corrupción	8
36	Gestión para Resultados en el desarrollo	8
37	Mejora regulatoria e innovación de la gestión pública	8
38	Finanzas Sanas	8
39	Inversión pública	9
40	Conectividad y transporte	9
41	Infraestructura digital	9
42	Ordenamiento territorial	9
43	N/A	10
\.


--
-- Data for Name: Partida; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Partida" ("iIdPartida", "vClave", "vPartida", "iActivo") FROM stdin;
\.


--
-- Data for Name: Permiso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Permiso" ("iIdPermiso", "vPermiso", "vDescripcion", "iTipo", "vUrl", "iIdPermisoPadre", "vClass", "iOrden", "iActivo", "iInicial") FROM stdin;
5	Apertura programática	CRUD Apertura programática	1	index.php/apertura	1	\N	4	1	0
9	Catálogo general de obra	Catálogo general de obra	1	index.php/C_obras	0	\N	2	1	0
10	Tablero	Tablero de control	1	 	0	\N	3	1	0
12	Por aperura programática	Por aperura programática	1	index.php/C_tablero	10	\N	2	1	0
11	Por obra	Tablero por obra	1	index.php/C_tablero	10	\N	1	1	0
13	Alta de obras	Pemite crear nuevas obras públicas	2	\N	0	\N	0	1	0
14	Alinear obras	Permite modificar la información referente  al PED y compromisos	2	\N	0	\N	0	1	0
1	Catálogos	Catálogos del sistema	1		0	\N	1	1	0
4	UGI	CRUD UGI	1	index.php/C_catalogos/principal/UGI	1	\N	3	1	0
7	Unidades de medida	CRUD Unidades de medida	1	index.php/C_catalogos/principal/Unidad	1	\N	6	1	0
6	Fuentes de financiamiento	CRUD F.F.	1	index.php/C_catalogos/principal/Financiamiento	1	\N	5	1	0
8	UBP	CRUD UBP	1	index.php/C_catalogos/principal/UBP	1	\N	7	1	0
2	Usuarios	CRUD Usuarios	1	index.php/C_usuarios	0	\N	1	1	0
3	Dependencias	CRUD Dependencias	1	index.php/C_catalogos/principal/Dependencia	1	\N	2	1	0
\.


--
-- Data for Name: Rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Rol" ("iIdRol", "vRol", "iActivo") FROM stdin;
1	Administrador general	1
2	Usuario SOP	1
3	Usuario SEPLAN	1
4	Usuario SAF	1
5	Usuario dependencia	1
6	Usuario ejecutor	1
\.


--
-- Data for Name: RolPermiso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."RolPermiso" ("iIdRol", "iIdPermiso", "iTipoAcceso") FROM stdin;
1	1	2
1	2	2
1	3	2
1	4	2
1	5	2
1	6	2
1	7	2
1	8	2
1	9	2
1	10	2
1	11	2
1	12	2
\.


--
-- Data for Name: Suficiencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Suficiencia" ("iIdSuficiencia", "vDescripcion", "vRuta", "iIdObra", "iActivo") FROM stdin;
\.


--
-- Data for Name: UBP; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."UBP" ("iIdUBP", "vUBP", "iAnio", "iActivo", "vClave") FROM stdin;
1	Elaboración de Estudios para la Construcción del Centro de Alto Rendimiento Deportivo, en el Municipio de Mérida, Yucatán.	2019	1	20263
2	Elaboración de Estudios para la construcción de la Academia Yucateca de Beisbol en el municipio de Mérida, Yucatán	2019	1	20264
3	Elaboración de Estudios para la construcción del estadio de fútbol en el municipio de Mérida, Yucatán	2019	1	20580
4	Elaboración de Estudios para la Modernización del Estadio General Salvador Alvarado en el Municipio de Mérida	2019	1	20265
5	Modernización de las Galerías del Teatro Peón Conteras	2019	1	20306
6	Modernización del Centro de Artes Visuales	2019	1	20323
7	Modernización de centros culturales del municipio de Mérida	2019	1	20329
8	Estudio de preinversión para la construcción del parador turístico playero en la localidad y municipio de Celestún	2019	1	20243
9	Estudio de preinversión para la construcción del parador turístico en la zona arqueológica de Kulubá, en el municipio de Tizimín	2019	1	20245
10	Estudio de preinversión para la construcción del parador turístico en las Coloradas, en el municipio de Río Lagartos	2019	1	20248
11	Estudio de preinversión para la construcción del parador turístico en la localidad y municipio de Izamal	2019	1	20249
12	Estudio de preinversión para la construcción del parador turístico en la zona arqueológica de Mayapán, en el municipio de Tecoh	2019	1	20251
13	Estudio de preinversión para la construcción del parador turístico y artesanal en zona de playa del puerto Progreso, Yucatán.	2019	1	20252
14	Estudio de preinversión para la construcción del parador turístico en la localidad de Sisal, municipio de Hunucmá	2019	1	20253
15	Rehabilitación de la infraestructura del recinto ferial, en la localidad de Xmatkuil municipio de Mérida	2019	1	18492
16	Construcción de un Centro Integral de Mejoramiento Genético Ganadero en el Municipio de Tizimín	2019	1	20123
17	Construcción de Caminos Saca-Cosecha en los municipios del Estado de Yucatán	2019	1	20415
18	Rehabilitación, construcción y restauración de infraestructura en el Centro Histórico y las plazas de los barrios de San José, Santiago y La Candelaria, en la localidad de Maní (Segunda Etapa).	2019	1	16899
19	Conservación Integral de la Imagen Urbana del Polígono de Acceso Turístico del municipio y localidad de Progreso, Yucatán	2019	1	20021
20	Mejoramiento de la imagen urbana y conversión de la red eléctrica, aérea y subterránea de la calle 60, 62 y Centro Histórico de Mérida.	2019	1	20022
21	Mantenimiento emergente a planteles de educación preescolar en el estado de Yucatán.	2019	1	16995
22	Mantenimiento emergente a planteles de educación primaria en el estado de Yucatán	2019	1	16999
23	Mantenimiento emergente a planteles de educación secundaria en el estado de Yucatán	2019	1	17087
24	Construcción y equipamiento de espacios educativos del Colegio de Estudios Científicos y Tecnológicos, plantel 02 Hunucmá	2019	1	16876
25	Equipamiento del taller de Soporte y mantenimiento de equipos de cómputo del Cecytey plantel 06 Emiliano Zapata, de Mérida (segunda etapa)	2019	1	19798
26	Construcción y equipamiento de un aula de tutorías en el Centro de Estudios Científicos y Tecnológicos, plantel 01 Espita	2019	1	19867
27	Equipamiento de biblioteca del Centro de Estudios Científicos y Tecnológicos, plantel 04 Hoctún	2019	1	20244
28	Construcción y equipamiento de espacios educativos en el Colegio de Bachilleres, plantel Tixkokob (segunda etapa)	2019	1	19747
29	Construcción y equipamiento de espacios educativos en el Colegio de Bachilleres, plantel Ticul	2019	1	19851
30	Construcción y equipamiento de espacios educativos en el Colegio de Bachilleres, plantel Kanasín	2019	1	19856
31	Mantenimiento de la infraestructura física educativa en centros educativos de nivel preescolar en el Estado de Yucatán.	2019	1	16717
32	Mantenimiento de la infraestructura física educativa en centros educativos de nivel primaria en el Estado de Yucatán.	2019	1	16740
33	Mantenimiento de la infraestructura física educativa en centros educativos de nivel secundaria en el Estado de Yucatán.	2019	1	16742
34	Ampliación de espacios educativos en el nivel preescolar en el estado de Yucatán.	2019	1	16745
35	Ampliación de espacios educativos en el nivel de educación primaria en el estado de Yucatán.	2019	1	16747
36	Ampliación de espacios educativos en el nivel de educación secundaria en el estado de Yucatán.	2019	1	16750
37	Adquisición de mobiliario y equipo para escuelas de nivel preescolar en el estado de Yucatán.	2019	1	16965
38	Adquisición de mobiliario y equipo para escuelas de nivel primaria en el estado de Yucatán.	2019	1	16967
39	Adquisición de mobiliario y equipo para escuelas de nivel secundaria en el estado de Yucatán.	2019	1	16968
40	Mantenimiento menor de edificios y oficinas administrativas de la Secretaría de Educación del Estado de Yucatán	2019	1	16683
41	Programa de Mantenimiento para la Reacreditación de Unidades de Primer Nivel del Estado de Yucatán	2019	1	18644
42	Ampliación y equipamiento de espacios educativos de la Universidad de Oriente tercera etapa.	2019	1	16991
43	Construcción y equipamiento de la quinta etapa de la Universidad Politécnica de Yucatán.	2019	1	18433
44	Equipamiento especializado para laboratorios de la Universidad Tecnológica del Centro (segunda etapa)	2019	1	14342
45	Mantenimiento y Equipamiento de la infraestructura física de la Universidad Tecnológica Metropolitana (Etapa 2)	2019	1	17832
46	Construcción y equipamiento de la Universidad Tecnológica del Poniente (primera etapa)	2019	1	14368
47	Modernización del Registro Civil en la localidad y municipio de Mérida.	2019	1	20235
48	Ampliación de red eléctrica en media y baja tensión en la localidad de San Luis Dzununcán municipio de Mérida.	2019	1	14363
49	Ampliación de red eléctrica en media y baja tensión en la localidad de Kopte, municipio de Motul.	2019	1	16959
50	Ampliación de red eléctrica en media y baja tensión en la localidad y municipio de Yobaín.	2019	1	16961
51	Ampliación de red eléctrica en media y baja tensión en la localidad de San Agustín (Salvador Alvarado) municipio de Tekax.	2019	1	18992
52	Ampliación de red eléctrica en media y baja tensión en la localidad de Xohuayán municipio de Oxkutzcab.	2019	1	18993
53	Ampliación de red eléctrica en media y baja tensión en la localidad de Cheumán municipio de Mérida.	2019	1	18995
54	Ampliación de red eléctrica en media y baja tensión en la localidad de Yaxché de Peón municipio de Ucú.	2019	1	18996
55	Ampliación de red eléctrica en media y baja tensión en la localidad de Chalmuch municipio de Mérida.	2019	1	18997
56	Ampliación de red eléctrica en media y baja tensión en la localidad de Dzidzilché municipio de Mérida.	2019	1	18998
57	Ampliación de red eléctrica en media y baja tensión en la localidad de Kikteil municipio de Mérida.	2019	1	18999
58	Ampliación de red eléctrica en media y baja tensión en la localidad de Chan Chocholá (Santa Eduviges Chan Chocholá) municipio de Maxcanú.	2019	1	19010
59	Ampliación de la Unidad Especializada en el Combate al Secuestro de la Fiscalía Genral del Estado, en Mérida Yucatán. 	2019	1	19561
60	Construcción del centro de profesionalización y adiestramiento del estado de Yucatán.	2019	1	17115
61	Equipamiento para la seguridad pública (FORTASEG) 2019	2019	1	18828
62	Remodelación del comedor del edificio central de la Secretaría de Seguridad Pública del estado de Yucatán	2019	1	19873
63	Construcción y equipamiento de la Escuela Superior de Artes de Yucatán	2019	1	19021
64	Implementación de sistema Bypass para el trasvase de arena en la escollera de Puerto de Abrigo de Telchac Puerto, Yucatán	2019	1	20435
65	Proyecto regional e integral para el manejo de residuos sólidos urbanos en la Zona Metropolitana de Mérida.	2019	1	20480
66	Aplicación de los recursos para el financiamiento de las obras, estudios y acciones de la Zona Metropolitana de Mérida	2019	1	11446
67	Estudios de movilidad urbana sostenible en el área de influencia de la Zona Económica Especial de Progreso	2019	1	20240
68	Desarrollo teritorial, urbano y metropolitano en el área de Influencia de la Zona Económica Especial de Progreso	2019	1	20294
69	Estudios de actualización del Plan Maestro La Plancha en el municipio de Mérida 	2019	1	20309
70	Construcción de la Avenida de acceso a Caucel Norte y Construcción del paso superior vehicular de Chenkú km 37+250 del periférico de Mérida	2019	1	18060
71	Conservación de la carretera E.C. (Valladolid- Río Lagartos) - Las Coloradas, tramo: 0+000-21+200, subtramo: 0+000-6+000 en el municipio de Río Lagartos.	2019	1	18888
72	Conservación del libramiento de Tizimín, tramo: 0+000-12+000, en el municipio de Tizimín	2019	1	18953
73	Estudio de preinversión para la construcción de la carretera Teabo - Chicán en los municipios de Teabo y Tixméhuac	2019	1	19477
74	Estudio para la construcción de la carretera Tiholop-Chikindzonot en los municipios de Yaxcabá y Chikindzonot	2019	1	19478
75	Reconstrucción de la carretera E. C. (Muna-Peto)- Alfonso Caso -Becanchén, tramo: 15+347-26+000, en el municipio de Tekax.	2019	1	19895
76	Estudio de preinversión para la construcción de la carretera Bolón-Poxilá, tramo: 0+000-4+100, en el municipio de Umán 	2019	1	19904
77	Estudio de preinversión para la modernización de la carretera San Isidro Ochil-Tekit, tramo 0+000-12+063, en los municipios de Homún y Tekit.	2019	1	19905
78	Reconstrucción de calles en la localidad y municipio de Kanasín	2019	1	19909
79	Conservación de la carretera Nohuayún - Tetiz, tramo: 0+000-3+790, en el municipio de Tetiz	2019	1	19916
80	Conservación de la carretera Tixpéhual -Sahé, tramo: 0+000-6+650, en el municipio de Tixpéhual	2019	1	19918
81	Conservación de la carretera E. C. (Valladolid -Cancún)- Limite del Estado, tramo: 0+000-24+315, en el municipio de Chemax	2019	1	19970
82	Estudio de preinversión para la construcción de la carretera Dzitox-Villahermosa, tramo: 0+000-13+222, en el municipio de Chichimilá	2019	1	19990
83	Conservación de la Carretera Samaría-San Pedro Bacab, tramo: 0+000-10+700, en el municipio de Tizimín.	2019	1	20064
84	Conservación de la Carretera Telchac Pueblo-Telchac Puerto, tramo: 0+000-15+496, en los municipios de Telchac Pueblo y Telchac Puerto.	2019	1	20069
85	Conservación de la Carretera Telchac Pueblo-Dzidzantún, tramo: 0+000-24+410, en los municipios de Telchac Pueblo, Sinanché, Yobaín y Dzidzantún.	2019	1	20078
86	Conservación de la Carretera Telchac Puerto-Dzilam de Bravo, tramo: 0+000-39+315, en los municipios de Telchac Puerto, Sinanché, Yobaín y Dzidzantún	2019	1	20080
87	Construcción de la carretera San Isidro Yaxché-E.C. (Xul-Huntochac), tramo: 0+000-7+324, en el municipio de Tekax.	2019	1	20093
88	Estudio de preinversión para la Construcción del PSV Periférico-Complejo de Seguridad	2019	1	20110
89	Estudio de preinversión para la Construcción del PSV del Fracc. Juan Pablo km 43+605 del Periférico de la Ciudad de Mérida.	2019	1	20111
90	Estudio de preinversión para la Construcción del PSV de Tixcacal km 46+740 del Periférico de la Ciudad de Mérida.	2019	1	20112
91	Construcción de obras de protección a ductos de PEMEX en el PSV de Anikabil	2019	1	20124
92	Construcción de obras de protección a ductos de PEMEX en el acceso a la Zona Económica Especial de Progreso	2019	1	20125
93	Mantenimiento y limpieza de áreas verdes en el estado de Yucatán	2019	1	17734
94	Rehabilitación de cunetas recolectoras de agua pluvial en los derechos de vía en 30 tramos carreteros en el Estado 	2019	1	17735
95	Mantenimiento  del sistema de alumbrado público en tramos carreteros y periférico de la ciudad de Mérida	2019	1	17736
96	Rehabilitación de la iglesia San Pedro y San Pablo en la localidad y municipio de Samahil.	2019	1	19922
97	Construcción de mercado en la localidad de Chabihau municipio de Yobaín.	2019	1	19959
98	Construcción de gradas y baños en campo deportivo en localidad de Chabihau municipio de Yobaín	2019	1	19960
99	Construcción de mercado en la localidad y municipio de Muxupip	2019	1	19961
100	Rehabilitación del mercado en la localidad y municipio de Telchac Pueblo	2019	1	20170
101	Rehabilitación del mercado de la localidad y municipio de Progreso	2019	1	20172
102	Construccion de campo deportivo en la localidad de xkanchakan municipio de Tecoh	2019	1	20181
103	Construccion campo deportivo en la localidad de Telchaquillo, municipio de Tecoh	2019	1	20184
104	Construccion de parque en la localidad y municipio de Rio Lagartos	2019	1	20185
105	Rehabilitación del centro de salud  en la localidad y municipio de Hoctún	2019	1	20186
106	Mantenimiento de imagen urbana en la coloradas y municipio de Rio lagartos	2019	1	20187
107	Construcción del centro de salud en la localidad y municipio de Baca	2019	1	20195
108	Construcción de campo deportivo en la localidad de Sotuta de Peón, municipio de Tecoh	2019	1	20198
109	Rehabilitación del mercado en la localidad y municipio de Dzidzantún	2019	1	20201
110	Rehabilitación del mercado en la localidad y municipio de Oxkutzcab	2019	1	20205
111	Construccion de cubierta de cancha de usos multiples en la localidad y municipio de Mama.	2019	1	20206
112	Rehabilitación del centro de salud en la localidad y municipio de Panabá	2019	1	20217
113	Construccion de la Casa de la Cultura en la localidad y municipio de Chapab	2019	1	20219
114	Rehabilitación de parque infantil en la localidad y municipio de Chapab	2019	1	20220
115	Construcción de centro de salud en la localidad y municipio de Chapab	2019	1	20222
116	Mantenimiento del edificio que alberga el Palacio de Gobierno del estado de Yucatán en la localidad y municipio de Mérida	2019	1	20225
117	Construcción de cancha de usos múltiples en la localidad y municipio de Maxcanu.	2019	1	20226
118	Rehabilitación del malecón en la localidad y municipio de Dzilám de Bravo	2019	1	20228
119	Construcción del centro del adulto mayor en la localidad y municipio de Tekax.	2019	1	20233
120	Construcción del centro de autismo en la localidad y municipio de Tekax.	2019	1	20234
121	Construccion de la Casa de la Cultura en la localidad y municipio de Dzan	2019	1	20241
122	Estudios para la remodelación de la imagen urbana en la localidad y municipio de Progreso	2019	1	20357
123	Construcción de redes hidrométricas para cierre de circuitos, cajas de operación, válvulas y tomas domiciliarias del sector hidráulico 14, de la localidad de Mérida, Yucatán.	2019	1	15754
124	Construcción de redes hidrométricas para cierre de circuitos, cajas de operación, válvulas y tomas domiciliarias del sector hidráulico 7 de la ciudad de Mérida, Yucatán.	2019	1	15776
125	Ampliación de redes de distribución agua potable de diversas colonias de la localidad de Mérida.	2019	1	19785
126	Ampliación y mejoramiento del sistema de agua potable de diversos municipios del Estado de Yucatán.	2019	1	19796
127	Mejoramiento de eficiencia de las fuentes de captación, línea de conducción y estaciones de rebombeo del sistema de agua potable de la localidad y municipio de Progreso, Yucatán.	2019	1	20101
128	Programa de Construcción de Sanitarios con Biodigestor	2019	1	12790
129	Suministro e instalación del sistema de generación de energía fotovoltaica de la planta de tratamiento de aguas residuales Alemán, en Mérida.	2019	1	16125
130	Conclusión de la Planta de Tratamiento de Aguas Residuales de 80 LPS de Caucel II de la localidad de Mérida.	2019	1	18588
131	Mantenimiento general del sistema fotovoltaico en 3 plantas de tratamiento de aguas residuales de la localidad de Mérida Yucatán.	2019	1	20083
132	Tercera etapa de la Biblioteca del campus de Ciencias Sociales, Económico Administrativas y Humanidades	2019	1	20526
133	Construcción de edificio de laboratorios y cubículos para profesores de la Facultad de Ingeniería Química	2019	1	20527
134	Construcción de la Segunda etapa de remodelación y ampliación de la Facultad de Enfermería.	2019	1	20528
135	Continuidad de la Universidad Tecnológica del Mayab (octava etapa)	2019	1	15757
136	Fondo para la Accesibilidad en el Transporte Público para Personas con Discapacidad (FOTRADIS)	2019	1	20681
137	Consolidación del Teatro Jose María Iturralde y Traconis en Valladolid, Yucatán	2019	1	17900
138	Mantenimiento y Conservación del Centro Cultural la Ibérica en el municipio de Mérida	2019	1	20279
139	Mantenimiento y Conservación del Centro Cultural la Ibérica en el municipio de Mérida	2019	1	20634
140	Ampliación de espacios educativos en el nivel de preescolar  en el estado de Yucatán	2019	1	18030
141	Ampliación de espacios educativos en el nivel de primaria en el estado de Yucatán.	2019	1	18031
142	Ampliación de espacios educativos en el nivel de secundaria en el estado de Yucatán.	2019	1	18032
143	Mantenimiento de la infraestructura física educativa en centros educativos de nivel preescolar en el Estado de Yucatán.	2019	1	20609
144	Mantenimiento de la infraestructura física educativa en centros educativos de nivel primaria en el Estado de Yucatán.	2019	1	20610
145	Mantenimiento de la infraestructura física educativa en centros educativos de nivel secundaria en el Estado de Yucatán.	2019	1	20611
146	Mantenimiento emergente a planteles de educación preescolar en el estado de Yucatán.	2019	1	20594
147	Mantenimiento emergente a planteles de educación primaria en el estado de Yucatán	2019	1	20595
148	Mantenimiento emergente a planteles de educación secundaria en el estado de Yucatán	2019	1	20596
149	Reconstrucción del camino Kinil - Chicán, tramo: 0+000-2+185, en las localidades de Kinil y Chicán de los municipios de Tekax y Tixmehuac	2019	1	20133
150	Reconstrucción del camino E.C (Mérida -Tetiz) -Texán de Palomeque-Dzibikak, tramo: 0+000-10+509, en las localidades de Texán de Palomeque y Dzibikak, de los municipios de Hunucmá y Umán	2019	1	20612
151	Reconstrucción de calles en la localidad y municipio de Tixkokob	2019	1	20613
152	Reconstrucción de calles en la localidad y municipio de Maxcanú	2019	1	20615
\.


--
-- Data for Name: UGI; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."UGI" ("iIdUGI", "vClave", "vUGI", "vDescripcion", "iActivo") FROM stdin;
3	0001	UGI de prueba	UGI  de prueba	1
4	0001	UGI de prueba	UGI  de prueba	1
5	0002	UGI de prueba 2	UGI  de prueba 2	1
6	0003	UGI de prueba 3	UGI  de prueba 3	1
7	0002	UGI de prueba 2	UGI  de prueba 2	1
8	0002	UGI de prueba 2	UGI  de prueba 2	1
9	0002	UGI de prueba 2	UGI  de prueba 2	1
2	0002	UGI de prueba 2	UGI  de prueba 2	1
1	0001	UGI de prueba	UGI  de prueba	1
\.


--
-- Data for Name: Unidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Unidad" ("iIdUnidad", "vUnidad", "iActivo") FROM stdin;
2	Aula y Laboratorio	1
3	Barda	1
4	Biblioteca	1
6	Cancha	1
7	Cede	1
8	Centro de Cómputo	1
9	Circuito Hidrometrico	1
10	Edificio	1
11	Edificio Administrativo	1
12	Edificio de Docencia	1
13	Estacionamiento	1
14	Hectáreas	1
15	Kilómetros	1
16	Laboratorio	1
17	Metros Cuadrados	1
18	Metros Lineales	1
19	Módulo	1
20	Multigimnasio	1
21	Obra	1
22	Parque	1
23	Piso	1
24	Pista de Atletismo	1
25	Planta de Tratamiento	1
26	Pozo	1
27	Proyecto Ejecutivo	1
28	Psv	1
29	Puente Peatonal	1
30	Recamaras 	1
31	Red De Agua Potable	1
32	Red Hidráulica	1
33	Redes Hidrometricas	1
34	Rehabilitación	1
35	Remozamiento	1
36	Salones	1
37	Sanitarios	1
38	Sistema	1
41	Subestación Eléctrica	1
42	Taller	1
43	Tanque	1
44	Techos	1
45	Techumbre	1
46	Templo 	1
47	Unidad Deportiva	1
48	Urbanización	1
50	Postes	1
51	Construcción De Escuelas	1
52	Escuelas Intervenidas	1
53	Pasos Peatonales	1
54	Estudio	1
55	Unidad Básica De Vivienda	1
56	Luminaria	1
57	Lote Con Infraestructura	1
58	Estufa Ecologica	1
59	Subsidio	1
60	Vivienda Nueva	1
61	Cuartos	1
62	Geotubo 	1
64	Proyecto	1
65	Comedor	1
66	Mejoramiento	1
67	Aula Didáctica	1
68	Mantenimiento	1
69	Equipo	1
71	Unidad Médica	1
72	Impermeabilización	1
74	Red Electrica	1
75	Unidad Medica 	1
63	Planta de Selección de Rsu	1
70	Sanitario con Biodigestor	1
73	Espacios de Cocina	1
49	Zonas de Captacion	1
39	Sistema de Generacion De Energia Fotovoltaica	1
40	Sistema de Riego	1
1	Aula	1
5	Biodigestores	0
\.


--
-- Data for Name: Usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Usuario" ("iIdUsuario", "vUsuario", "vNombre", "vPrimerApellido", "vSegundoApellido", "vPassword", "vCorreo", "vToken", "iIdDependencia", "iIdRol", "iActivo") FROM stdin;
5	jorge.estrella	Jorge Emilio	Estrella	Zavala	36e618512a68721f032470bb0891adef3362cfa9	jorge.estrella@yucatan.gob.mx	\N	21	3	1
1	admin	Administrador	.	.	36e618512a68721f032470bb0891adef3362cfa9	jorge.estrellaz@yucatan.gob.mx	\N	1	1	1
\.


--
-- Data for Name: UsuarioPermiso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."UsuarioPermiso" ("iIdUsuario", "iIdPermiso", "iTipoAcceso") FROM stdin;
\.


--
-- Name: Apertura_iIdApertura_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Apertura_iIdApertura_seq"', 1, false);


--
-- Name: Categoria_iIdCategoria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Categoria_iIdCategoria_seq"', 1, false);


--
-- Name: Compromiso_iIdCompromiso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Compromiso_iIdCompromiso_seq"', 1, false);


--
-- Name: Contrato_iIdContrato_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Contrato_iIdContrato_seq"', 1, false);


--
-- Name: Convenio_iIdConvenio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Convenio_iIdConvenio_seq"', 1, false);


--
-- Name: Dependencia_iIdDependencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Dependencia_iIdDependencia_seq"', 3, true);


--
-- Name: Financiamiento_iIdFinanciamiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Financiamiento_iIdFinanciamiento_seq"', 1, false);


--
-- Name: Indicador_iIdIndicador_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Indicador_iIdIndicador_seq"', 1, false);


--
-- Name: LineaAccion_iIdLineaAccion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."LineaAccion_iIdLineaAccion_seq"', 1, false);


--
-- Name: Localidad_iIdLocalidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Localidad_iIdLocalidad_seq"', 1, false);


--
-- Name: Municipio_iIdMunicipio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Municipio_iIdMunicipio_seq"', 1, false);


--
-- Name: ODS_iIdOds_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."ODS_iIdOds_seq"', 1, false);


--
-- Name: Obra_iIdObra_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Obra_iIdObra_seq"', 1, false);


--
-- Name: Oficio_iIdOficio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Oficio_iIdOficio_seq"', 1, false);


--
-- Name: PED2019Eje_iIdEje_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."PED2019Eje_iIdEje_seq"', 1, false);


--
-- Name: PED2019Estrategia_iIdEstrategia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."PED2019Estrategia_iIdEstrategia_seq"', 1, false);


--
-- Name: PED2019Objetivo_iIdObjetivo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."PED2019Objetivo_iIdObjetivo_seq"', 1, false);


--
-- Name: PED2019Tema_iIdTema_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."PED2019Tema_iIdTema_seq"', 3, true);


--
-- Name: Partida_iIdPartida_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Partida_iIdPartida_seq"', 1, false);


--
-- Name: Permiso_iIdPermiso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Permiso_iIdPermiso_seq"', 14, true);


--
-- Name: Rol_iIdRol_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Rol_iIdRol_seq"', 6, true);


--
-- Name: Suficiencia_iIdSuficiencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Suficiencia_iIdSuficiencia_seq"', 1, false);


--
-- Name: UBP_iIdUbp_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."UBP_iIdUbp_seq"', 1, false);


--
-- Name: UGI_iIdUgi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."UGI_iIdUgi_seq"', 9, true);


--
-- Name: Unidad_iIdUnidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Unidad_iIdUnidad_seq"', 1, false);


--
-- Name: Usuario_iIdUsuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Usuario_iIdUsuario_seq"', 5, true);


--
-- Name: Apertura Apertura_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Apertura"
    ADD CONSTRAINT "Apertura_pkey" PRIMARY KEY ("iIdApertura");


--
-- Name: Categoria Categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Categoria"
    ADD CONSTRAINT "Categoria_pkey" PRIMARY KEY ("iIdCategoria");


--
-- Name: Compromiso Compromiso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Compromiso"
    ADD CONSTRAINT "Compromiso_pkey" PRIMARY KEY ("iIdCompromiso");


--
-- Name: Contrato Contrato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Contrato"
    ADD CONSTRAINT "Contrato_pkey" PRIMARY KEY ("iIdContrato");


--
-- Name: Convenio Convenio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Convenio"
    ADD CONSTRAINT "Convenio_pkey" PRIMARY KEY ("iIdConvenio");


--
-- Name: Coordenada Coordenada_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Coordenada"
    ADD CONSTRAINT "Coordenada_pkey" PRIMARY KEY ("iIdObra", "nPunto");


--
-- Name: Dependencia Dependencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Dependencia"
    ADD CONSTRAINT "Dependencia_pkey" PRIMARY KEY ("iIdDependencia");


--
-- Name: Financiamiento Financiamiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Financiamiento"
    ADD CONSTRAINT "Financiamiento_pkey" PRIMARY KEY ("iIdFinanciamiento");


--
-- Name: Indicador Indicador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Indicador"
    ADD CONSTRAINT "Indicador_pkey" PRIMARY KEY ("iIdIndicador");


--
-- Name: PED2019LineaAccion LineaAccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019LineaAccion"
    ADD CONSTRAINT "LineaAccion_pkey" PRIMARY KEY ("iIdLineaAccion");


--
-- Name: Localidad Localidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Localidad"
    ADD CONSTRAINT "Localidad_pkey" PRIMARY KEY ("iIdLocalidad");


--
-- Name: Municipio Municipio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Municipio"
    ADD CONSTRAINT "Municipio_pkey" PRIMARY KEY ("iIdMunicipio");


--
-- Name: ODS ODS_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ODS"
    ADD CONSTRAINT "ODS_pkey" PRIMARY KEY ("iIdOds");


--
-- Name: ObraFinanciamiento ObraFinanciamiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ObraFinanciamiento"
    ADD CONSTRAINT "ObraFinanciamiento_pkey" PRIMARY KEY ("iIdObra", "iIdFinanciamiento");


--
-- Name: ObraLocalidad ObraLocalidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ObraLocalidad"
    ADD CONSTRAINT "ObraLocalidad_pkey" PRIMARY KEY ("iIdObra", "iIdLocalidad");


--
-- Name: Obra Obra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Obra"
    ADD CONSTRAINT "Obra_pkey" PRIMARY KEY ("iIdObra");


--
-- Name: Oficio Oficio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Oficio"
    ADD CONSTRAINT "Oficio_pkey" PRIMARY KEY ("iIdOficio");


--
-- Name: PED2019Eje PED2019Eje_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019Eje"
    ADD CONSTRAINT "PED2019Eje_pkey" PRIMARY KEY ("iIdEje");


--
-- Name: PED2019Estrategia PED2019Estrategia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019Estrategia"
    ADD CONSTRAINT "PED2019Estrategia_pkey" PRIMARY KEY ("iIdEstrategia");


--
-- Name: PED2019Objetivo PED2019Objetivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019Objetivo"
    ADD CONSTRAINT "PED2019Objetivo_pkey" PRIMARY KEY ("iIdObjetivo");


--
-- Name: PED2019Tema PED2019Tema_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PED2019Tema"
    ADD CONSTRAINT "PED2019Tema_pkey" PRIMARY KEY ("iIdTema");


--
-- Name: Partida Partida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Partida"
    ADD CONSTRAINT "Partida_pkey" PRIMARY KEY ("iIdPartida");


--
-- Name: Permiso Permiso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Permiso"
    ADD CONSTRAINT "Permiso_pkey" PRIMARY KEY ("iIdPermiso");


--
-- Name: RolPermiso RolPermiso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolPermiso"
    ADD CONSTRAINT "RolPermiso_pkey" PRIMARY KEY ("iIdRol", "iIdPermiso");


--
-- Name: Rol Rol_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Rol"
    ADD CONSTRAINT "Rol_pkey" PRIMARY KEY ("iIdRol");


--
-- Name: Suficiencia Suficiencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Suficiencia"
    ADD CONSTRAINT "Suficiencia_pkey" PRIMARY KEY ("iIdSuficiencia");


--
-- Name: UBP UBP_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UBP"
    ADD CONSTRAINT "UBP_pkey" PRIMARY KEY ("iIdUBP");


--
-- Name: UGI UGI_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UGI"
    ADD CONSTRAINT "UGI_pkey" PRIMARY KEY ("iIdUGI");


--
-- Name: Unidad Unidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Unidad"
    ADD CONSTRAINT "Unidad_pkey" PRIMARY KEY ("iIdUnidad");


--
-- Name: UsuarioPermiso UsuarioPermiso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UsuarioPermiso"
    ADD CONSTRAINT "UsuarioPermiso_pkey" PRIMARY KEY ("iIdUsuario", "iIdPermiso");


--
-- Name: Usuario Usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Usuario"
    ADD CONSTRAINT "Usuario_pkey" PRIMARY KEY ("iIdUsuario");


--
-- Name: PK_Permis; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX "PK_Permis" ON public."RolPermiso" USING btree ("iIdRol", "iIdPermiso");


--
-- Name: Contrato FK_Contrato_Obra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Contrato"
    ADD CONSTRAINT "FK_Contrato_Obra" FOREIGN KEY ("iIdObra") REFERENCES public."Obra"("iIdObra") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: Convenio FK_Convenio_Obra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Convenio"
    ADD CONSTRAINT "FK_Convenio_Obra" FOREIGN KEY ("iIdObra") REFERENCES public."Obra"("iIdObra") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: Imagen FK_Imagen_Obra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Imagen"
    ADD CONSTRAINT "FK_Imagen_Obra" FOREIGN KEY ("iIdObra") REFERENCES public."Obra"("iIdObra") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: ObraFinanciamiento FK_ObraF_Financiamiento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ObraFinanciamiento"
    ADD CONSTRAINT "FK_ObraF_Financiamiento" FOREIGN KEY ("iIdFinanciamiento") REFERENCES public."Financiamiento"("iIdFinanciamiento") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: ObraFinanciamiento FK_ObraF_Obra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ObraFinanciamiento"
    ADD CONSTRAINT "FK_ObraF_Obra" FOREIGN KEY ("iIdObra") REFERENCES public."Obra"("iIdObra") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: ObraLocalidad FK_ObraLoc_Localidad; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ObraLocalidad"
    ADD CONSTRAINT "FK_ObraLoc_Localidad" FOREIGN KEY ("iIdObra") REFERENCES public."Obra"("iIdObra") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: ObraLocalidad FK_ObraLoc_Obra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ObraLocalidad"
    ADD CONSTRAINT "FK_ObraLoc_Obra" FOREIGN KEY ("iIdLocalidad") REFERENCES public."Localidad"("iIdLocalidad") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: Oficio FK_Oficio_obra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Oficio"
    ADD CONSTRAINT "FK_Oficio_obra" FOREIGN KEY ("iIdObra") REFERENCES public."Obra"("iIdObra") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: RolPermiso FK_RolPer_Per; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolPermiso"
    ADD CONSTRAINT "FK_RolPer_Per" FOREIGN KEY ("iIdPermiso") REFERENCES public."Permiso"("iIdPermiso") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: RolPermiso FK_RolPer_Rol; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RolPermiso"
    ADD CONSTRAINT "FK_RolPer_Rol" FOREIGN KEY ("iIdRol") REFERENCES public."Rol"("iIdRol") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: Suficiencia FK_Sufic_Obra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Suficiencia"
    ADD CONSTRAINT "FK_Sufic_Obra" FOREIGN KEY ("iIdObra") REFERENCES public."Obra"("iIdObra") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: Usuario FK_Usr_Dep; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Usuario"
    ADD CONSTRAINT "FK_Usr_Dep" FOREIGN KEY ("iIdDependencia") REFERENCES public."Dependencia"("iIdDependencia") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: UsuarioPermiso FK_Usr_Per; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UsuarioPermiso"
    ADD CONSTRAINT "FK_Usr_Per" FOREIGN KEY ("iIdPermiso") REFERENCES public."Permiso"("iIdPermiso") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: Usuario FK_Usr_Rol; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Usuario"
    ADD CONSTRAINT "FK_Usr_Rol" FOREIGN KEY ("iIdRol") REFERENCES public."Rol"("iIdRol") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: UsuarioPermiso FK_Usr_Usr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UsuarioPermiso"
    ADD CONSTRAINT "FK_Usr_Usr" FOREIGN KEY ("iIdUsuario") REFERENCES public."Usuario"("iIdUsuario") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

