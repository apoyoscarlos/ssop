<div class="row">
    <div class="col-md-4 mb-3">
                <label for="b_municipio">Municipio</label>
                <select class="custom-select" id="b_municipio" name="b_municipio">
                    <option value="">Seleccione...</option>
                    <?= $municipios ?>
                </select>
    </div>
    <div class="col-md-4 mb-3">
                <label for="b_localidad">Localidad</label>
                <select class="custom-select" id="b_localidad" name="b_localidad">
                    <option value="">Seleccione...</option>
                    <?= $localidades ?>
                </select>
    </div>
    <div class="col-md-4 mb-3">
        <div class="form-group">
            <div class="button-group">
            <button class="btn waves-effect waves-light btn-light" onclick="buildArrayLocalidad()" style="margin-top:30px">Agregar</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered display" style="width:100%" id="grid">
                        <thead>
                            <tr>
                                <th>Municipio</th> 
                                <th>Localidad</th>                       
                                <th width="160px"> </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
    </div>
</div>
<script>


var f = document.getElementById("b_municipio");
f.addEventListener("change", e => {
    return cargarLocalidad();
});

function cargarLocalidad() {
    var id = $('#b_municipio option:selected').val();

        $.post("<?=base_url();?>C_EtapaDocumental/cargarLocalidades",{id:id},function(resultado,status){

            if(resultado != "" || resultado.indexOf('Error') == -1){                       
                $('#b_localidad').empty().append('<option value="">Seleccione...</option>');
                $('#b_localidad').append(resultado);
            }
            else alerta(resultado,'error');                 
        });
}

function buildArrayLocalidad(){

    event.preventDefault();
    
    var idM = $('#b_municipio option:selected').val();
    var municipio = $('#b_municipio option:selected').text();
    var idL = $('#b_localidad option:selected').val();
    var localidad = $('#b_localidad option:selected').text();

    if(idM == "" || idL == ""){
        alerta("Debe seleccionar previamente un Municipio y una Localidad.", 'warning');
    }
    else{
        let obj = {
            idL: idL,
            municipio: municipio,
            localidad: localidad
        }

        //console.log('test');
        if(findElement(obj.idL) == null){
            tableArray.push(obj);
            addRow(obj, tableArray.length - 1);
        }
        else{
            alerta("La localidad ya fue agregada.","warning");
        }

    }
}

function addRow(obj, index){
    $("#grid > tbody").append("<tr><td>"+obj.municipio+"</td><td>"+obj.localidad+"</td><td><button type='button' class='btn btn-circle waves-effect waves-light btn-danger' data-toggle='tooltip' data-placement='top' title='Eliminar' onclick='confirmar(\"¿Esta usted seguro?\",removeItem,"+index+")'><i class='mdi mdi-close'></i></button></td></tr>");
}

function buildTable(table){

    $("#grid > tbody").empty();

    var index = 0;
    table.forEach(function (obj) {
        addRow(obj, index);
        index++;
    });
}

function findElement(id){
    const getElem = tableArray.find(o => o.idL === id);

    return getElem;
}

function removeItem(index){

    event.preventDefault();

    tableArray.splice(index, 1);

    buildTable(tableArray);
}

</script>