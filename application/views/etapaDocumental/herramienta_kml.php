<div class="row">
    <div class="col-md-4 mb-3">
                <label for="b_descripcion_kml">Descripcion del documento</label>
                <input required class="form-control" type="text" id="b_descripcion_kml" name="b_descripcion_kml" />
                <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                 </div>
    </div>
    <div class="col-md-4 mb-3">
                <label for="b_kml">Importar kml/kmz</label>
                <input type="file" id="b_kml" name="b_kml" />
    </div>
    <div class="col-md-4 mb-3">
              <p id="json" style="display:none"></p>
    </div>

</div>
<div class="row" id="maps">
    <div id="map" style="width:100%;height:500px"></div>
    <div id="capture"></div>
</div>

<script src="<?=base_url() ?>public/dist/js/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/togeojson/0.16.0/togeojson.min.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVTowrObEv_LxHtbqi74PeDQoHAS6EYpg&callback=initMap">
</script>
<script>
      var map, infoWindow;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(20.9753704, -89.6169586),
          zoom: 12,
          mapTypeId: 'terrain'
        });
      }

        let getDom = xml => (new DOMParser()).parseFromString(xml, "text/xml")
        let getExtension = fileName => fileName.split(".").pop()


        var f = document.getElementById("b_kml");
        f.addEventListener("change", e => {
            let file = e.target.files[0];
            var fullPath = document.getElementById('b_kml').value;
            /*IF KMZ OR KML*/
            if (getExtension(fullPath) === "kml") {
                readKML(file);
            }
            else{
                readKMZ(file);
            }

        });

        function readKMZ(file){
            let geoJson = getKmlDom(file).then(kmlDom => {
                let geoJsonObject = toGeoJSON.kml(kmlDom);

                readObject(geoJsonObject);
                return JSON.stringify(geoJsonObject);
            })
            geoJson.then(gj => document.getElementById("json").innerText = gj);
        }

        let getKmlDom = (kmzFile) => {
            var zip = new JSZip();
            return zip.loadAsync(kmzFile)
                .then(zip => {
                    let kmlDom = null
                    zip.forEach((relPath, file) => {
                        if (getExtension(relPath) === "kml" && kmlDom === null) {
                            kmlDom = file.async("string").then(getDom)
                        }
                    })
                    return kmlDom || Promise.reject("No kml file found")
                });
        }

        function readKML(file){
                      // readFile function is defined. ONLY KML
            var reader = new FileReader();
            let resulttxt = '';
            // file reading finished successfully
            reader.addEventListener('load', function(i) {
                var text = i.target.result;

                var jsonObj = toGeoJSON.kml((new DOMParser()).parseFromString(text, "text/xml"));
                readObject(jsonObj);

                let geoJson  =  JSON.stringify(jsonObj);
                document.getElementById("json").innerText = geoJson;
            });

            // read as text file
            reader.readAsText(file);
        }

        function readObject(kmlJson){

            var i = 0;


            kmlJson.features.forEach(function (obj) {

                var indexForm = 0;
                var lat = 0;
                var long = 0;

                var coordinates = [];

                if(obj.geometry.type == "Point"){

                    //Coordinate para maps
                    var coordinate = {
                            lat: obj.geometry.coordinates[1],
                            lng: obj.geometry.coordinates[0]
                        }

                    //Coordinate para DB
                    var coordinateEntity = {
                            nLat: obj.geometry.coordinates[1],
                            nLong: obj.geometry.coordinates[0],
                            nPunto: indexForm,
                            iTipo: 0,
                            nNombre: $("#b_descripcion_kml").val()
                        }

                        coordinates.push(coordinate);

                        coordinatesToSave.push(coordinateEntity);
                }
                else if(obj.geometry.type == "LineString")
                {
                    indexForm = 0;
                    for(var x=0; x<obj.geometry.coordinates.length; x++){
                        var coordinate = {
                            lat: obj.geometry.coordinates[x][1],
                            lng: obj.geometry.coordinates[x][0]
                        }

                        //Coordinate para DB
                        var coordinateEntity = {
                            nLat: obj.geometry.coordinates[x][1],
                            nLong: obj.geometry.coordinates[x][0],
                            nPunto: indexForm,
                            iTipo: 1,
                            nNombre: $("#b_descripcion_kml").val()
                        }

                        indexForm++;

                        coordinates.push(coordinate);

                        coordinatesToSave.push(coordinateEntity);
                    }
                }
                else{
                    indexForm = 0;
                    for(var x=0; x<obj.geometry.coordinates.length; x++){
                        for(var y=0; y<obj.geometry.coordinates[x].length; y++){
                            var coordinate = {
                                lat: obj.geometry.coordinates[x][y][1],
                                lng: obj.geometry.coordinates[x][y][0]
                            }

                            //Coordinate para DB
                            var coordinateEntity = {
                                nLat: obj.geometry.coordinates[x][y][1],
                                nLong:  obj.geometry.coordinates[x][y][0],
                                nPunto: indexForm,
                                iTipo: 2,
                                nNombre: $("#b_descripcion_kml").val()
                            }

                            indexForm++;

                            coordinates.push(coordinate);

                            coordinatesToSave.push(coordinateEntity);
                        }
                    }
                }

                lat = coordinates[0].lat;
                long = coordinates[0].lng;

                    if(i == 0){
                        map = new google.maps.Map(document.getElementById('map'), {
                        center: new google.maps.LatLng(lat, long),
                        zoom: 16,
                        mapTypeId: 'terrain'
                        });
                    }

                    if(obj.geometry.type == "LineString"){
                        buildLine(coordinates);
                    }

                    if(obj.geometry.type == "Polygon"){
                        buildPolygon(coordinates);
                    }

                    if(obj.geometry.type == "Point"){
                        buildPoint({lat: lat, lng: long});
                    }

                    i++;
                });
        }

        function buildPolygon(Coords){
            var polygon = new google.maps.Polygon({
                paths: Coords,
                strokeColor: '#1F2D3B',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#1F2D3B',
                fillOpacity: 0.35
            });
            polygon.setMap(map);
        }

        function buildLine(Coords){
            var flightPath = new google.maps.Polyline({
                path: Coords,
                geodesic: true,
                strokeColor: '#1F2D3B',
                strokeOpacity: 1.0,
                strokeWeight: 3
            });

            flightPath.setMap(map);
        }

        function buildPoint(obj){

            var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            var beachMarker = new google.maps.Marker({
                position: obj,
                map: map,
                icon: image
            });
        }
</script>

