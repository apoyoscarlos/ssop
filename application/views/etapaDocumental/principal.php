<br><br>
<form class="needs-validation was-validated" onsubmit="guardar(this,event);">
<div class="card">
    <div class="card-body">
        <h1 class="card-title">Obras</h1>
        <h5 class="card-subtitle"> Captura Documental de Proyectos de Obra. </h5>
        <br><br>

        <input type="hidden" id="id" name="id" value="<?=$id?>" />
      
        <div class="row">
            <div class="col-md-12 mb-3">
                <label for="b_partidas">Partida</label>
                <select class="custom-select" id="b_partidas" required name="b_partidas">
                    <option value="">Seleccione...</option>
                    <?= $partidas ?>
                </select>
                <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
            </div>

            <div class="col-md-6 mb-3">
                <label for="b_descImp" >Descripcion del impacto</label>
                <textarea name="b_descImp" required  class="form-control" id="b_descImp" rows="5" cols="10"></textarea>
                <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
            </div>

            <div class="col-md-6 mb-3">
                <label for="b_beneficiarios">Beneficiarios</label>
                <textarea name="b_beneficiarios" required  class="form-control" id="b_beneficiarios" rows="5" cols="10"></textarea>
                <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                 </div>
            </div>

            <div class="col-md-6 mb-3">
                <label for="b_importe">Importe Original</label>
                <input type="number"  class="form-control" required id="b_importe" name="b_importe" />
                <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                 </div>
            </div>

            <div class="col-md-6 mb-3">
                <label for="b_anio">Año Recurso</label>
                <input type="number" min="1900" max="2999" step="1" class="form-control" required id="b_anio" name="b_anio" />
                <div class="invalid-feedback">
                            Este campo no puede estar vacio y debe contener un año valido.
                 </div>
            </div>

            <div class="col-md-12 mb-3">
                <label for="b_categorias">Categoria de la obra</label>
                <select class="custom-select" id="b_categorias" required name="b_categorias">
                    <option value="">Seleccione...</option>
                    <?= $categorias ?>
                </select>
                <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
            </div>

            <div class="col-md-12 mb-3">
                <label for="b_domicilio">Domicilio de la obra</label>
                <textarea required  class="form-control" name="b_domicilio" id="b_domicilio" rows="5" cols="10"></textarea>
                <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                 </div>
            </div>

            
        </div>
    </div>
</div>
<!-- alternative pagination -->
<div id="contenedor"  class="card">
    <div class="card-body">

        <?php
        include_once('municipios.php');
        ?>
    </div>
</div>

<div id="contenedor2"  class="card">
    <div class="card-body">
        <?php
        include_once('herramienta_kml.php');
        ?>

        <br /> <br /> 
<center>
                        <button class="btn waves-effect waves-light btn-success" type="submit">Guardar</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse" onclick="regresar()">Cancelar</button>
                    </center>
    </div>
</div>

</form>

<script>

var tableArray = [];
var coordinatesToSave = [];
    
    function guardar(f, e) {
        e.preventDefault();

        if(tableArray == 'undefined' || tableArray.length == 0){
            return alerta("Es necesario agregar localidades.", "error");
        }

        if(coordinatesToSave == 'undefined' || coordinatesToSave.length == 0){
            return alerta("Es necesario cargar un archivo kml o kmz.", "error");
        }

        var dataToSend = { formdata: $(f).serialize(),
                    coordinates: JSON.stringify(coordinatesToSave),
                    localidades: JSON.stringify(tableArray) };

        $.ajax({
            type: "POST",
            url: "<?= base_url() ?>C_EtapaDocumental/insert", //Nombre del controlador
            data: dataToSend,
            success: function(resp) {
                alerta("Captura Documental almacenada con exito", "success");
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                alerta(textStatus, "error");

                console.log(errorThrown);
            }
        });
    }
</script>