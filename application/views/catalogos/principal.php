<br><br>
<div class="card">
     <div class="card-body">
         <h4 class="card-title">Buscar por:</h4>
         <form name="formbusqueda" id="formbusqueda" onsubmit="buscar(event);">
             <div class="row">
                 <div class="col-md-9 mb-9">
                     <label for="validationTooltip02">Nombre</label>
                     <input class="form-control" name="palabra" id="palabra" type="text" placeholder="Ingrese su texto aqui...">
                 </div>
                 <div class="col-sm-12 col-md-3">
                     <div class="form-group">
                         <div class="button-group">
                            <button type="submit" class="btn waves-effect waves-light btn-light" style="margin-top:30px" >Buscar</button>
                            <button type="button" class="btn waves-effect waves-light btn-primary" style="margin-top:30px" onclick="capturar(0)">Nuevo</button>
                         </div>
                     </div>
                 </div>
             </div>
         </form>
         <input type="hidden" id="start" value="<?=$start?>">
         <input type="hidden" id="length" value="<?=$length?>">
     </div>
 </div>
 <div class="card">
    <div class="card-body" id="panel2Contenido">
        <?php
            $data['columnas'] = $columnas;
            $data['catalogo'] = $catalogo;
            $this->view('catalogos/listado',$data);
        ?>
    </div>
 </div>
 <script>
    var table;

    function buscar(e){
        if (!e) { var e = window.event; }
        e.preventDefault();
        $("#start").val(0);
        cargar('<?= base_url() ?>C_catalogos/listado/<?=$catalogo?>', '#panel2Contenido','POST');
    }

    function capturar(id) {
        //  Guardamos la página actual
        $("#start").val(table.page.info().start);
        $("#length").val(table.page.len());
        //-------------------------------
        var variables = 'id=' + id + '&cat=<?=$catalogo?>';
        cargar('<?= base_url() ?>C_catalogos/capturar', '#panel2Contenido','POST',variables);
    }

    function confirmarEliminacion(id){
        confirmar('¿Realmente desea eliminar el registro seleccionado?',eliminar,id);
    }

    function eliminar(id) {
        $.post("<?=base_url();?>C_catalogos/eliminar",{id:id,cat:'<?=$catalogo?>'},function(resultado,status){
            if(resultado == "0"){                       
                alerta('El registro ha sido eliminado','success');
                regresar(e);
            }
            else alerta(resultado,'error');                 
        });
    }

    function regresar(e){
        e.preventDefault();
        cargar('<?= base_url() ?>C_catalogos/listado/<?=$catalogo?>', '#panel2Contenido','POST');
    }
</script>
