<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-10">
                <h4 class="card-title">Capturar de datos</h4>
            </div>
            <div class="col-md-2">
                <button class="btn btn-light" type="submit" onclick="regresar(event);"><i class="mdi mdi-arrow-left">Regresar</i></button>
            </div>
        </div>
        <br><br>           
        <form class="needs-validation was-validated" onsubmit="guardar(this,event);">
            <input type="hidden" name="cat" id="cat" value="<?=$cat;?>">
            <?php
            foreach ($campos as $key => $value)
            {
                if($key == 'iActivo' || $key == 'iId'.$cat)
                {
                    echo '<input class="form-control" id="validationCustom04" name="'.$key.'" value="'.$value.'" type="hidden">';
                }
                else
                {
                    echo '<div class="form-row">
                        <div class="col">
                            <label for="validationCustom04">'.substr($key,1).'</label>
                            <input class="form-control" id="validationCustom04" name="'.$key.'" required="" type="text" value="'.$value.'">
                            <div class="invalid-feedback">
                                Este campo no puede estar vacio.
                            </div>
                        </div>
                    </div>';
                }
            }

            ?>
            <div class="row">
                <div class="col"><br><br>
                    <button class="btn waves-effect waves-light btn-success" type="submit">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    function guardar(f,e){
        e.preventDefault();

        $.ajax({         
            type: "POST",
            url: "<?=base_url()?>C_catalogos/guardar", //Nombre del controlador
            data: $(f).serialize(),

            success: function(resp) {
              if(resp == 0){
                alerta('Guardado exitosamente','success');
                regresar(e);

              } else {
                alerta(resp,'error');
              }
            }
        });
    }
</script>