<table class="table table-hover" id="tabla">
    <thead>
    	<tr>
    		<?php
    			foreach ($columnas as $col)
		        {
		            echo '<th>'.substr($col->column_name,1).'</th>';
		        }
    		?>
    		<th width="200px;">Acciones</th>
    	</tr>
	</thead>
	<tbody>
    </tbody>    
</table>

<script type="text/javascript">
 	 $(document).ready(function() {
        table = $('#tabla').DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "displayStart":$('#start').val(),
            "pageLength":$('#length').val(),
            "ajax": {
                "url": "<?=base_url();?>C_catalogos/mostrar_registros",
                "type": "POST",
                "data": {
                    "catalogo":'<?=$catalogo?>',
                    "palabra": $("#palabra").val()
                }
            }
        });
    });
</script>