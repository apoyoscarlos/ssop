<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">

                    <table class="table table-striped table-bordered display" style="width:100%" id="example">                    
                        <thead>
                            <tr>
                                <th>Nombre completo</th>
                                <th>Usuario</th>                               
                                <th>Dependencia</th>
                                <th>Correo institucional</th>
                                <th width="160px"> </th>
                            </tr>
                        </thead>                
                    </table>                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        var rol = $('#b_rol').val();
        var usuario = $('#b_usuario').val();

        //console.log(rol,usuario);

        // Setup - add a text input to each footer cell
        /*$('#example tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="'+title+'" />' );
        } );*/
     
        // DataTable
        var table = $('#example').DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?=base_url();?>C_usuarios/carga_usuarios",
                "type": "POST",
                "data": {
                    "rol": rol,
                    "usuario": usuario
                }
            }
            /*,
            initComplete: function () {
                    this.api().columns().every( function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
         
                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );
         
                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }*/
        });       

                       
     
        // Apply the search
        /*table.columns().every( function () {
            var that = this;
     
            $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );*/
    }); 


    function EliminarUsuario(id) {
        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "<?= base_url() ?>C_usuarios/delete", //Nombre del controlador
            data: {
                'id': id
            },

            success: function(resp) {
                if (resp == true) {

                    regresar();
                    alerta('Eliminado exitosamente', 'success');

                } else {
                    alerta('Error al eliminar', 'error');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    }
</script>