<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">

                    <table class="table table-striped table-bordered display" style="width:100%" id="example">                    
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre del proyecto</th>
                                <th>Dependencia Responsable</th>
                                <th>Dependencia Ejecutora</th>
                                <th width="460px">Fuente de financiamiento</th>
                                <th>Apertura programática</th>
                                <th>Año de ejecución</th>
                                <th>Partida Presupuestal</th>
                                <th>Constructor</th>
                                <th>Descripción de la construcción</th>
                                <th>Avance físico</th>
                                <th>Monto pagado</th>
                                <th>Etapa</th>
                                <th width="160px"> </th>
                            </tr>
                        </thead>                
                    </table>                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        var resp = $('#b_res').val();
        var ejec = $('#b_ejec').val();
        var anioejec = $('#b_anioejec').val();
        var fuente = $('#b_fuente').val();
        var apertura = $('#b_apertura').val();
        var partida = $('#b_partida').val();
        var etapa = $('#b_etapa').val();
        var nombre = $('#b_nombre').val();
        var constru = $('#b_constru').val();
        var id = $('#b_id').val();     
     
        // DataTable
        table = $('#example').DataTable({
            "searching": false,
            "processing": true,
            "serverSide": true,            
            "ajax": {
                "url": "<?=base_url();?>C_obras/carga_obras",
                "type": "POST",
                "data": {
                    "resp": resp,
                    "ejec": ejec,
                    "anioejec": anioejec,
                    "fuente": fuente,
                    "apertura": apertura,
                    "partida": partida,
                    "etapa": etapa,
                    "nombre": nombre,
                    "constru": constru,
                    "id": id
                }
            }            
        });

    }); 

</script>