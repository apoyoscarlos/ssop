<br><br>
<div class="card">
    <div class="card-body">
        <h1 class="card-title">Obras</h1>
        <h5 class="card-subtitle"> Administración del listado de obras. </h5>
        <br><br>
        <div class="row">
            <div class="col-md-4 mb-3">
                <label for="validationCustom01">Dependencia Responsable</label>
                <select class="select2 form-control custom-select" id="b_res" name="b_res">
                    <option value="">Seleccione...</option>
                    <?= $dependencias; ?>
                </select>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom01">Dependencia Ejecutora</label>
                <select class="select2 form-control custom-select" id="b_ejec" name="b_ejec">
                    <option value="">Seleccione...</option>
                    <?= $dependencias; ?>                    
                </select>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationTooltip02">Año de ejecución</label>
                <input class="form-control" id="b_anioejec" name="b_anioejec" type="text" placeholder="Año de ejecución">
            </div>        
        </div>        
        <div class="row">    
            <div class="col-md-4 mb-3">
                <label for="validationCustom01">Fuente de financiamiento</label>
                <select class="select2 form-control custom-select" id="b_fuente" name="b_fuente">
                    <option value="">Seleccione...</option>
                    <?= $fuentes;  ?>
                </select>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom01">Apertura programática</label>
                <select class="select2 form-control custom-select" id="b_apertura" name="b_apertura">
                    <option value="">Seleccione...</option>
                </select>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom01">Partida presupuestal</label>
                <select class="select2 form-control custom-select" id="b_partida" name="b_partida">
                    <option value="">Seleccione...</option>
                </select>
            </div>        
        </div>        
        <div class="row">
            <div class="col-md-3 mb-3">
                <label for="validationCustom01">Etapa</label>
                <select class="select2 form-control custom-select" id="b_etapa" name="b_etapa">
                    <option value="">Seleccione...</option>
                    <option value="1">SIN INICIAR</option>
                    <option value="2">EN CONSTRUCCIÓN</option>
                    <option value="3">CONCLUIDA</option>
                    <option value="4">INAUGURADA</option>
                    <option value="5">EN LICITACIÓN</option>
                    <option value="6">EN PLANEACIÓN</option>
                </select>
            </div>
            <div class="col-md-3 mb-3">
                <label for="validationTooltip02">Nombre del proyecto</label>
                <input class="form-control" id="b_nombre" name="b_nombre" type="text" placeholder="Nombre">
            </div>          
            <div class="col-md-3 mb-3">
                <label for="validationTooltip02">Constructor</label>
                <input class="form-control" id="b_constru" name="b_constru" type="text" placeholder="Constructor">
            </div> 
            <div class="col-md-3 mb-3">
                <label for="validationTooltip02">ID</label>
                <input class="form-control" id="b_id" name="b_id" type="text" placeholder="ID">
            </div>
        </div>
        <div class="row">           
            <div class="col-sm-12 col-md-3">
                <div class="form-group">
                    <div class="button-group">
                        <button type="button" class="btn waves-effect waves-light btn-light" style="margin-top:35px" onclick="buscarObra()">Buscar</button>
                        <button type="button" class="btn waves-effect waves-light btn-primary" style="margin-top:35px" onclick="agregar_obra()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- alternative pagination -->
<div id="contenedor">
    <?php
    include_once('contenido_tabla.php');
    ?>
</div>

<script>
    $(document).ready(function(){
        $('.select2').select2();
    });

    function modificar_obra(id) {        
        cargar('<?= base_url() ?>C_obras/modificar', '#contenedor', 'POST', 'id=' + id);
    }

    function buscarObra() {
        var rol = $("#b_rol").val();
        var usuario = $("#b_usuario").val();

        $.ajax({
            type: "POST",
            url: "<?= base_url() ?>C_obras/search",
            //contentType: 'json',
            success: function(resp) {
                $("#contenedor").html(resp);
                //$("#grid").dataTable().fnReloadAjax();
            },
            error: function(XMLHHttRequest, textStatus, errorThrown) {}
        });
    }

    function agregar_obra() {
        cargar('<?=base_url();?>C_obras/agregar', '#contenedor');
    }

    function quitar_fuente(fuenteid, op)
    {
        $.post('<?=base_url();?>C_obras/quitar_fuente',{fuenteid:fuenteid, op:op}, function(resp) {
            //console.log(resp);
            $('#f_'+fuenteid).remove();
        });
    }

    function agrega_fuente(op) {
        
        var fuente = $('#fuente_1 option:selected').val();
        var fuente_text = $('#fuente_1 option:selected').text();
        var monto = $('#montof_1').val();

        if(parseInt(monto) > 0)
        {
            $.post('<?=base_url();?>C_obras/agregar_fuentes', {fuente:fuente, fuente_text:fuente_text, monto:monto, op:op}, function(resp){
                if(resp==false) { alerta('La fuente de financiamiento ya fue seleccionada', 'warning'); }
                else { $('#b_fuentes').append(resp); }
            });            
        }
        else alerta('Error, debe capturar un monto para agregar la fuente de financiamiento', 'error');
    }

    function regresar() {
        var rol = $("#brol").val();
        var usuario = $("#busuario").val();

        $.ajax({
            type: "POST",
            url: "<?= base_url() ?>C_obras/search",
            //contentType: 'json',
            success: function(resp) {
                $("#contenedor").html(resp);
                 $('#grid').DataTable({
                    stateSave: true,
                });
            },
            error: function(XMLHHttRequest, textStatus, errorThrown) {}
        });
    }
</script>