<div class="col-12">
    <div class="card">
        <div class="card-body">            
            <div class="row">
                <?php 
                print_r($apertura);
                if($o_obra!=false)
                {
                    foreach ($o_obra as $vof) {
                        if($vof->vNumOficio != 0) 
                        {
                            $num_of = $vof->vNumOficio;
                            $desc_of = $vof->vDescripcion;
                        }
                        else $num_of = '';
                    }
                    
                }


                if($d_obra!=false)
                {
                    foreach ($d_obra as $vobra) {
                                                
                        $obraid = $vobra->iIdObra;
                        $nombre = $vobra->vNombre;
                        $descr = $vobra->vDescripcion;
                        $fecha = $vobra->dFechaValidacion;
                        $responsableid = $vobra->iIdResponsable;
                        $ejecutorid = $vobra->iIdEjecutor;
                        $anio = $vobra->iAnioEjecucion;
                    }
                }
                ?>
                <div class="col-md-10">
                    <h4 class="card-title">Modificar obra</h4>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-light" type="submit" onclick="regresar()"><i class="mdi mdi-arrow-left">Regresar</i></button>
                </div>
            </div>
            <br><br>
            <form class="needs-validation was-validated" onsubmit="actualizarObra(this,event);">
                                <br>
                <div class="row">
                    <input type="hidden" id="obraid" name="obraid" value="<?=$obraid;?>">
                    <div class="col-md-3 mb-3">
                        <label>Nombre del proyecto<span class="text-danger">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" id="nombre" name="nombre" value="<?=$nombre;?>" class="form-control" required="" placeholder="Ingresar nombre" maxlength="150">
                        <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">                    

                    <div class="col-md-3 mb-3">
                        <label>Descripción del proyecto<span class="text-danger">*</span></label>
                        <!--<input type="text" id="descripcion" name="descripcion" class="form-control" placeholder="ingresar apellido">-->
                    </div>
                    <div class="col-md-9 mb-9">                        
                        <!--<input type="text" id="descripcion" name="descripcion" class="form-control" placeholder="ingresar apellido">-->
                        <textarea id="descripcion" name="descripcion" class="form-control" maxlength="500" rows="2" placeholder="Descripción del proyecto"><?=$descr;?></textarea>
                    </div>
                </div>
                <br>
                <div class="row">            

                    <div class="col-md-2 mb-2">
                        <label for="validationCustom04">Dependencia Responsable</label>
                    </div>

                    <div class="col-md-4 mb-4">                        
                        <select id="responsable" name="responsable" required class="select2 form-control custom-select" style="width: 100%; height:36px;">
                            <option value="">Seleccionar...</option>
                            <?= $dep_resp ?>
                        </select>
                        <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
                    </div>

                    <div class="col-md-2 mb-2">
                        <label for="validationCustom04">Dependencia Ejecutora</label>
                    </div>

                    <div class="col-md-4 mb-4">                        
                        <select id="ejecutor" name="ejecutor" required class="select2 form-control custom-select">
                            <option value="">Seleccionar...</option>
                            <?= $dep_ejec ?>
                        </select>
                        <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
                    </div>    
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <h4 class="card-title">Oficio de validación</h4>
                    </div>
                    <div class="col-md-10">
                        <hr style="border: 1px solid #cacaca">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2 mb-2">
                        <label>Número de Oficio<span class="text-danger">*</span></label>
                    </div>

                    <div class="col-md-4 mb-4">                        
                        <input type="text" id="numero_oficio" name="numero_oficio" value="<?=$num_of;?>" class="form-control" required="" maxlength="50" placeholder="Número de oficio">
                        <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
                    </div>

                    <div class="col-md-2 mb-2">
                        <label>Descripción</label>                        
                    </div> 
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="desc_oficio" name="desc_oficio" class="form-control" placeholder="ingresar apellido">-->
                        <textarea id="desc_oficio" name="desc_oficio" class="form-control" rows="2" maxlength="150" placeholder="Descripción del oficio de validación"></textarea>
                    </div> 
                </div>                
                <br>
                <div class="row">                    

                    <div class="col-md-2 mb-2">
                        <label>Cargar Oficio</label>
                    </div>
                    
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="adjunto" name="adjunto" class="form-control" placeholder="nombre.apellido">-->
                        <input id="adjuntos" name="adjuntos" type="file" class="file" data-show-preview="false" data-allowed-file-extensions='["pdf"]'>
                    </div>

                    <div class="col-md-2 mb-2">
                        <label>Fecha de validación<span class="text-danger">*</span></label>
                    </div> 

                    <div class="col-md-4 mb-4">                        
                        <input type="date" id="fecha_validacion" name="fecha_validacion" value="<?=$fecha;?>" class="form-control" placeholder="ingresar contraseña" required="">
                        <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
                    </div> 
                                  
                </div>
                <br>                

                <div class="row">
                    <div class="col-md-3">
                        <h4 class="card-title">Fuentes de financiamiento</h4>
                    </div>
                    <div class="col-md-9">
                        <hr style="border: 1px solid #cacaca">
                    </div>
                </div>
                <br>  
                <div class="row">

                    <div class="col-md-5 mb-4">
                        <label for="validationCustom04">Fuente de financiamiento</label>
                        <select id="fuente_1" name="fuente_1" class="select2 form-control custom-select">
                            <option value="">Seleccionar...</option>
                            <?= $fuentes ?>
                        </select>
                        <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
                    </div>

                    <div class="col-md-5 mb-4">
                        <label>Monto<span class="text-danger">*</span></label>
                        <input type="text" id="montof_1" name="montof_1" class="form-control" placeholder="Monto">
                        <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
                    </div>

                    <div class="col-md-2" align="center">
                        <button type="button" onclick="agrega_fuente(2)" class="btn btn-info">Agregar </button>
                    </div>

                    <div class="col-md-12 mb-12 table-responsive m-t-20">
                        <table class="table table-bordered table-responsive-lg">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Fuente de financiamiento</th>
                                    <th scope="col">Monto</th>
                                    <th scope="col">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody id="b_fuentes">
                                <?php 
                                    $cont = count($f_obra);
                                    for ($i=0; $i < $cont; $i++) { 
                                        echo $f_obra[$i];
                                    }
                                ?>
                            </tbody>
                        </table>
                        
                    </div>

                    
                    <script>
                        // Example starter JavaScript for disabling form submissions if there are invalid fields
                        (function() {
                            'use strict';
                            window.addEventListener('load', function() {
                                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                var forms = document.getElementsByClassName('needs-validation');
                                // Loop over them and prevent submission
                                var validation = Array.prototype.filter.call(forms, function(form) {
                                    form.addEventListener('submit', function(event) {
                                        if (form.checkValidity() === false) {
                                            event.preventDefault();
                                            event.stopPropagation();
                                        }
                                        form.classList.add('was-validated');
                                    }, false);
                                });
                            }, false);
                        })();
                    </script>
                </div>               
                <br>              

                <div class="row">
                    <div class="col-md-2">
                        <h4 class="card-title">Adjuntos</h4>
                    </div>
                    <div class="col-md-10">
                        <hr style="border: 1px solid #cacaca">
                    </div>
                </div>
                <br>
                <div class="row">                    

                    <div class="col-md-2 mb-2">
                        <label>Descripción 1</label>                        
                    </div> 
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="desc_oficio" name="desc_oficio" class="form-control" placeholder="ingresar apellido">-->
                        <textarea id="desc_doc1" name="desc_doc1" class="form-control" rows="2" maxlength="150" placeholder="Descripción del oficio de validación"></textarea>
                    </div> 
                    
                    <div class="col-md-2 mb-2">
                        <label>Cargar Oficio</label>
                    </div>
                    
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="adjunto" name="adjunto" class="form-control" placeholder="nombre.apellido">-->
                        <input id="doc1" name="doc1" type="file" class="file" data-show-preview="false" data-allowed-file-extensions='["pdf"]'>
                    </div>                    
                                  
                </div>
                <br>
                <div class="row">                    

                    <div class="col-md-2 mb-2">
                        <label>Descripción 2</label>                        
                    </div> 
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="desc_oficio" name="desc_oficio" class="form-control" placeholder="ingresar apellido">-->
                        <textarea id="desc_doc2" name="desc_doc2" class="form-control" rows="2" maxlength="150" placeholder="Descripción del oficio de validación"></textarea>
                    </div> 
                    
                    <div class="col-md-2 mb-2">
                        <label>Cargar Oficio 2</label>
                    </div>
                    
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="adjunto" name="adjunto" class="form-control" placeholder="nombre.apellido">-->
                        <input id="doc2" name="doc2" type="file" class="file" data-show-preview="false" data-allowed-file-extensions='["pdf"]'>
                    </div>                    
                                  
                </div>
                <br>
                <div class="row">                    

                    <div class="col-md-2 mb-2">
                        <label>Descripción 3</label>                        
                    </div> 
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="desc_oficio" name="desc_oficio" class="form-control" placeholder="ingresar apellido">-->
                        <textarea id="desc_doc3" name="desc_doc3" class="form-control" rows="2" maxlength="150" placeholder="Descripción del oficio de validación"></textarea>
                    </div> 
                    
                    <div class="col-md-2 mb-2">
                        <label>Cargar Oficio 3</label>
                    </div>
                    
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="adjunto" name="adjunto" class="form-control" placeholder="nombre.apellido">-->
                        <input id="doc3" name="doc3" type="file" class="file" data-show-preview="false" data-allowed-file-extensions='["pdf"]'>
                    </div>                    
                                  
                </div>
                <br>
                <div class="row">                    

                    <div class="col-md-2 mb-2">
                        <label>Descripción 4</label>                        
                    </div> 
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="desc_oficio" name="desc_oficio" class="form-control" placeholder="ingresar apellido">-->
                        <textarea id="desc_doc4" name="desc_doc4" class="form-control" rows="2" maxlength="150" placeholder="Descripción del oficio de validación"></textarea>
                    </div> 
                    
                    <div class="col-md-2 mb-2">
                        <label>Cargar Oficio 4</label>
                    </div>
                    
                    <div class="col-md-4 mb-4">                        
                        <!--<input type="text" id="adjunto" name="adjunto" class="form-control" placeholder="nombre.apellido">-->
                        <input id="doc4" name="doc4" type="file" class="file" data-show-preview="false" data-allowed-file-extensions='["pdf"]'>
                    </div>                    
                                  
                </div>
                <br>
                <div class="row">                    

                    <div class="col-md-2 mb-2">
                        <label for="validationCustom04">Apertura programática</label>
                    </div>
                    <div class="col-md-4 mb-4">                        
                        <select id="apertura" name="apertura" required class="select2 form-control custom-select">
                            <option value="">Seleccionar...</option>
                            <?= $apertura ?>
                        </select>
                        <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">                                        

                    <div class="col-md-2 mb-2">
                        <label for="validationCustom04">Año de ejecución</label>                    
                    </div> 
                    <div class="col-md-4 mb-4">
                        <input type="text" id="anio_ejec" name="anio_ejec" value="<?=$anio;?>" required class="form-control" placeholder="Año de ejecución">
                        <div class="invalid-feedback">
                            Este campo no puede estar vacio.
                        </div>
                    </div> 
                </div>           
                <br>
                <div id="fuentes_fin" class="row">
                
                                <br></div>
                <div class="row">
                    <center>
                        <button class="btn waves-effect waves-light btn-success" type="submit">Guardar</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse" onclick="regresar()">Cancelar</button>
                    </center>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php base_url() ?>public/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<script src="<?php base_url() ?>public/dist/js/pages/forms/mask/mask.init.js"></script>

<script>
    var idobra = 0;
    var descOf = '';
    var numOf = 0;
    var num = 0;
    var nom_ar = '';

    $(document).ready(function(){
        $(".select2").select2();
            
        $(".file").fileinput({
            showUpload: false,
            language: 'es',
            layoutTemplates: {
                actions: '<div class="file-actions">\n' +
                '    <div class="file-footer-buttons">\n' +
                '        {delete} {zoom}' +
                '    </div>\n' +
                '    {drag}\n' +
                '    <div class="clearfix"></div>\n' +
                '</div>',
            },
            uploadUrl: '<?=base_url();?>C_obras/oficios',
            maxFileCount: 1,
            allowedFileExtensions: ["pdf"],
            maxFileSize: 3082,            
            uploadAsync: false,
            uploadExtraData: function() {
                return {
                    iIdObra: idobra,
                    vDescripcion: descOf,
                    iNumOficio: numOf,
                    i: num,
                    ar: nom_ar
                }
            }
        });
    });

    

    function valida_docs()
    {
        var band = [];
        $("input[type^='file']").each(function(index){
            var nombre = this.name;
            var filesCount = $('#'+nombre).fileinput('getFilesCount');

            if(filesCount > 0)
            {   
                if($('#desc_'+nombre).val()!="") band.push(true);
                else band.push(false);                
            }
        });
        
        if(band.includes(false)) return false;
        else return true;        
    }

    function actualizarObra(f, e) {
        e.preventDefault();
        var valid = valida_docs();
        if(valid===true)
        {
            $.ajax({
                type: "POST",
                url: "<?= base_url() ?>C_obras/actualizar", //Nombre del controlador
                data: $(f).serialize(),

                success: function(resp) {
                    resp_s = JSON.parse(resp);
                    console.log(resp_s.resp);

                    if (resp_s.resp > 0) {
                        //alert('si');
                        idobra = parseInt(resp_s.resp);
                        
                        /*
                        $("input[type^='file']").each(function(index){
                            var nombre = this.name;
                            var filesCount = $('#'+nombre).fileinput('getFilesCount');
                            if(filesCount > 0)
                            {
                                switch(nombre)
                                {
                                    case 'adjuntos':  
                                        descOf =  $('#desc_oficio').val(); numOf =  parseInt($('#numero_oficio').val()); num = 0; nom_ar = 'adjuntos'; break;
                                    case 'doc1':  
                                        descOf =  $('#desc_doc1').val(); numOf =  0; num = 1; nom_ar = 'doc1'; break;
                                    case 'doc2':  
                                        descOf =  $('#desc_doc2').val(); numOf =  0; num = 2; nom_ar = 'doc2'; break;
                                    case 'doc3':  
                                        descOf =  $('#desc_doc3').val(); numOf =  0; num = 3; nom_ar = 'doc3'; break;
                                    case 'doc4':  
                                        descOf =  $('#desc_doc4').val(); numOf =  0; num = 4; nom_ar = 'doc4'; break;
                                }
                                $('#'+nombre).fileinput('upload');
                            }
                        });                        
                        */
                        regresar();
                        alerta('Guardado exitosamente', 'success');

                    }

                    if(resp_s.resp == 'err_fuente') {
                        alerta('Seleccione al menos una fuente de financiamiento', 'warning');
                    }
                    if(resp_s.resp == "validar_pass") {
                        alerta('La confirmación de su contraseña no coincide con la contraseña', 'warning');
                    }
                    if(resp_s.resp == "validar_usuario") {
                        alerta('El nombre de usuario ya se encuentra registrado en el sistema', 'warning');
                    }
                    if(resp_s.resp == "validar_correo_inst") {
                        alerta('El correo institucional ya se encuentra registrado en el sistema', 'warning');
                    }
                    if(resp_s.resp == "validar_correo_per") {
                        alerta('El correo personal ya se encuentra registrado en el sistema', 'warning');
                    }
                    if(resp_s.resp == "error") {
                        alerta('Error al guardar', 'error');
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                }
            });
        }
        else 
        {
            alerta('Error, todos los documentos a cargar deben tener una descrición', 'error');
        }
    }
        
</script>