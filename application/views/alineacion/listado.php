<div class="row">
    <div class="col-md-10 mb-10">
        <label for="compromiso">Compromiso del Gobernador</label>
        <select class="custom-select" id="compromiso" required name="compromiso">
            <option value="">Seleccione...</option>
            <?= $compromisos ?>
        </select>
        <div class="invalid-feedback">
            Este campo no puede estar vacio.
        </div>
    </div>
    <div class="col-md-2 mb-2">
        <div class="form-group">
            <div class="button-group">
                <button class="btn waves-effect waves-light btn-light" onclick="buildArray()" style="margin-top:30px">Agregar</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered display" style="width:100%" id="grid">
                <thead>
                    <tr>
                        <th>Eje</th> 
                        <th>Politica Publica</th>
                        <th>Objetivo</th>
                        <th>Indicador</th>
                        <th>Compromiso</th>
                        <th width="160px"> </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function buildArray(){
        event.preventDefault();
        
        var idE = $('#ejes option:selected').val();
        var idPP = $('#politica_publica option:selected').val();
        var idO = $('#objetivo option:selected').val();
        var idI = $('#indicador option:selected').val();
        var idEs = $('#estrategia option:selected').val();
        var idL = $('#linea_accion option:selected').val();
        var idC = $('#compromiso option:selected').val();
        var eje = $('#ejes option:selected').text();
        var politica = $('#politica_publica option:selected').text();
        var objetivo = $('#objetivo option:selected').text();
        var indicador = $('#indicador option:selected').text();
        var estrategia = $('#estrategia option:selected').text();
        var accion = $('#linea_accion option:selected').text();
        var compromiso = $('#compromiso option:selected').text();

        if(idE == "" || idPP == "" || idO == "" || idI == "" || idEs == "" || idL == "" || idC == "" ){
            alerta("Debe seleccionar los campos requeridos.", 'warning');
        }
        else{
            let obj = {
                idE: idE,
                idPP: idPP,
                idO: idO,
                idI: idI,
                idEs: idEs,
                idL: idL,
                idC: idC,
                eje: eje,
                politica: politica,
                objetivo: objetivo,
                indicador: indicador,
                estrategia: estrategia,
                accion: accion,
                compromiso: compromiso
            }

            tableArray.push(obj)
            this.addRow(obj, tableArray.length - 1);
            
            /* //console.log('test');
            if(findElement(obj) == null){
                tableArray.push(obj);
                this.addRow(obj, tableArray.length - 1);
            }
            else{
                alerta("No se puede duplicar el registro.","warning");
            } */

        }
    }

    function addRow(obj, index){
        $("#grid > tbody").append(
            "<tr>" + 
                "<td>" + obj.eje        +   "</td>" +
                "<td>" + obj.politica   +   "</td>" +
                "<td>" + obj.objetivo   +   "</td>" +
                "<td>" + obj.indicador  +   "</td>" +
                "<td>" + obj.compromiso +   "</td>" +
                "<td><button type='button' class='btn btn-circle waves-effect waves-light btn-danger' data-toggle='tooltip' data-placement='top' title='Eliminar' onclick='confirmar(\"¿Esta usted seguro?\",removeItem,"+ index +")'><i class='mdi mdi-close'></i></button></td>"+
            "</tr>");
    }

    /* function findElement(obj){
        const getElem = tableArray.find(function(element) {
                        console.log(element);
                        });
        return getElem;
    } */

    function removeItem(drop){
        var index = 0;
        event.preventDefault();
        tableArray.splice(drop, 1);
       
        $("#grid > tbody").empty();
        tableArray.forEach(function (obj) {
            addRow(obj, index);
            index++;
        });
    }
</script>