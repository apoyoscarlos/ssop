<br><br>
<form class="needs-validation was-validated" onsubmit="guardar(this,event);">
    <div class="card">
        <div class="card-body">
            <h1 class="card-title">Obras</h1>
            <h5 class="card-subtitle"> Captura Documental de Proyectos de Obra. </h5>
            <br><br>
            <input type="hidden" id="obra_id" name="obra_id" value="<?= $obra_id ?>" />
        
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="ejes">Eje</label>
                    <select class="custom-select" id="ejes" required name="ejes">
                        <option value="">Seleccione...</option>
                        <?= $ejes ?>
                    </select>
                    <div class="invalid-feedback">
                        Este campo no puede estar vacio.
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <label for="politica_publica">Política Pública</label>
                    <select class="custom-select" id="politica_publica" required name="politica_publica">
                        <option value="">Seleccione...</option>
                        <?= $politicas ?>
                    </select>
                    <div class="invalid-feedback">
                        Este campo no puede estar vacio.
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <label for="objetivo">Objetivo</label>
                    <select class="custom-select" id="objetivo" required name="objetivo">
                        <option value="">Seleccione...</option>
                        <?= $objetivos ?>
                    </select>
                    <div class="invalid-feedback">
                        Este campo no puede estar vacio.
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <label for="indicador">Indicador</label>
                    <select class="custom-select" id="indicador" required name="indicador">
                        <option value="">Seleccione...</option>
                        <?= $indicadores ?>
                    </select>
                    <div class="invalid-feedback">
                        Este campo no puede estar vacio.
                    </div>
                </div>

                <div class="col-md-12 mb-3">
                    <label for="estrategia">Estrategia</label>
                    <select class="custom-select" id="estrategia" required name="estrategia">
                        <option value="">Seleccione...</option>
                        <?= $estrategias ?>
                    </select>
                    <div class="invalid-feedback">
                        Este campo no puede estar vacio.
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <label for="linea_accion">Línea de acción</label>
                    <select class="custom-select" id="linea_accion" required name="linea_accion">
                        <option value="">Seleccione...</option>
                        <?= $acciones ?>
                    </select>
                    <div class="invalid-feedback">
                        Este campo no puede estar vacio.
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <label for="ods">ODS</label>
                    <input type="text" class="form-control"  id="ods" required name="ods" readonly >
                    <input type="hidden" id="ods" name="idOds" >
                </div>
            </div>
        </div>
    </div>
    <div id="contenedor"  class="card">
        <div class="card-body">
            <?php  include_once('listado.php'); ?>
        </div>
    </div>
    <div id="contenedor"  class="card">
        <div class="card-body">
            <div class="col-md-12 mb-12">
                <label for="descripcion" >Descripción del compromiso</label>
                <textarea name="descripcion" required  class="form-control" id="descripcion" rows="5" cols="10" maxlength="500"></textarea>
                <div class="invalid-feedback">
                    Este campo no puede estar vacio.
                </div>
            </div>
            <center class="mt-3">
                <button class="btn waves-effect waves-light btn-success" type="submit">Guardar</button>
                <button type="reset" class="btn waves-effect waves-light btn-inverse" onclick="regresar()">Cancelar</button>
            </center>
        </div>
    </div>
</form>

<script>
    var tableArray = [];
    
    $( document ).ready(function() {
        $('#ejes').on('change', function() {
            var id = $('#ejes option:selected').val();
            var siguiente = "#politica_publica";
            var opcion = 'Politicas';
            return cargaDatos( opcion, id, siguiente);
        });
 
        $('#politica_publica').on('change', function() {
            var id = $('#politica_publica option:selected').val();
            var siguiente = "#objetivo";
            var opcion = 'Objetivos';
            return cargaDatos( opcion, id, siguiente);
        });

        $('#objetivo').on('change', function() {
            var id = $('#objetivo option:selected').val();
            var siguiente = "#indicador";
            var opcion = 'Indicadores';
            return cargaDatos( opcion, id, siguiente);
        });

        $('#indicador').on('change', function() {
            var id = $('#objetivo option:selected').val();
            var siguiente = "#estrategia";
            var opcion = 'Estrategias';
            return cargaDatos( opcion, id, siguiente);
        });

        $('#estrategia').on('change', function() {
            var id = $('#estrategia option:selected').val();
            var siguiente = "#linea_accion";
            var opcion = 'Acciones';
            return cargaDatos( opcion, id, siguiente);
        });

        $('#linea_accion').on('change', function() {
            var id = $('#linea_accion option:selected').val();
            return getOds(id);
        });
    });

    function cargaDatos(opcion, id,siguiente) {
        $.post("<?=base_url();?>C_Alineacion/cargarDatos",{ id:id, opcion: opcion },function(resultado, status){
            if(resultado != "" || resultado.indexOf('Error') == -1){
                $(siguiente).empty().append('<option value="">Seleccione...</option>');
                $(siguiente).append(resultado);
            }
            else alerta(resultado,'error');
        });
    }

    function getOds(id){
        $.post("<?=base_url();?>C_Alineacion/getOds",{ id:id },function(resultado, status){
            if(resultado != "" || resultado.indexOf('Error') == -1){
                var res = resultado.trim();
                res = res.split("||");
                $("input:text").val("Glenn Quagmire")
                $("#ods:text").val(res[1]);
                $("#idOds:text").val(res[0]);
            }
            else alerta(resultado,'error');
        });
    }

    function regresar(){
        console.log('regresar');
    }

    function guardar(f, e) {
        e.preventDefault();
        console.log(f);
        if(tableArray == 'undefined' || tableArray.length == 0){
            return alerta("Es necesario agregar un registro.", "error");
        }

        var dataToSend = { formdata: $(f).serialize(),  datos: JSON.stringify(tableArray) };
        console.log(dataToSend);
        $.ajax({
            type: "POST",
            url: "<?= base_url() ?>C_Alineacion/agregar",
            data: dataToSend,
            success: function(resp) {
                alerta("Captura Documental almacenada con exito", "success");
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                alerta(textStatus, "error");
                console.log(errorThrown);
            }
        });
    }
</script>
