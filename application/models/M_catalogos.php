<?php
class M_catalogos extends CI_Model {


	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default',TRUE);
	}
 	
 	public function campos_tabla($tabla)
	{
		$sql = "SHOW COLUMNS FROM $tabla FROM {$this->db->database};";
		return $this->db->query($sql); 
	}

	public function columna($tabla)
	{
		$sql = "SELECT COLUMN_NAME 
				FROM information_schema.COLUMNS
				WHERE TABLE_NAME = '$tabla' AND COLUMN_NAME != 'iActivo';";

		return $this->db->query($sql)->result(); 
	}

	public function total_registros($tabla)
	{
		$this->db->select('iId'.$tabla);
		$this->db->from($tabla);
		$this->db->where('iActivo',1);

		return $this->db->get()->num_rows();
	}

	public function buscar($palabra, $tabla,$inicio=null,$lim=null,$col='',$orden=null)
	{
        $like = "\"v$tabla\" ILIKE '%$palabra%'";
        $this->db->select('*');
		$this->db->from($tabla);
		$this->db->where('iActivo',1);
		$this->db->where($like);
		
		if($inicio != null && $lim != null) $this->db->limit($lim, $inicio);

		if(!empty($col) && !empty($orden)) {
			$this->db->order_by($col,$orden);
		} else {			
			$this->db->order_by('iId'.$tabla);
		}

		$query = $this->db->get();
		$_SESSION['sql'] = $this->db->last_query();
		return $query;
	}

	public function consultar_registro($id,$tabla)
	{
		$this->db->select('*');
		$this->db->from($tabla);
		$this->db->where('iId'.$tabla,$id);

		return $this->db->get()->row();
	}


	//Mostrar Roles
	public function roles($where='') {

        $this->db->select('iIdRol AS id , vRol AS valor');
		$this->db->from('Rol');
		$this->db->order_by('vRol', 'asc');

		if($where != '') $this->db->where($where);

		return $this->db->get();
	}

	//Mostrar Dependencias
	public function dependencias($where=''){
        $this->db->select('iIdDependencia AS id , vDependencia AS valor');
		$this->db->from('Dependencia');
		$this->db->where('iActivo', 1);
		$this->db->order_by('vDependencia');
		
		if($where != '') $this->db->where($where);

		return $this->db->get();
	}

	public function Localidades($where=''){
			$this->db->order_by('vLocalidad');
			$this->db->select('iIdLocalidad AS id , vLocalidad AS valor');
			$this->db->from('Localidad');
			
			if($where != '') $this->db->where($where);
	
			return $this->db->get();
	}

	public function Partidas($where=''){
		$this->db->order_by('vPartida');
		$this->db->select('iIdPartida AS id , vPartida AS valor');
		$this->db->from('Partida');
		
		if($where != '') $this->db->where($where);

		return $this->db->get();
	}

	public function Categorias($where=''){
		$this->db->order_by('vCategoria');
		$this->db->select('iIdCategoria AS id , vCategoria AS valor');
		$this->db->from('Categoria');
		
		if($where != '') $this->db->where($where);

		return $this->db->get();
	}

	public function Municipios($where=''){
		$this->db->order_by('vMunicipio');
		$this->db->select('iIdMunicipio AS id , vMunicipio AS valor');
		$this->db->from('Municipio');
		
		if($where != '') $this->db->where($where);

		return $this->db->get();
	}

	//Mostrar Fuentes de financiamiento
	public function fuentes($where = '')
	{
		$this->db->select('iIdFinanciamiento as id,vFinanciamiento as valor');
		$this->db->from('Financiamiento');
		$this->db->where('iActivo',1);

		if($where!='') $this->db->where($where);
		return $this->db->get();
		
	}

	//Mostrar catálogo de apertura programática
	public function apertura($where = '')
	{
		$this->db->select('iIdApertura as id, vApertura as valor');
		$this->db->from('Apertura');
		$this->db->where('iActivo', 1);

		if($where!='') $this->db->where($where);
		return $this->db->get();
	}

	public function Compromisos($where = '')
	{
		$this->db->order_by('iIdCompromiso');
		$this->db->select('iIdCompromiso as id, vCompromiso as valor');
		$this->db->from("Compromiso");
		$this->db->where('iActivo', 1);
		if($where!='') $this->db->where($where);
		return $this->db->get();
	}

	public function Ejes($where = '')
	{
		$this->db->order_by('iIdEje');
		$this->db->select('iIdEje as id, vEje as valor');
		$this->db->from("PED2019Eje");
		if($where!='') $this->db->where($where);
		return $this->db->get();
	}

	public function Politicas($where = '')
	{
		$this->db->order_by('iIdTema');
		$this->db->select('iIdTema as id, vTema as valor');
		$this->db->from("PED2019Tema");
		if($where!='') $this->db->where($where);
		return $this->db->get();
	}

	public function Objetivos($where = '')
	{
		$this->db->order_by('iIdObjetivo');
		$this->db->select('iIdObjetivo as id, vObjetivo as valor');
		$this->db->from("PED2019Objetivo");
		if($where!='') $this->db->where($where);
		return $this->db->get();
	}

	public function Indicadores($where = '')
	{
		$this->db->order_by('iIdIndicador');
		$this->db->select('iIdIndicador as id, vIndicador as valor');
		$this->db->from("Indicador");
		if($where!='') $this->db->where($where);
		return $this->db->get();
	}

	public function Estrategias($where = '')
	{
		$this->db->order_by('iIdEstrategia');
		$this->db->select('iIdEstrategia as id, vEstrategia as valor');
		$this->db->from("PED2019Estrategia");
		if($where!='') $this->db->where($where);
		return $this->db->get();
	}

	public function Acciones($where = '')
	{
		$this->db->order_by('iIdLineaAccion');
		$this->db->select('iIdLineaAccion as id, vLineaAccion as valor, iIdOds as idOds');
		$this->db->from("PED2019LineaAccion");
		if($where!='') $this->db->where($where);
		return $this->db->get();
	}
}

?> 