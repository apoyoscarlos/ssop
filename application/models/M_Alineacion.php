<?php

class M_Alineacion extends CI_Model {
    private $table = 'Obra';
    function __construct()
	{
		parent::__construct();
        $this->db = $this->load->database('default',TRUE);
	}

	public function getOds($where){
		$this->db->select('ODS.iIdOds as id, vOds as valor');
		$this->db->from('ODS');
		$this->db->join('PED2019LineaAccion', 'ODS.iIdOds = PED2019LineaAccion.iIdOds');
		$this->db->where($where);
		$query = $this->db->get();
        $resultado = $query->result();
        return $resultado;
	}
	
	public function guardar($data){
		$this->db->insert_batch('Alineacion', $data);
		return 1;
	}
}
?>