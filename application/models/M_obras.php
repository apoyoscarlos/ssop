<?php
class M_obras extends CI_Model {


	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default',TRUE);
	}
 	
 	public function mostrar_obras($inicial='', $lim='', $orden = '', $col = 0, $where = null, $like = null, $nombre = '', $constru = '')
	{

		//$this->db->select('o.iIdObra, o.vNombre, d.vDependencia as responsable, de.vDependencia as ejecutora, f.vFinanciamiento,  ap.vApertura, o.iAnioEjecucion, pa.vClave, pa.vPartida, o.vEmpresa, o.vDescripcion, o.iAvanceFisico, o.nMontoPagado, o.iEstado');
		$this->db->select('o.iIdObra, o.vNombre, d.vDependencia as responsable, de.vDependencia as ejecutora,  ap.vApertura, o.iAnioEjecucion, pa.vClave, pa.vPartida, o.vEmpresa, o.vDescripcion, o.iAvanceFisico, o.nMontoPagado, o.iEstado');
		$this->db->from('Obra o');
		$this->db->join('Dependencia d','o.iIdResponsable = d.iIdDependencia and d.iActivo = 1','INNER');
		$this->db->join('Dependencia de','o.iIdEjecutor = de.iIdDependencia and de.iActivo = 1','INNER');
		$this->db->join('Apertura ap','o.iIdApertura = ap.iIdApertura','LEFT');
		$this->db->join('Partida pa','o.iIdPartida = pa.iIdPartida','LEFT');
		$this->db->where('o.iActivo',1);

		if($where!=null)
		{			
            if(isset($where['resp']) && $where['resp']!='') $this->db->where('o.iIdResponsable', $where['resp']);
            if(isset($where['ejec']) && $where['ejec']!='') $this->db->where('o.iIdEjecutor', $where['ejec']);
            if(isset($where['fuente']) && $where['fuente']!='') 
            {
				$this->db->join('ObraFinanciamiento obf','o.iIdObra = obf.iIdObra','LEFT');
				$this->db->join('Financiamiento f','obf.iIdFinanciamiento = f.iIdFinanciamiento','LEFT');
            	$this->db->where('f.iIdFinanciamiento', $where['fuente']);
            }
            if(isset($where['apertura']) && $where['apertura']!='') $this->db->where('ap.iIdApertura', $where['apertura']);
            if(isset($where['partida']) && $where['partida']!='') $this->db->where('pa.iIdPartida', $where['partida']);
            if(isset($where['etapa']) && $where['etapa']!='') $this->db->where('o.iEstado', $where['etapa']);
			if(isset($where['id']) && $where['id']!='') $this->db->where('o.iIdObra', $where['id']);
			if(isset($where['anioejec']) && $where['anioejec']!='') $this->db->where('o.iAnioEjecucion', $where['anioejec']);
		}
		
			//$this->db->where("(\"o.iAnioEjecucion\" ilike '%$like['anioejec']%' or \"vNombre\" ilike '%$like['nombre']%' or \"vEmpresa\" ilike '%$like['constru']%' or \"iIdObra\" ilike '%$like['id']%')");
			
			
		if($nombre!='') 
		{
			$this->db->like('o.vNombre', $nombre);
			if($constru!='') $this->db->or_like('o.vEmpresa', $constru);
		}
		else 
		{
			if($constru!='') $this->db->like('o.vEmpresa', $constru);
		}
		
		//if(isset($like['constru']) && $like['constru']!='') $this->db->like('o.vEmpresa', $like['constru']);

		$this->db->limit($lim, $inicial);

		if(!empty($orden) && $orden != '')
        {
            switch ($col) {
                case 0: $this->db->order_by('o.iIdObra', $orden); break;
                case 1: $this->db->order_by('o.vNombre', $orden); break;
                case 2: $this->db->order_by('responsable', $orden); break;
                case 3: $this->db->order_by('ejecutora', $orden); break;
                case 4: $this->db->order_by('f.vFinanciamiento', $orden); break;
                case 5: $this->db->order_by('ap.vApertura', $orden); break;
                case 6: $this->db->order_by('o.iAnioEjecucion', $orden); break;
                case 7: $this->db->order_by('pa.vPartida', $orden); break;
                case 8: $this->db->order_by('o.vEmpresa', $orden); break;
                case 9: $this->db->order_by('o.vDescripcion', $orden); break;
                case 10: $this->db->order_by('o.iAvanceFisico', $orden); break;
                case 11: $this->db->order_by('o.nMontoPagado', $orden); break;
                case 12: $this->db->order_by('o.iEstado', $orden); break;
            }        
        }
			
		$query = $this->db->get();

		if($query!=false) return $query->result();
		else return false;
	}

	public function total_obras($where = null, $like = null, $nombre = '', $constru = '')
	{
		$this->db->select('o.iIdObra');
		$this->db->from('Obra o');
		$this->db->where('o.iActivo',1);

		if($where!=null)
		{	
            if(isset($where['resp']) && $where['resp']!='') $this->db->where('o.iIdResponsable', $where['resp']);
            if(isset($where['ejec']) && $where['ejec']!='') $this->db->where('o.iIdEjecutor', $where['ejec']);
            if(isset($where['fuente']) && $where['fuente']!='') 
            { 
				$this->db->join('ObraFinanciamiento of','o.iIdObra = of.iIdObra','LEFT');
            	$this->db->where('of.iIdFinanciamiento', $where['fuente']); 
            }
            if(isset($where['apertura']) && $where['apertura']!='') $this->db->where('ap.iIdApertura', $where['apertura']);
            if(isset($where['partida']) && $where['partida']!='') $this->db->where('pa.iIdPartida', $where['partida']);
            if(isset($where['etapa']) && $where['etapa']!='') $this->db->where('o.iEstado', $where['etapa']);
            if(isset($where['id']) && $where['id']!='') $this->db->where('o.iIdObra', $where['id']);		
			if(isset($where['anioejec']) && $where['anioejec']!='') $this->db->where('o.iAnioEjecucion', $where['anioejec']);
		}

		if($nombre!='') 
		{
			$this->db->like('o.vNombre', $nombre);
			if($constru!='') $this->db->or_like('o.vEmpresa', $constru);
		}
		else 
		{
			if($constru!='') $this->db->like('o.vEmpresa', $constru);
		}

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}	

	public function guardar_obra($datos)
	{
		$this->db->insert('Obra', $datos);
		return $this->db->insert_id();
	}

	public function actualizar_obra($datos, $obraid)
	{
		$this->db->where('iIdObra', $obraid);
		return $this->db->update('Obra', $datos);
	}

	public function borrar_fuentes($obraid)
	{
		$this->db->where('iIdObra', $obraid);
		return $this->db->delete('ObraFinanciamiento');
	}

	public function guardar_financiamiento($datos_f)
	{
		return $this->db->insert('ObraFinanciamiento', $datos_f);
	}

	public function guardar_validacion($datos_val)
	{
		return $this->db->insert('Oficio', $datos_val);
	}

	public function carga_obras($obraid)
	{
		$this->db->select('o.iIdObra, o.vNombre, o.vDescripcion, o.dFechaValidacion, o.iIdResponsable, o.iIdEjecutor, o.iAnioEjecucion, o.iIdApertura');
		$this->db->from('Obra o');
		$this->db->where('o.iActivo', 1);
		$this->db->where('o.iIdObra', $obraid);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function carga_fuentes($obraid)
	{
		$this->db->select('obf.iIdFinanciamiento, f.vFinanciamiento, obf.nMonto');
		$this->db->from('ObraFinanciamiento obf');
		$this->db->join('Financiamiento f','obf.iIdFinanciamiento = f.iIdFinanciamiento','INNER');
		$this->db->where('obf.iIdObra',$obraid);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

	public function carga_oficios($obraid)
	{
		$this->db->select('iIdOficio, vDescripcion, vRuta, iIdObra, vNumOficio');
		$this->db->from('Oficio');
		$this->db->where('iIdObra', $obraid);

		$query = $this->db->get();
		if($query!=false) return $query->result();
		else return false;
	}

}

?> 