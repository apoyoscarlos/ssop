<?php

class M_EtapaDocumental extends CI_Model {
    private $table = 'Obra';
	private $idF = 'iIdObra';
	
	private $tableLocalidadObra = 'ObraLocalidad';

	private $tableCoordenadaObra = 'Coordenada';
    
    function __construct()
	{
		parent::__construct();
        $this->db = $this->load->database('default',TRUE);
    }
    
    //Guarda la entitdad obra en la DB
	public function guardar_obra($data){

		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	
	public function guardar_Localidades($data){

		$this->db->insert_batch($this->tableLocalidadObra, $data);
		return 1;
	}
	
	public function guardar_Coordenadas($data){

		$this->db->insert_batch($this->tableCoordenadaObra, $data);
		return 1;
    }
    
    //Obtener Obra con determinado id
	public function obtener_obra($id){

		$this->db->select();
		$this->db->from($table);
		$this->db->where($this->idF, $id);

		$query =  $this->db->get()->row();
        
        return $query;

    }
    
    //Actualiza la entidad obra en la DB
	public function modificar_obra($id,$data){

		$this->db->where('iIdObra', $id);
		return $this->db->update('Obra', $data);
    }
    
    //Cambia de estatus activo a inactivo(Metodo de eliminacion)
	public function eliminar_obra($id){

		$data = array('iActivo' => 0);

		$this->db->where('iIdObras', $id);
		return $this->db->update('Obra',$data);
	}

	public function eliminar_localidades($id){

		$this->db->where_in('iIdObra', $id);

		$delete = $this->db->delete($this->tableLocalidadObra);
        return $delete?true:false;
	}

	public function eliminar_coordenadas($id){

		$this->db->where_in('iIdObra', $id);

		$delete = $this->db->delete($this->tableCoordenadaObra);
        return $delete?true:false;
	}

}


?>