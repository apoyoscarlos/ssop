<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_catalogos extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        session_start();
        $this->load->helper('url');
        $this->load->model('M_catalogos','mc');
        $this->load->model('M_seguridad','ms');
    }

    //Muestra la vista principal
    public function principal($catalogo=null)
    {
        $data['catalogo'] = $catalogo;
        $data['columnas'] = $this->mc->columna($catalogo);
        $data['start'] = 0;
        $data['length'] = 10;
        $this->load->view('catalogos/principal',$data);
    }

    function listado($catalogo)
    {
        $data['catalogo'] = $catalogo;
        $data['columnas'] = $this->mc->columna($catalogo);
        $this->load->view('catalogos/listado',$data);
    }

    function nombre_columna($posicion,$catalogo)
    {
        $columnas = $this->mc->columna($catalogo);
        $cont = 0;
        $nombre = ''; 

        foreach ($columnas as $col)
        {
            if($posicion == $cont) $nombre = $col->column_name;
            $cont++;
        }

        return $nombre;
    }

    function mostrar_registros()
    {
        $draw = $this->input->post('draw', TRUE);
        $start = $this->input->post('start', TRUE);
        $length = $this->input->post('length', TRUE);
        $columns = $this->input->post('columns', TRUE);
        $ordenamiento = $this->input->post('order', TRUE);
        $col = $ordenamiento[0]['column'];
        $orden = $ordenamiento[0]['dir'];
        
        $catalogo = $this->input->post('catalogo');
        $palabra = $this->input->post('palabra');
        $datos = array();

        $nombre_columna = $this->nombre_columna($col,$catalogo);

        //  Obtenemos las columnas y los registros de la tabla
        $columnas = $this->mc->columna($catalogo);
        $registros_filtrados = $this->mc->buscar($palabra, $catalogo)->num_rows();
        $registros = $this->mc->buscar($palabra, $catalogo,$start,$length,$nombre_columna,$orden);

        $registros = $registros->result();
        foreach ($registros as $reg)
        {
            $id = 0;
            $array = array();            
            foreach ($columnas as $col)
            {
                if($col->column_name == "iId$catalogo") $id = $reg->{$col->column_name};
                $array[]= $reg->{$col->column_name};
            }
            $array[] = '<button type="button" class="btn btn-circle waves-effect waves-light btn-warning" onclick="capturar('.$id.')"><i class="mdi mdi-border-color"></i></button><button type="button" class="btn btn-circle waves-effect waves-light btn-danger" onclick="confirmarEliminacion('.$id.',event);"><i class="mdi mdi-close"></i></button>';

            $datos[] = $array;

        }


        $usuarios = array(  'draw' => $draw,
                            'recordsTotal' => $this->mc->total_registros($catalogo),
                            'recordsFiltered' => $registros_filtrados,
                            'data' => $datos,
                    );
        echo json_encode($usuarios);
    }

    public function blank(){
        echo 'Hola mundo!!';
    }

    // Muestra la pantalla de captura
    public function capturar(){
        if(isset($_POST['id']) && isset($_POST['cat']))
        {
            $datos = array();
            $datos['id'] = $this->input->post('id');
            $datos['cat'] = $this->input->post('cat');
            $campos = array();
            
            if($datos['id'] == 0)
            {
                $columnas = $this->mc->columna($datos['cat']);
                foreach ($columnas as $col)
                {
                   $campos[$col->column_name] = '';
                }
                $datos['campos'] = $campos;
            }
            else
            {
                $registro = $this->mc->consultar_registro($datos['id'],$datos['cat']);
                foreach ($registro as $key => $value)
                {
                    $campos[$key] = $value;
                }
                $datos['campos'] = $campos;
            }

            $this->load->view('catalogos/capturar',$datos);
        }
    }

    //  Guarda los cambios en el registro    
    public function guardar()
    {
        if(isset($_POST['cat']) && !empty($_POST['cat']))
        {

            $cat = $this->input->post('cat');
            $id = $where['iId'.$cat] = (int)$this->input->post('iId'.$cat);
            $datos = array();
            $columnas = $this->mc->columna($cat);
            foreach ($columnas as $col)
            {
                if($col->column_name != 'iId'.$cat)
                {
                    $datos[$col->column_name] =  $this->input->post($col->column_name);
                }
            }

            $con = $this->ms->iniciar_transaccion();

            if($id == 0){
                $this->ms->inserta_registro($cat,$datos,$con);
            } else {
                $this->ms->actualiza_registro($cat,$where,$datos,$con);
            }
            //var_dump($where);
            //var_dump($datos);

            if($this->ms->terminar_transaccion($con)) echo '0';
            else echo 'El registro no pudo ser guardado';  
        }
    }

    //  Elimina el registro
    public function eliminar()
    {
        if(isset($_POST['id']) && isset($_POST['cat']))
        {
            $cat = $this->input->post('cat');
            $where['iId'.$cat] = $this->input->post('id');
            $respuesta = array();

            if($this->ms->desactivar_registro($cat,$where))
            {   
                $respuesta['cod'] = 0;
                $respuesta['mensaje'] = 'El registro ha sido eliminado';
            }
            else
            {
                $respuesta['cod'] = 1; 
                $respuesta['mensaje'] = 'El registro no pudo ser eliminado';
            }
        }
    }
}
?>