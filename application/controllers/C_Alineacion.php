<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_Alineacion extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        session_start();
        $this->load->helper('url');
        $this->load->library('Class_options');
        $this->load->model('M_Alineacion', 'modelo');
    }

    //Muestra la vista principal
    public function index(){
        $lib = new Class_options();
        $data['ejes'] = $lib->options_tabla('Ejes');
        $data['compromisos'] = $lib->options_tabla('Compromisos');
        $data['obra_id'] = 1;

        $this->load->view('alineacion/principal', $data);
    }

    public function cargarDatos(){
        $lib = new Class_options();
        $opcion = $this->input->post('opcion');
        
        if($opcion == "Politicas"){
            $where['iIdEje'] = $this->input->post('id');
        }
        else if($opcion == "Objetivos"){
            $where['iIdTema'] = $this->input->post('id');
        }
        else if($opcion == "Indicadores"){
            $where['iIdObjetivo'] = $this->input->post('id');
        }
        else if($opcion == "Estrategias"){
            $where['iIdObjetivo'] = $this->input->post('id');
        }
        else if($opcion == "Acciones"){
            $where['iIdEstrategia'] = $this->input->post('id');
        }

        $data['result'] = $lib->options_tabla($opcion, "", $where);
        echo $data['result'];
    }

    public function getOds(){
        $where['PED2019LineaAccion.iIdLineaAccion'] = $this->input->post('id');
        $data['result'] = $this->modelo->getOds($where);
    	foreach ($data['result'] as $row)
        {
            echo $row->id."||".$row->valor;
        }
    }

    //Funcion para insertar/actualizar
    public function agregar()
    {
        parse_str($_POST['formdata'], $formdata);
        $tabla = json_decode($_POST['datos']);
        $data = array();

        foreach ($tabla as $value) {
            $final= array();
            $final['iIdObra']            = 1;
            $final['iIdEje']             = $value->idE;
            $final['iIdPoliticaPublica'] = $value->idPP;
            $final['iIdObjetivo']        = $value->idO;
            $final['iIdIndicador']       = $value->idI;
            $final['iIdEstrategia']      = $value->idEs;
            $final['iIdOds']             = $value->idL;
            $final['iIdCompromiso']      = $value->idC;
            $final['vDescripcion']       = $formdata["descripcion"];
         
            array_push($data, $final);
        }

        $resultado = $this->modelo->guardar($data);
        echo $resultado;
    }
}
