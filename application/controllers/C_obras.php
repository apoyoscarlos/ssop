<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_obras extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        session_start();
        $this->load->helper('url');
        $this->load->model('M_catalogos','mc');
        $this->load->model('M_obras','mo');
        $this->load->library('Class_options');
    }

    //Muestra la vista principal
    public function index()
    {        
        $lib = new Class_options();

        $data['dependencias'] = $lib->options_tabla('dependencias');
        $data['fuentes'] = $lib->options_tabla('fuentes');
        //$data['listado'] = $this->mo->listado_obras();
        
        $this->load->view('obras/principal',$data);
    }

    public function carga_obras()
    {
        $draw = $this->input->post('draw', TRUE);
        $start = $this->input->post('start', TRUE);
        $length = $this->input->post('length', TRUE);
        $ordenamiento = $this->input->post('order', TRUE);
        $col = $ordenamiento[0]['column'];
        $orden = $ordenamiento[0]['dir'];
        $like = array();

        //carga los datos de búsqueda
        $resp = $this->input->post('resp', TRUE);
        $ejec = $this->input->post('ejec', TRUE);
        $anioejec = $this->input->post('anioejec', TRUE);
        $fuente = $this->input->post('fuente', TRUE);
        $apertura = $this->input->post('apertura', TRUE);
        $partida = $this->input->post('partida', TRUE);
        $etapa = $this->input->post('etapa', TRUE);
        $nombre = $this->input->post('nombre', TRUE);
        $constru = $this->input->post('constru', TRUE);
        $id = $this->input->post('id', TRUE);

        $where = array(
            'resp' => $resp, 
            'ejec' => $ejec, 
            'fuente' => $fuente, 
            'apertura' => $apertura, 
            'partida' => $partida, 
            'etapa' => $etapa, 
            'id' => $id,
            'anioejec' => $anioejec
        );        
    
        //echo 'draw: '.$draw.'- start: '.$start.'- length: '.$length.'- col: '.$col.'- orden: '.$orden;

        $obras = $this->mo->mostrar_obras($start, $length, $orden, $col, $where, $like, $nombre, $constru);        
        $total_obras = $this->mo->total_obras();
        $total_filtro = $this->mo->total_obras($where, $like, $nombre, $constru);

        $ob = array();        

        //print_r($obras);
        
        
        foreach ($obras as $vobra) {

            $btn_ob = '<button type="button" class="btn btn-circle waves-effect waves-light btn-warning" data-toggle="tooltip" data-placement="top" title="Editar" onclick="modificar_obra('.$vobra->iIdObra.')"><i class="mdi mdi-border-color"></i></button>&nbsp;&nbsp;';
            $btn_ob.= '<button type="button" class="btn btn-circle waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="confirmar(\'¿Esta usted seguro?\',EliminarObra,'.$vobra->iIdObra.')"><i class="mdi mdi-close"></i></button>&nbsp;&nbsp;';
            $listaf = '';
            $fuentes = $this->mo->carga_fuentes($vobra->iIdObra);
            if($fuentes!=false)
            {
                $i = 1;
                foreach ($fuentes as $vf) {
                    $listaf.= $i.'.- '.$vf->vFinanciamiento.'<br>';
                    $i++;
                }
            }

            //$datos = array($vobra->iIdObra, $vobra->vNombre, $vobra->responsable, $vobra->ejecutora, $vobra->vFinanciamiento, $vobra->vApertura, $vobra->iAnioEjecucion,     $vobra->vPartida, $vobra->vEmpresa, $vobra->vDescripcion, $vobra->iAvanceFisico, $vobra->nMontoPagado, $vobra->iEstado,$btn_ob);
            $datos = array($vobra->iIdObra, $vobra->vNombre, $vobra->responsable, $vobra->ejecutora, $listaf, $vobra->vApertura, $vobra->iAnioEjecucion,     $vobra->vPartida, $vobra->vEmpresa, $vobra->vDescripcion, $vobra->iAvanceFisico, $vobra->nMontoPagado, $vobra->iEstado,$btn_ob);

            $ob[] = $datos;
        }

        $obras_res = array( 
            'draw' => $draw,
            'recordsTotal' => count($total_obras),
            'recordsFiltered' => count($total_filtro),
            'data' => $ob
        );

        echo json_encode($obras_res);                
    }

    public function agregar()
    {
        $lib = new Class_options();

        $data['dependencias'] = $lib->options_tabla('dependencias');
        $data['fuentes'] = $lib->options_tabla('fuentes');
        $data['apertura'] = $lib->options_tabla('apertura');

        $this->load->view('obras/contenido_agregar', $data);
    }

    public function modificar()
    {
        $lib = new Class_options();

        $obraid = $this->input->post('id');
        $d_obra = $this->mo->carga_obras($obraid);
        $data['d_obra'] = $d_obra;
        $data['o_obra'] = $this->mo->carga_oficios($obraid, 1);


        $data['dep_resp'] = $lib->options_tabla('dependencias',$d_obra[0]->iIdResponsable);
        $data['dep_ejec'] = $lib->options_tabla('dependencias',$d_obra[0]->iIdEjecutor);
        $data['fuentes'] = $lib->options_tabla('fuentes');
        $data['apertura'] = $lib->options_tabla('apertura', $d_obra[0]->iIdApertura); 


        $f_obra = $this->mo->carga_fuentes($obraid);

        if(count($f_obra) > 0)
        {
            foreach ($f_obra as $vf) {
                $_SESSION['fuentes_mod'][] = array('fuenteid' => $vf->iIdFinanciamiento, 'monto' => $vf->nMonto);
                $data['f_obra'][] = '<tr id="f_'.$vf->iIdFinanciamiento.'"><td>'.$vf->iIdFinanciamiento.'</td><td>'.$vf->vFinanciamiento.'</td><td>'.$vf->nMonto.'</td><td><button onclick="quitar_fuente('.$vf->iIdFinanciamiento.', 2)" type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i> </button></td></tr>';
            }
        }

        $this->load->view('obras/contenido_modificar',$data);
    }

    public function quitar_fuente()
    {
        $fuenteid = $this->input->post('fuenteid', TRUE);
        $op = $this->input->post('op', TRUE);
        $var_s = ($op == 1) ? 'fuentes' : 'fuentes_mod';
        $cont = count($_SESSION[$var_s]);        
        print_r($_SESSION[$var_s]);
        for ($i=0; $i < $cont; $i++) { 

            $f = $_SESSION[$var_s][$i]['fuenteid'];
            if($f == $fuenteid) $bf = $i;

        }

        array_splice($_SESSION[$var_s], $bf, 1);
        print_r($_SESSION[$var_s]);
        
    }

    public function guardar()
    {           
        if(isset($_SESSION['fuentes']) && count($_SESSION['fuentes']) > 0)
        {
            $dFechaVal = $this->input->post('fecha_validacion', TRUE);

            $datos['vNombre'] = $this->input->post('nombre', TRUE);
            $datos['vDescripcion'] = $this->input->post('descripcion', TRUE);
            //$datos_f['nMonto'] = $this->input->post('montof_1', TRUE);
            //$datos_f['iIdFinanciamiento'] = $this->input->post('fuente_1', TRUE);
            $datos['iIdResponsable'] = $this->input->post('responsable', TRUE);
            $datos['iIdEjecutor'] = $this->input->post('ejecutor', TRUE);
            $datos['iIdApertura'] = $this->input->post('apertura', TRUE);
            $datos['iAnioEjecucion'] = $this->input->post('anio_ejec', TRUE);
            $datos['dFechaCaptura'] = date("Y-m-d H:i:s");
            $datos['dFechaValidacion'] = date('Y-m-d H:i:s', strtotime($dFechaVal));
        
            //$datos_val['iNumOficio'] = $this->input->post('numero_oficio', TRUE);
            //$datos_val['vDescripcion'] = $this->input->post('desc_oficio', TRUE);
            //$datos_val['vRuta'] = $this->input->post('adjunto', TRUE);
            //$datos_val['iActivo'] = 1;

            $obraid = $this->mo->guardar_obra($datos);

            $cont = count($_SESSION['fuentes']);
            $datos_f['iIdObra'] = $obraid;
            for ($i=0; $i < $cont; $i++) { 

                $datos_f['nMonto'] = $_SESSION['fuentes'][$i]['monto'];
                $datos_f['iIdFinanciamiento'] = $_SESSION['fuentes'][$i]['fuenteid'];
                $guardar_fin = $this->mo->guardar_financiamiento($datos_f);        
            }     
            //$datos_val['iIdObra'] = $obraid;
            
            if($guardar_fin==1) { $resp['resp'] = $obraid; unset($_SESSION['fuentes']); }
            else $resp['resp'] = 'error';
        }
        else $resp['resp'] = 'err_fuente';

        echo json_encode($resp);
    }

    public function actualizar()
    {


        if(isset($_SESSION['fuentes_mod']) && count($_SESSION['fuentes_mod']) > 0)
        {
            $obraid = $this->input->post('obraid', TRUE);
            $dFechaVal = $this->input->post('fecha_validacion', TRUE);

            $datos['vNombre'] = $this->input->post('nombre', TRUE);
            $datos['vDescripcion'] = $this->input->post('descripcion', TRUE);
            //$datos_f['nMonto'] = $this->input->post('montof_1', TRUE);
            //$datos_f['iIdFinanciamiento'] = $this->input->post('fuente_1', TRUE);
            $datos['iIdResponsable'] = $this->input->post('responsable', TRUE);
            $datos['iIdEjecutor'] = $this->input->post('ejecutor', TRUE);
            $datos['iIdApertura'] = $this->input->post('apertura', TRUE);
            $datos['iAnioEjecucion'] = $this->input->post('anio_ejec', TRUE);
            $datos['dFechaCaptura'] = date("Y-m-d H:i:s");
            $datos['dFechaValidacion'] = date('Y-m-d H:i:s', strtotime($dFechaVal));            

            $act = $this->mo->actualizar_obra($datos, $obraid);
            $borrar = $this->mo->borrar_fuentes($obraid);

            $cont = count($_SESSION['fuentes_mod']);
            $datos_f['iIdObra'] = $obraid;
            for ($i=0; $i < $cont; $i++) {

                $datos_f['nMonto'] = $_SESSION['fuentes_mod'][$i]['monto'];
                $datos_f['iIdFinanciamiento'] = $_SESSION['fuentes_mod'][$i]['fuenteid'];
                $guardar_fin = $this->mo->guardar_financiamiento($datos_f);        
            }     
            //$datos_val['iIdObra'] = $obraid;
            
            if($guardar_fin==1) { $resp['resp'] = $obraid; unset($_SESSION['fuentes_mod']); }
            else $resp['resp'] = 'error';
        }
        else $resp['resp'] = 'err_fuente';

        echo json_encode($resp);
    }

    public function oficios()
    {
        $ruta = 'public/documentos/';
        $iIdObra = $this->input->post('iIdObra', TRUE);
        $vDescripcion = $this->input->post('vDescripcion', TRUE);
        $iNumOficio = $this->input->post('iNumOficio', TRUE);
        $i = $this->input->post('i', TRUE);
        $ar = $this->input->post('ar', TRUE);        
    
        $nombre = $_FILES[$ar]['name'];
        $nombreTemp = $_FILES[$ar]['tmp_name'];
        $nombreArch = 'Oficio_'.trim($iIdObra).'_'.$i;


        $resto = explode(".", $nombre);            
        $extension = end($resto);
        $nombreArch.='.'.$extension;
        $rutaAdj = $ruta.$nombreArch;

        $datos = array(
            'vDescripcion' => $vDescripcion,
            'vRuta' => $rutaAdj,
            'iIdObra' => intval($iIdObra),
            'iActivo' => 1,
            'vNumOficio' => $iNumOficio
        );

        $guardar_val = $this->mo->guardar_validacion($datos);
        if($guardar_val==1) 
        {
            move_uploaded_file($nombreTemp, $rutaAdj);
        }

        $arr = array("file_id"=>0,"overwriteInitial"=>true);
        echo json_encode($arr);

    }

    //Funcion de busquedas
    public function search()
    {       
        $this->load->view('obras/contenido_tabla');
    }

    public function agregar_fuentes()
    {
        $fuenteid = $this->input->post('fuente', TRUE);
        $monto = $this->input->post('monto', TRUE);
        $texto = $this->input->post('fuente_text', TRUE);
        $op = $this->input->post('op', TRUE);
        $ids = array();
        $var_s = ($op == 1) ? 'fuentes' : 'fuentes_mod';

        if(isset($_SESSION[$var_s]) && !empty($_SESSION[$var_s]))
        {
            $cont = count($_SESSION[$var_s]);

            for ($i=0; $i < $cont; $i++) { 
                $ids[] = $_SESSION[$var_s][$i]['fuenteid'];
            }        
            if(!in_array($fuenteid, $ids)) 
            {
                $_SESSION[$var_s][] = array('fuenteid' => $fuenteid, 'monto' => $monto);
                echo '<tr id="f_'.$fuenteid.'"><td>'.$fuenteid.'</td><td>'.$texto.'</td><td>'.$monto.'</td><td><button onclick="quitar_fuente('.$fuenteid.',1)" type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i> </button></td></tr>';
            }
            else echo false;
            
        }
        else
        {
            $_SESSION[$var_s][] = array('fuenteid' => $fuenteid, 'monto' => $monto);
            echo '<tr id="f_'.$fuenteid.'"><td>'.$fuenteid.'</td><td>'.$texto.'</td><td>'.$monto.'</td><td><button onclick="quitar_fuente('.$fuenteid.',1)" type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i> </button></td></tr>';
        }
    }
}
?>