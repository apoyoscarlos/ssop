<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_EtapaDocumental extends CI_Controller
{

 public function __construct()
 {
  parent::__construct();
  session_start();
  $this->load->helper('url');
  $this->load->library('Class_options');
  $this->load->model('M_EtapaDocumental', 'mo');
 }

 //Muestra la vista principal
 public function index()
 {
  $lib = new Class_options();

  $data['municipios'] = $lib->options_tabla('Municipios');
  $data['categorias'] = $lib->options_tabla('Categorias');
  $data['partidas']   = $lib->options_tabla('Partidas');
  $data['id'] = 4;

  $this->load->view('etapaDocumental/principal', $data);
 }

 public function cargarLocalidades()
 {
  $lib = new Class_options();

  $where['iIdMunicipio'] = $this->input->post('id');

  $data['localidades'] = $lib->options_tabla('Localidades', $this->input->post('id'), $where);
  echo $data['localidades'];
 }

 //Funcion para insertar/actualizar
 public function insert()
 {
  parse_str($_POST['formdata'], $formdata); //This will convert the string to array
  $coordinates = json_decode($_POST['coordinates']);
  $localidades = json_decode($_POST['localidades']);

  $id = $formdata["id"];

  $data = array();

  $data['iIdPartida']   = $formdata["b_partidas"];
  $data['iIdCategoria'] = $formdata["b_categorias"];

  //General Data
  $data['vNombre'] = "ObraTest01"; 
  $data['vDescripcionImpacto'] = $formdata["b_descImp"]; // $this->input->post('clave');//
  $data['vBeneficiarios']         = $formdata["b_beneficiarios"];
  $data['nImporteOriginal']    = $formdata["b_importe"];
  $data['iAnioRecurso']        = $formdata["b_anio"];
  $data['vDomicilio']          = $formdata["b_domicilio"];

  //Insert
  $resultado = $this->mo->guardar_obra($data);
  $id = $resultado;

  //Update
/*   $resultado = $this->mo->modificar_obra($id, $data); */


  //Entidades de Relacion Externos
  //-- Localidad-------------------------------------------------
  $listadoLocalidad = array();

  $this->mo->eliminar_localidades($id);

  foreach ($localidades as $resultx) {
   $dataLocalidad                 = array();
   $dataLocalidad['iIdLocalidad'] = $resultx->idL;
   $dataLocalidad['iIdObra']      = $id;

   array_push($listadoLocalidad, $dataLocalidad);
  }

  $resultadoloc = $this->mo->guardar_Localidades($listadoLocalidad);
  //--------------------------------------------------------------

  //-- Coordenadas------------------------------------------------
  $listadoCoordenadas = array();

  $this->mo->eliminar_coordenadas($id);

  foreach ($coordinates as $resultx) {

   $dataCoordenadas = array();

   $dataCoordenadas['nPunto']  = $resultx->nPunto;
   $dataCoordenadas['iTipo']   = $resultx->iTipo;
   $dataCoordenadas['nLat']    = $resultx->nLat;
   $dataCoordenadas['nLong']   = $resultx->nLong;
   $dataCoordenadas['nNombre'] = $resultx->nNombre;
   $dataCoordenadas['iIdObra'] = $id;

   array_push($listadoCoordenadas, $dataCoordenadas);
  }

  $resultadocoor = $this->mo->guardar_Coordenadas($listadoCoordenadas);
  //-----------------------------------------------------------------

  echo $resultado;

 }
}
